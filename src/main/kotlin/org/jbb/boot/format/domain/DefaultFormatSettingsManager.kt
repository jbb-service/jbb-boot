/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import arrow.core.Either
import com.google.common.collect.Lists
import org.jbb.boot.format.api.DefaultFormatSettings
import org.jbb.boot.format.api.UpdateDefaultFormatSettingsFailed
import org.jbb.boot.format.domain.DefaultFormatSettingsFactory.toModel
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.validation.ValidationIssue
import org.jbb.swanframework.validation.ValidationService
import org.springframework.stereotype.Component

@Component
internal class DefaultFormatSettingsManager(
        private val repository: FormatSettingRepository,
        private val validationService: ValidationService,
        private val dryRunContext: DryRunContext,
) {

    fun getSettings() = toModel(repository.findAllByIsDefaultIsTrue())

    fun updateSettings(
            newSettings: DefaultFormatSettings
    ): Either<UpdateDefaultFormatSettingsFailed, DefaultFormatSettings> {
        val validationIssues: MutableList<ValidationIssue> = mutableListOf()
        validationIssues.addAll(validationService.validate(newSettings))
        if (!FormatValidationUtils.isValidDateTimePattern(newSettings.dateTimeFormat)) {
            validationIssues.add(
                    ValidationIssue(
                            name = "dateTimeFormat",
                            message = "date time format is not valid"
                    )
            )
        }
        if (!FormatValidationUtils.isValidDurationPattern(newSettings.durationFormat)) {
            validationIssues.add(ValidationIssue(
                    name = "durationFormat",
                    message = "duration format is not valid"
            ))
        }
        if (validationIssues.isNotEmpty()) {
            return Either.Left(UpdateDefaultFormatSettingsFailed(validationIssues))
        }
        dryRunContext.completeIfDryRun()
        val currentFormats = repository.findAllByIsDefaultIsTrue()
        val dateTimeSetting = findSettingForType(
                currentFormats,
                FormatType.DATE_TIME
        ).apply { formatValue = newSettings.dateTimeFormat }
        val durationSetting = findSettingForType(
                currentFormats,
                FormatType.DURATION
        ).apply { formatValue = newSettings.durationFormat }
        val updatedSettings = Lists.newArrayList(dateTimeSetting, durationSetting)
        repository.saveAll(updatedSettings)
        return Either.Right(toModel(updatedSettings))
    }

    private fun findSettingForType(currentFormats: List<FormatSettingEntity>,
                                   formatType: FormatType): FormatSettingEntity =
            currentFormats.find { it.formatType == formatType }
                    ?: FormatSettingEntity(true, formatType)
}