/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import org.jbb.swanframework.application.MemberId
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
internal interface FormatSettingRepository : PagingAndSortingRepository<FormatSettingEntity, String> {
    fun findAllByIsDefaultIsTrue(): List<FormatSettingEntity>
    fun findAllByMemberId(memberId: MemberId): List<FormatSettingEntity>
}