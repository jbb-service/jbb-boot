/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DurationFormatUtils
import java.time.format.DateTimeFormatter

internal object FormatValidationUtils {

    fun isValidDateTimePattern(dateTimePattern: String?): Boolean {
        return if (StringUtils.isBlank(dateTimePattern)) {
            true
        } else try {
            DateTimeFormatter.ofPattern(dateTimePattern)
            true
        } catch (e: Exception) {
            false
        }
    }

    fun isValidDurationPattern(durationPattern: String?): Boolean {
        return if (StringUtils.isBlank(durationPattern)) {
            true
        } else try {
            DurationFormatUtils.formatDuration(1L, durationPattern)
            DateTimeFormatter.ofPattern(durationPattern)
            true
        } catch (e: Exception) {
            false
        }
    }
}