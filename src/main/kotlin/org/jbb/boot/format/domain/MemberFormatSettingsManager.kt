/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import arrow.core.Either
import org.jbb.boot.format.api.MemberFormat
import org.jbb.boot.format.api.MemberFormatSettings
import org.jbb.boot.format.api.UpdateMemberFormatSettingsFailed
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.validation.ValidationIssue
import org.jbb.swanframework.validation.ValidationService
import org.springframework.stereotype.Component

@Component
internal class MemberFormatSettingsManager(
        private val repository: FormatSettingRepository,
        private val validationService: ValidationService,
        private val dryRunContext: DryRunContext,
) {

    fun getSettings(memberId: MemberId): MemberFormatSettings {
        val memberFormats = repository.findAllByMemberId(memberId)
        val defaultFormats = repository.findAllByIsDefaultIsTrue()
        return merge(memberFormats, defaultFormats)
    }

    fun updateSettings(memberId: MemberId, newSettings: MemberFormatSettings):
            Either<UpdateMemberFormatSettingsFailed, MemberFormatSettings> {
        val currentMemberFormats = repository.findAllByMemberId(memberId)
        val defaultFormats = repository.findAllByIsDefaultIsTrue()
        return update(memberId, newSettings, currentMemberFormats)
                .map { merge(it, defaultFormats) }
    }

    private fun merge(
            memberFormats: List<FormatSettingEntity>,
            defaultFormats: List<FormatSettingEntity>
    ) = MemberFormatSettings(
            dateTimeFormat = resolveFormat(memberFormats, defaultFormats, FormatType.DATE_TIME),
            durationFormat = resolveFormat(memberFormats, defaultFormats, FormatType.DURATION)
    )

    private fun resolveFormat(
            memberFormats: List<FormatSettingEntity>,
            defaultFormats: List<FormatSettingEntity>,
            formatType: FormatType
    ): MemberFormat {
        val foundFormat = memberFormats.find { it.formatType == formatType }
                ?: defaultFormats.find { it.formatType == formatType }
                ?: throw IllegalStateException("$formatType not found")
        return MemberFormat(
                useDefault = foundFormat.isDefault,
                customValue = if (foundFormat.isDefault) null else foundFormat.formatValue
        )
    }

    private fun update(
            memberId: MemberId,
            newSettings: MemberFormatSettings,
            currentMemberFormats: List<FormatSettingEntity>
    ): Either<UpdateMemberFormatSettingsFailed, List<FormatSettingEntity>> {
        val validationIssues: MutableList<ValidationIssue> = mutableListOf()
        validationIssues.addAll(validationService.validate(newSettings))

        validateIfExactlyOneFieldProvided(
                newSettings.dateTimeFormat, "dateTimeFormat",
                validationIssues
        )
        validateIfExactlyOneFieldProvided(
                newSettings.durationFormat, "durationFormat",
                validationIssues
        )

        if (!FormatValidationUtils.isValidDateTimePattern(newSettings.dateTimeFormat.customValue)) {
            validationIssues.add(ValidationIssue(
                    name = "dateTimeFormat",
                    message = "date time format is not valid"
            ))
        }
        if (!FormatValidationUtils.isValidDurationPattern(newSettings.durationFormat.customValue)) {
            validationIssues.add(ValidationIssue(
                    name = "durationFormat",
                    message = "duration format is not valid"
            ))
        }
        if (validationIssues.isNotEmpty()) {
            return Either.Left(UpdateMemberFormatSettingsFailed(validationIssues))
        }
        dryRunContext.completeIfDryRun()

        updateFormat(newSettings.dateTimeFormat, FormatType.DATE_TIME, currentMemberFormats,
                memberId)
        updateFormat(newSettings.durationFormat, FormatType.DURATION, currentMemberFormats,
                memberId)
        return Either.Right(repository.findAllByMemberId(memberId))
    }

    private fun validateIfExactlyOneFieldProvided(memberFormat: MemberFormat, fieldName: String,
                                                  validationIssues: MutableList<ValidationIssue>) {
        if (memberFormat.useDefault == !memberFormat.customValue.isNullOrBlank()) {
            validationIssues.add(ValidationIssue(
                    name = fieldName,
                    message = "you should use default OR provide a custom pattern"
            ))
        }
    }

    private fun updateFormat(
            memberFormat: MemberFormat, formatType: FormatType,
            currentMemberFormats: List<FormatSettingEntity>, memberId: MemberId
    ) {
        if (memberFormat.useDefault) {
            currentMemberFormats.filter { it.formatType == formatType }
                    .forEach { repository.delete(it) }
        }
        memberFormat.customValue
                ?.let { createOrUpdateFormat(memberId, formatType, currentMemberFormats, it) }
    }

    private fun createOrUpdateFormat(
            memberId: MemberId, formatType: FormatType,
            currentMemberFormats: List<FormatSettingEntity>,
            customValue: String
    ) {
        val updatedEntity = currentMemberFormats.find { it.formatType == formatType }
                ?: FormatSettingEntity(false, formatType, memberId, customValue)
        updatedEntity.formatValue = customValue
        repository.save(updatedEntity)
    }
}