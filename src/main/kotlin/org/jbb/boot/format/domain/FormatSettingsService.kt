/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import arrow.core.Either
import org.jbb.boot.format.api.*
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEventBus
import org.springframework.stereotype.Service

@Service
internal class FormatSettingsService(
        private val defaultSettingsManager: DefaultFormatSettingsManager,
        private val memberSettingsManager: MemberFormatSettingsManager,
        private val domainEventBus: DomainEventBus,
) : DefaultFormatSettingsFacade, MemberFormatSettingsFacade {

    override fun getDefaultFormatSettings(): DefaultFormatSettings =
            defaultSettingsManager.getSettings()

    override fun updateDefaultFormatSettings(defaultFormatSettings: DefaultFormatSettings):
            Either<UpdateDefaultFormatSettingsFailed, DefaultFormatSettings> =
            defaultSettingsManager.updateSettings(defaultFormatSettings)
                    .also { domainEventBus.post(DefaultFormatSettingsChangedEvent()) }

    override fun getFormatSettingsFor(memberId: MemberId): MemberFormatSettings =
            memberSettingsManager.getSettings(memberId)

    override fun updateFormatSettingsFor(
            memberId: MemberId,
            newSettings: MemberFormatSettings
    ): Either<UpdateMemberFormatSettingsFailed, MemberFormatSettings> =
            memberSettingsManager.updateSettings(memberId, newSettings)
                    .also { domainEventBus.post(MemberFormatSettingsChangedEvent(memberId)) }
}