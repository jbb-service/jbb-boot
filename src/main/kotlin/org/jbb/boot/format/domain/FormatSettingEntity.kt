/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "JBB_FORMAT_SETTINGS")
internal data class FormatSettingEntity(
        @field:Column(name = "is_default")
        val isDefault: Boolean,

        @field:Enumerated(EnumType.STRING)
        @field:Column(name = "format_type")
        val formatType: FormatType,

        @field:Column(name = "member_id")
        val memberId: MemberId? = null,

        @field:NotBlank
        @field:Column(name = "format_value")
        var formatValue: String? = null
) : BaseEntity()