/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_5_0
import org.jbb.boot.format.api.DefaultFormatSettings
import org.jbb.boot.format.api.DefaultFormatSettingsFacade
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.swanframework.arrow.assertRight
import org.springframework.stereotype.Component

@Component
internal class FirstDefaultFormatsCreator(
        private val defaultFormatSettingsFacade: DefaultFormatSettingsFacade
) : InstallationAction<Nothing> {

    override fun name() = "Creating default formats"

    override fun fromVersion(): Version = VERSION_0_5_0

    override fun install(context: Nothing?): Either<InstallationAbortedDueToActionRequired, Unit> {
        defaultFormatSettingsFacade.updateDefaultFormatSettings(
                DefaultFormatSettings(
                        dateTimeFormat = "dd/MM/yyyy HH:mm:ss",
                        durationFormat = "HH:mm:ss"
                )
        ).assertRight()
        return Either.Right(Unit)
    }
}