/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

internal const val FORMAT_SETTINGS_API = "Format settings API"

internal const val DEFAULT = "/default"
internal const val FORMAT_SETTINGS = "/format-settings"
internal const val MEMBERS = "/members"
internal const val MEMBER_ID_VAR = "memberId"
internal const val MEMBER_ID = "/{$MEMBER_ID_VAR}"