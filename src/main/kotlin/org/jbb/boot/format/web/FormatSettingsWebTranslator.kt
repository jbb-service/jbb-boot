/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import org.jbb.boot.format.api.DefaultFormatSettings
import org.jbb.boot.format.api.MemberFormat
import org.jbb.boot.format.api.MemberFormatSettings

internal object FormatSettingsWebTranslator {

    fun toDto(memberFormatSettings: MemberFormatSettings): MemberFormatSettingsDto =
            MemberFormatSettingsDto(
                    dateTimeFormat = MemberFormatDto(
                            useDefault = memberFormatSettings.dateTimeFormat.useDefault,
                            customValue = memberFormatSettings.dateTimeFormat.customValue
                    ),
                    durationFormat = MemberFormatDto(
                            useDefault = memberFormatSettings.durationFormat.useDefault,
                            customValue = memberFormatSettings.durationFormat.customValue
                    )
            )

    fun toModel(updateSettingsDto: MemberFormatSettingsDto): MemberFormatSettings =
            MemberFormatSettings(
                    dateTimeFormat = MemberFormat(
                            useDefault = updateSettingsDto.dateTimeFormat?.useDefault ?: false,
                            customValue = updateSettingsDto.dateTimeFormat?.customValue
                    ),
                    durationFormat = MemberFormat(
                            useDefault = updateSettingsDto.durationFormat?.useDefault ?: false,
                            customValue = updateSettingsDto.durationFormat?.customValue
                    )
            )

    fun toDto(defaultSettings: DefaultFormatSettings): DefaultFormatSettingsDto =
            DefaultFormatSettingsDto(
                    dateTimeFormat = defaultSettings.dateTimeFormat,
                    durationFormat = defaultSettings.durationFormat
            )

    fun toModel(defaultSettingsDto: DefaultFormatSettingsDto): DefaultFormatSettings =
            DefaultFormatSettings(
                    dateTimeFormat = defaultSettingsDto.dateTimeFormat ?: "",
                    durationFormat = defaultSettingsDto.durationFormat ?: ""
            )
}