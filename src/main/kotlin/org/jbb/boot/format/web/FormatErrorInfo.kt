/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.format.web

import org.jbb.boot.format.api.UpdateDefaultFormatSettingsFailed
import org.jbb.boot.format.api.UpdateMemberFormatSettingsFailed
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class FormatErrorInfo(
        override val status: HttpStatus,
        override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    DEFAULT_FORMAT_SETTINGS_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST,
            "FRMT-100",
            "Default format settings update failed",
            setOf(UpdateDefaultFormatSettingsFailed::class.java)
    ),
    MEMBER_FORMAT_SETTINGS_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST,
            "FRMT-101",
            "Member format settings update failed",
            setOf(UpdateMemberFormatSettingsFailed::class.java)
    );

}