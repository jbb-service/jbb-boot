/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.format.api.MemberFormatSettingsFacade
import org.jbb.boot.format.api.UpdateMemberFormatSettingsFailed
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toDto
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toModel
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = FORMAT_SETTINGS_API)
@RequestMapping(
        API_V1 + MEMBERS + MEMBER_ID + FORMAT_SETTINGS,
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class MemberFormatSettingsResource(
        private val memberReadFacade: MemberReadFacade,
        private val memberFormatSettingsFacade: MemberFormatSettingsFacade,
) {

    @GetMapping
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(
            summary = "Get format settings for a member",
            description = "Gets a format settings for a member"
    )
    @Throws(MemberNotFound::class)
    fun getMemberFormatSettings(
            @PathVariable(MEMBER_ID_VAR) memberId: MemberId
    ): MemberFormatSettingsDto = memberReadFacade.getMemberById(memberId)
            .map { toDto(memberFormatSettingsFacade.getFormatSettingsFor(it.memberId)) }
            .assertRight()

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(
            summary = "Update format settings for a member",
            description = "Updates a format settings for a member"
    )
    @Throws(MemberNotFound::class, UpdateMemberFormatSettingsFailed::class)
    fun updateMemberFormatSettings(
            @PathVariable(MEMBER_ID_VAR) memberId: MemberId,
            @RequestBody formatSettingsDto: MemberFormatSettingsDto
    ): MemberFormatSettingsDto {
        memberReadFacade.getMemberById(memberId).assertRight()
        val settings = toModel(formatSettingsDto)
        return toDto(
                memberFormatSettingsFacade.updateFormatSettingsFor(memberId, settings).assertRight()
        )
    }
}