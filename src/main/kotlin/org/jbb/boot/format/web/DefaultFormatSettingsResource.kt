/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.format.api.DefaultFormatSettingsFacade
import org.jbb.boot.format.api.UpdateDefaultFormatSettingsFailed
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toDto
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toModel
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = FORMAT_SETTINGS_API)
@RequestMapping(API_V1 + FORMAT_SETTINGS + DEFAULT, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class DefaultFormatSettingsResource(
        private val defaultFormatSettingsFacade: DefaultFormatSettingsFacade,
) {

    @Operation(
            summary = "Get default format settings",
            description = "Gets a default format settings"
    )
    @PreAuthorize(PERMIT_ALL)
    @GetMapping
    fun getDefaultFormatSettings(): DefaultFormatSettingsDto =
            toDto(defaultFormatSettingsFacade.getDefaultFormatSettings())

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(
            summary = "Update default format settings",
            description = "Updates a default format settings"
    )
    @Throws(UpdateDefaultFormatSettingsFailed::class)
    fun updateDefaultFormatSettings(
            @RequestBody updateDto: DefaultFormatSettingsDto
    ): DefaultFormatSettingsDto {
        val settings = toModel(updateDto)
        return toDto(
                defaultFormatSettingsFacade.updateDefaultFormatSettings(settings).assertRight()
        )
    }
}