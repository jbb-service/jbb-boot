/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.format.api

import arrow.core.Either
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue

// Operations
interface MemberFormatSettingsFacade {

    fun getFormatSettingsFor(memberId: MemberId): MemberFormatSettings

    fun updateFormatSettingsFor(
            memberId: MemberId,
            newSettings: MemberFormatSettings
    ): Either<UpdateMemberFormatSettingsFailed, MemberFormatSettings>
}

// Errors
class UpdateMemberFormatSettingsFailed(override val issues: List<ValidationIssue>) :
        RuntimeException(),
        ValidationFailed

// Models
data class MemberFormatSettings(
        val dateTimeFormat: MemberFormat,
        val durationFormat: MemberFormat
)

data class MemberFormat(
        val useDefault: Boolean,
        val customValue: String?
)

// Events
class MemberFormatSettingsChangedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "MemberFormatSettingsChangedEvent(memberId='$memberId') ${super.toString()}"
    }
}