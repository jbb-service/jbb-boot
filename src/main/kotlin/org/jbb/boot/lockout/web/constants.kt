/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web

internal const val LOCKOUT_SETTINGS_API = "Lockout settings API"
internal const val MEMBER_LOCKS_API = "Member locks API"

internal const val LOCKOUT_SETTINGS = "/lockout-settings"
internal const val MEMBERS = "/members"
const val MEMBER_ID_VAR = "memberId"
const val MEMBER_ID = "/{$MEMBER_ID_VAR}"
internal const val ACTIVE_LOCK = "/active-lock"
