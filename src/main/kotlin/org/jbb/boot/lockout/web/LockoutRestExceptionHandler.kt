/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web

import org.jbb.boot.lockout.api.ActiveMemberLockNotFound
import org.jbb.boot.lockout.api.UpdateLockoutSettingsFailed
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@Order(DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice(annotations = [RestController::class])
internal class LockoutRestExceptionHandler {

    @ExceptionHandler(UpdateLockoutSettingsFailed::class)
    fun handle(ex: UpdateLockoutSettingsFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(LockoutErrorInfo.LOCKOUT_SETTINGS_UPDATE_FAILED, ex)
    }

    @ExceptionHandler(ActiveMemberLockNotFound::class)
    fun handle(ex: ActiveMemberLockNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(LockoutErrorInfo.ACTIVE_MEMBER_LOCK_NOT_FOUND)
    }

}