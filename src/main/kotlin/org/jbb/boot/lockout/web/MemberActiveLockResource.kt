/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.lockout.api.ActiveMemberLockNotFound
import org.jbb.boot.lockout.api.MemberLockFacade
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = MEMBER_LOCKS_API)
@RequestMapping(
        API_V1 + MEMBERS + MEMBER_ID + ACTIVE_LOCK,
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class MemberActiveLockResource(
        private val memberReadFacade: MemberReadFacade,
        private val memberLockFacade: MemberLockFacade,
) {

    @Operation(
            summary = "Get active lock for a member",
            description = "Gets an active lock for a member"
    )
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @GetMapping
    @Throws(MemberNotFound::class, ActiveMemberLockNotFound::class)
    fun getActiveLock(@PathVariable(MEMBER_ID_VAR) memberId: MemberId): MemberLockDto {
        memberReadFacade.getMemberById(memberId).assertRight()
        return memberLockFacade.getMemberActiveLock(memberId)
                ?.let { ActiveLockWebTranslator.toDto(it) }
                ?: throw ActiveMemberLockNotFound()
    }

    @DryRunnable
    @DeleteMapping
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Delete active lock for a member",
            description = "Removes an active lock for a member"
    )
    @Throws(MemberNotFound::class, ActiveMemberLockNotFound::class)
    fun deleteActiveLock(@PathVariable(MEMBER_ID_VAR) memberId: MemberId) {
        memberReadFacade.getMemberById(memberId).assertRight()
        memberLockFacade.releaseMemberLock(memberId).assertRight()
    }
}