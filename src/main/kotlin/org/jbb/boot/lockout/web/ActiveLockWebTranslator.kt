/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web

import org.jbb.boot.lockout.api.MemberLock

internal object ActiveLockWebTranslator {

    fun toDto(lock: MemberLock): MemberLockDto = MemberLockDto(
            memberId = lock.memberId,
            active = lock.active,
            createdAt = lock.createdAt,
            expiresAt = lock.expiresAt
    )

}