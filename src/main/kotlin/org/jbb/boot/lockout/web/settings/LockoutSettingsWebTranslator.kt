/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web.settings

import org.jbb.boot.lockout.api.LockoutSettings

internal object LockoutSettingsWebTranslator {

    fun toDto(settings: LockoutSettings): LockoutSettingsDto = LockoutSettingsDto(
            ignoreAttemptsFromMinutes = settings.ignoreAttemptsFromMinutes,
            attemptsLimit = settings.attemptsLimit,
            lockDurationMinutes = settings.lockDurationMinutes,
            lockingEnabled = settings.lockingEnabled
    )

    fun toModel(dto: LockoutSettingsDto): LockoutSettings = LockoutSettings(
            attemptsLimit = dto.attemptsLimit ?: 0,
            ignoreAttemptsFromMinutes = dto.ignoreAttemptsFromMinutes ?: 0,
            lockDurationMinutes = dto.lockDurationMinutes ?: 0,
            lockingEnabled = dto.lockingEnabled ?: true
    )


}