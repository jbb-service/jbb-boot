/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.web.settings

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.lockout.api.LockoutSettingsFacade
import org.jbb.boot.lockout.api.UpdateLockoutSettingsFailed
import org.jbb.boot.lockout.web.LOCKOUT_SETTINGS
import org.jbb.boot.lockout.web.LOCKOUT_SETTINGS_API
import org.jbb.boot.lockout.web.settings.LockoutSettingsWebTranslator.toDto
import org.jbb.boot.lockout.web.settings.LockoutSettingsWebTranslator.toModel
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = LOCKOUT_SETTINGS_API)
@RequestMapping(API_V1 + LOCKOUT_SETTINGS, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class LockoutSettingsResource(
        private val lockoutSettingsFacade: LockoutSettingsFacade,
) {

    @Operation(summary = "Get lockout settings", description = "Gets a lockout settings")
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @GetMapping
    fun getLockoutSettings(): LockoutSettingsDto =
            toDto(lockoutSettingsFacade.getLockoutSettings())

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(summary = "Update lockout settings", description = "Updates a lockout settings")
    @Throws(UpdateLockoutSettingsFailed::class)
    fun updateLockoutSettings(
            @RequestBody settingsDto: LockoutSettingsDto
    ): LockoutSettingsDto {
        val settings = toModel(settingsDto)
        return toDto(lockoutSettingsFacade.updateLockoutSettings(settings).assertRight())
    }
}