/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.web

import org.jbb.boot.lockout.api.ActiveMemberLockNotFound
import org.jbb.boot.lockout.api.UpdateLockoutSettingsFailed
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class LockoutErrorInfo(
        override val status: HttpStatus, override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    LOCKOUT_SETTINGS_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST, "LOCK-100",
            "Lockout settings update failed", setOf(UpdateLockoutSettingsFailed::class.java)
    ),
    ACTIVE_MEMBER_LOCK_NOT_FOUND(
            HttpStatus.NOT_FOUND, "LOCK-101", "Active lock for member not found",
            setOf(ActiveMemberLockNotFound::class.java)
    );

}
