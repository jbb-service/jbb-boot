/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.domain

import org.jbb.boot.lockout.api.LockoutSettingsFacade
import org.jbb.boot.lockout.api.MemberLockFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.security.NewRootCauseException
import org.jbb.swanframework.security.SignInFailureHandler
import org.springframework.security.authentication.LockedException
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest

@Component
internal class LockoutSignInFailureHandler(
        private val settingsFacade: LockoutSettingsFacade,
        private val lockFacade: MemberLockFacade,
        private val lockJudge: LockJudge
) : SignInFailureHandler {

    override fun handle(
            login: String, memberId: MemberId?, request: HttpServletRequest
    ): NewRootCauseException? {
        val lockCreated = memberId?.let {
            lockJudge.lockMemberIfQualify(
                    memberId = it,
                    hasActiveLock = lockFacade.getMemberActiveLock(memberId)?.let { true }
                            ?: false,
                    lockoutSettings = settingsFacade.getLockoutSettings()
            )
        } ?: false
        return LockedException("Lock has been created for member $memberId").takeIf { lockCreated }
    }

}