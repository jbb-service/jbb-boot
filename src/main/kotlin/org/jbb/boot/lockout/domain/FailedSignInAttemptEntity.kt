/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.jpa.BaseEntity
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
@Table(name = "JBB_FAILED_SIGN_IN_ATTEMPTS")
internal data class FailedSignInAttemptEntity(

        @field:NotNull @field:Column(name = "member_id")
        val memberId: MemberId,

        @field:NotNull @field:Column(name = "attempt_date_time")
        var attemptDateTime: Instant
) : BaseEntity()