/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.domain

import arrow.core.Either
import org.jbb.boot.lockout.api.ActiveMemberLockNotFound
import org.jbb.boot.lockout.api.MemberLock
import org.jbb.boot.lockout.api.MemberLockFacade
import org.jbb.boot.lockout.api.MemberUnlockedEvent
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
internal class MemberLockService(
        private val repository: MemberLockRepository,
        private val domainEventBus: DomainEventBus,
        private val dryRunContext: DryRunContext
) : MemberLockFacade {

    @Transactional
    override fun getMemberActiveLock(memberId: MemberId): MemberLock? {
        val activeLock = repository.findAllByMemberIdAndActiveTrue(memberId)
        activeLock?.takeIf { it.expirationHappened() }?.deactivate(early = false)
        return activeLock?.takeIf { it.active }?.let { MemberLockFactory.toModel(it) }
    }

    @Transactional
    override fun releaseMemberLock(memberId: MemberId): Either<ActiveMemberLockNotFound, Unit> {
        val activeLock = repository.findAllByMemberIdAndActiveTrue(memberId)
                ?: return Either.Left(ActiveMemberLockNotFound())
        dryRunContext.completeIfDryRun()
        activeLock.deactivate(early = true)
        return Either.Right(Unit)
    }

    private fun MemberLockEntity.deactivate(early: Boolean): MemberLockEntity {
        this.run {
            active = false
            earlyDeactivatedAt = if (early) Instant.now() else null
        }
        repository.save(this)
        domainEventBus.post(MemberUnlockedEvent(this.memberId))
        return this
    }


}