/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain.settings

import org.jbb.boot.lockout.api.LockoutSettings

internal object LockoutSettingsFactory {

    fun toModel(entity: LockoutSettingsEntity) = LockoutSettings(
            attemptsLimit = entity.attemptsLimit,
            ignoreAttemptsFromMinutes = entity.ignoreAttemptsFromMinutes,
            lockDurationMinutes = entity.lockDurationMinutes,
            lockingEnabled = entity.lockingEnabled
    )

    fun toEntity(lockoutSettings: LockoutSettings): LockoutSettingsEntity =
            LockoutSettingsEntity(
                    attemptsLimit = lockoutSettings.attemptsLimit,
                    ignoreAttemptsFromMinutes = lockoutSettings.ignoreAttemptsFromMinutes,
                    lockDurationMinutes = lockoutSettings.lockDurationMinutes,
                    lockingEnabled = lockoutSettings.lockingEnabled
            )
}