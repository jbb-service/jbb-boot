/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.domain

import org.jbb.boot.lockout.api.LockoutSettings
import org.jbb.boot.lockout.api.MemberLockedEvent
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.application.TimeMachine
import org.jbb.swanframework.eventbus.DomainEventBus
import org.springframework.stereotype.Component
import java.time.Instant
import java.time.temporal.ChronoUnit


@Component
internal class LockJudge(
        private val attemptRepository: FailedSignInAttemptRepository,
        private val memberLockRepository: MemberLockRepository,
        private val domainEventBus: DomainEventBus
) {

    fun lockMemberIfQualify(
            memberId: MemberId, hasActiveLock: Boolean,
            lockoutSettings: LockoutSettings
    ): Boolean {
        if (!lockoutSettings.lockingEnabled || hasActiveLock) {
            return false
        }

        removeOldSignInAttempts(memberId, lockoutSettings.takeIntoConsiderationAttemptsFrom())
        recordSignInAttempt(memberId)
        return tryToLockMember(
                memberId,
                lockoutSettings.attemptsLimit,
                lockoutSettings.lockDurationMinutes
        )
    }

    private fun removeOldSignInAttempts(memberId: MemberId, attemptsNoSoonerThan: Instant) {
        attemptRepository.findAllByMemberId(memberId)
                .filter { it.attemptDateTime.isBefore(attemptsNoSoonerThan) }
                .forEach { attemptRepository.delete(it) }
    }

    private fun recordSignInAttempt(memberId: MemberId) {
        FailedSignInAttemptEntity(memberId, Instant.now()).let { attemptRepository.save(it) }
    }

    private fun tryToLockMember(
            memberId: MemberId, attemptLimit: Int,
            lockDurationMinutes: Long
    ): Boolean {
        if (attemptRepository.findAllByMemberId(memberId).size >= attemptLimit) {
            MemberLockEntity(
                    memberId = memberId,
                    active = true,
                    expiresAt = TimeMachine.now()
                            .plus(lockDurationMinutes, ChronoUnit.MINUTES),
                    earlyDeactivatedAt = null
            ).persist()
            return true
        }
        return false
    }

    private fun MemberLockEntity.persist() {
        memberLockRepository.save(this)
        domainEventBus.post(MemberLockedEvent(this.memberId, this.expiresAt))
        attemptRepository.findAllByMemberId(this.memberId).forEach { attemptRepository.delete(it) }
    }
}

