/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.domain.settings

import arrow.core.Either
import org.jbb.boot.lockout.api.LockoutSettings
import org.jbb.boot.lockout.api.LockoutSettingsChangedEvent
import org.jbb.boot.lockout.api.LockoutSettingsFacade
import org.jbb.boot.lockout.api.UpdateLockoutSettingsFailed
import org.jbb.boot.lockout.domain.settings.LockoutSettingsFactory.toEntity
import org.jbb.boot.lockout.domain.settings.LockoutSettingsFactory.toModel
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.validation.ValidationService
import org.springframework.stereotype.Service

@Service
internal class LockoutSettingsService(
        private val repository: LockoutSettingsRepository,
        private val validationService: ValidationService,
        private val dryRunContext: DryRunContext,
        private val domainEventBus: DomainEventBus
) : LockoutSettingsFacade {

    override fun getLockoutSettings(): LockoutSettings = toModel(repository.findAll().first())

    override fun updateLockoutSettings(lockoutSettings: LockoutSettings):
            Either<UpdateLockoutSettingsFailed, LockoutSettings> {
        val issues = validationService.validate(lockoutSettings)
        if (issues.isNotEmpty()) {
            return Either.Left(UpdateLockoutSettingsFailed(issues))
        }
        dryRunContext.completeIfDryRun()

        val entity = repository.findAll().firstOrNull() ?: toEntity(lockoutSettings)
        entity.run {
            this.attemptsLimit = lockoutSettings.attemptsLimit
            this.ignoreAttemptsFromMinutes =
                    lockoutSettings.ignoreAttemptsFromMinutes
            this.lockDurationMinutes = lockoutSettings.lockDurationMinutes
            this.lockingEnabled = lockoutSettings.lockingEnabled
        }
        return Either.Right(toModel(repository.save(entity)))
                .also { domainEventBus.post(LockoutSettingsChangedEvent()) }
    }
}