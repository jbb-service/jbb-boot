/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_9_0
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.boot.lockout.api.LockoutSettings
import org.jbb.boot.lockout.api.LockoutSettingsFacade
import org.jbb.swanframework.arrow.assertRight
import org.springframework.stereotype.Component

@Component
internal class FirstLockoutSettingsCreator(
        private val lockoutSettingsFacade: LockoutSettingsFacade
) : InstallationAction<Nothing> {

    override fun name() = "Creating lockout settings"

    override fun fromVersion(): Version = VERSION_0_9_0

    override fun install(context: Nothing?): Either<InstallationAbortedDueToActionRequired, Unit> {
        lockoutSettingsFacade.updateLockoutSettings(
                LockoutSettings(
                        attemptsLimit = 5,
                        ignoreAttemptsFromMinutes = 10,
                        lockDurationMinutes = 10,
                        lockingEnabled = true
                )
        ).assertRight()
        return Either.Right(Unit)
    }
}