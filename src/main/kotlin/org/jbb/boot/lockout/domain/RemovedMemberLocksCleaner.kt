/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain

import com.google.common.eventbus.Subscribe
import org.jbb.boot.member.api.MemberRemovedEvent
import org.jbb.swanframework.eventbus.DomainEventBusListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
internal class RemovedMemberLocksCleaner(
        private val lockRepository: MemberLockRepository,
        private val attemptRepository: FailedSignInAttemptRepository
) : DomainEventBusListener {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @Subscribe
    fun removeLockAndFailedSignInAttemptsForMember(event: MemberRemovedEvent) {
        log.debug("Remove all locks for member id {}", event.memberId)
        lockRepository.findAllByMemberId(event.memberId).forEach { lockRepository.delete(it) }

        log.debug("Remove records about failed sign in attempts for member id {}", event.memberId)
        attemptRepository.findAllByMemberId(event.memberId).forEach { attemptRepository.delete(it) }

    }

}