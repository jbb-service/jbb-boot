/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.jpa.BaseEntity
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
@Table(name = "JBB_MEMBER_LOCKS")
internal data class MemberLockEntity(

        @field:NotNull @field:Column(name = "member_id")
        val memberId: MemberId,

        @field:NotNull @field:Column(name = "active")
        var active: Boolean,

        @field:NotNull @field:Column(name = "expires_at")
        val expiresAt: Instant,

        @field:Column(name = "early_deactivated_at")
        var earlyDeactivatedAt: Instant?
) : BaseEntity() {

        fun expirationHappened() = expiresAt.isBefore(Instant.now())
}