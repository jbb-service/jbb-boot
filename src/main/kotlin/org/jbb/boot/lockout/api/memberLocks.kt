/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.api

import arrow.core.Either
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent
import java.time.Instant

// Operations
interface MemberLockFacade {

    fun getMemberActiveLock(memberId: MemberId): MemberLock?

    fun releaseMemberLock(memberId: MemberId): Either<ActiveMemberLockNotFound, Unit>

}

// Errors
class ActiveMemberLockNotFound : RuntimeException()

// Models
data class MemberLock(
        val memberId: MemberId,
        val active: Boolean,
        val createdAt: Instant,
        val expiresAt: Instant,
        val earlyDeactivatedAt: Instant?
)

// Events
class MemberLockedEvent(val memberId: MemberId, val expirationDateTime: Instant) : DomainEvent() {
    override fun toString(): String {
        return "MemberLockedEvent(memberId='$memberId', expirationDateTime=$expirationDateTime) ${super.toString()}"
    }
}

class MemberUnlockedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "MemberUnlockedEvent(memberId='$memberId') ${super.toString()}"
    }
}