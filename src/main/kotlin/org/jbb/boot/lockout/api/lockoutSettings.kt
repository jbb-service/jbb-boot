/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.api

import arrow.core.Either
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.validation.constraints.Min

// Operations
interface LockoutSettingsFacade {

    fun getLockoutSettings(): LockoutSettings

    fun updateLockoutSettings(lockoutSettings: LockoutSettings):
            Either<UpdateLockoutSettingsFailed, LockoutSettings>
}

// Errors
class UpdateLockoutSettingsFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
data class LockoutSettings(
        @field:Min(1)
        val attemptsLimit: Int,
        @field:Min(1)
        val ignoreAttemptsFromMinutes: Long,
        @field:Min(1)
        val lockDurationMinutes: Long,
        val lockingEnabled: Boolean
) {
    fun takeIntoConsiderationAttemptsFrom(): Instant =
            Instant.now().minus(ignoreAttemptsFromMinutes, ChronoUnit.MINUTES)
}

// Events
class LockoutSettingsChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "LockoutSettingsChangedEvent() ${super.toString()}"
    }
}