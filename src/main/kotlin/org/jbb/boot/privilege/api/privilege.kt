/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.privilege.api

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent

// Operations
interface PrivilegeFacade {

    fun getPrivilegesForMember(memberId: MemberId): Set<Privilege>

    fun updatePrivileges(memberId: MemberId, privileges: Set<Privilege>): Set<Privilege>
}

// Models
const val CREATE_ADMIN_INSTALL_ACTION_NAME = "Creating administrator"

enum class Privilege {
    ADMINISTRATOR
}

// Events
class AdministratorPrivilegeAddedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "AdministratorPrivilegeAddedEvent(memberId='$memberId') ${super.toString()}"
    }
}

class AdministratorPrivilegeRemovedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "AdministratorPrivilegeRemovedEvent(memberId='$memberId')"
    }
}