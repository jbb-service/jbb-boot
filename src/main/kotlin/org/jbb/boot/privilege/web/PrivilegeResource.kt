/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.boot.privilege.api.Privilege
import org.jbb.boot.privilege.api.PrivilegeFacade
import org.jbb.boot.privilege.web.PrivilegeWebTranslator.toDto
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

const val MEMBERS = "/members"
const val MEMBER_ID_VAR = "memberId"
const val MEMBER_ID = "/{$MEMBER_ID_VAR}"
const val PRIVILEGES = "/privileges"
const val ADMINISTRATOR = "/administrator"

@RestController
@Tag(name = "Member privileges API")
@RequestMapping(
        API_V1 + MEMBERS + MEMBER_ID + PRIVILEGES,
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class PrivilegeResource(
        private val memberReadFacade: MemberReadFacade,
        private val privilegeFacade: PrivilegeFacade,
) {

    @GetMapping
    @PreAuthorize(PERMIT_ALL)
    @Operation(summary = "Get privileges of a member", description = "Gets member privileges")
    @Throws(MemberNotFound::class)
    fun getPrivileges(@PathVariable(MEMBER_ID_VAR) memberId: MemberId): PrivilegesDto? {
        memberReadFacade.getMemberById(memberId).assertRight()
        return toDto(privilegeFacade.getPrivilegesForMember(memberId))
    }

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @PutMapping(path = [ADMINISTRATOR], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(
            summary = "Update administrator privileges for a member",
            description = "Adds or removes administrator privilege for member"
    )
    @Throws(MemberNotFound::class)
    fun updateAdministratorPrivilege(
            @PathVariable(MEMBER_ID_VAR) memberId: MemberId,
            @RequestBody updatePrivilegeDto: @Valid UpdatePrivilegeDto
    ): PrivilegesDto {
        memberReadFacade.getMemberById(memberId).assertRight()
        val privileges = privilegeFacade.getPrivilegesForMember(memberId)
        val newPrivileges = if (updatePrivilegeDto.granted) {
            privileges.plus(Privilege.ADMINISTRATOR)
        } else {
            privileges.minus(Privilege.ADMINISTRATOR)
        }
        privilegeFacade.updatePrivileges(memberId, newPrivileges)
        return toDto(privilegeFacade.getPrivilegesForMember(memberId))
    }
}