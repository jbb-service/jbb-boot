/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import org.jbb.boot.privilege.api.AdministratorPrivilegeAddedEvent
import org.jbb.boot.privilege.api.AdministratorPrivilegeRemovedEvent
import org.jbb.boot.privilege.api.Privilege
import org.jbb.swanframework.eventbus.DomainEventBus
import org.springframework.stereotype.Component

@Component
internal class PrivilegeEventEmitter(private val domainEventBus: DomainEventBus) {

    fun sendAboutAddition(privilege: PrivilegeEntity) {
        when (privilege.privilege) {
            Privilege.ADMINISTRATOR -> domainEventBus
                    .post(AdministratorPrivilegeAddedEvent(privilege.memberId))
        }
    }

    fun sendAboutRemoval(privilege: PrivilegeEntity) {
        when (privilege.privilege) {
            Privilege.ADMINISTRATOR -> domainEventBus
                    .post(AdministratorPrivilegeRemovedEvent(privilege.memberId))
        }
    }
}