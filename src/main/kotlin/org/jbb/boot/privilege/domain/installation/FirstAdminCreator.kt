/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_1_0
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.boot.privilege.api.CREATE_ADMIN_INSTALL_ACTION_NAME
import org.jbb.boot.privilege.api.Privilege
import org.jbb.boot.privilege.api.PrivilegeFacade
import org.jbb.swanframework.arrow.assertRight
import org.springframework.stereotype.Component

@Component
internal class FirstAdminCreator(
        private val memberWriteFacade: MemberWriteFacade,
        private val privilegeFacade: PrivilegeFacade,
) : InstallationAction<CreateMemberCommand> {

    override fun name() = CREATE_ADMIN_INSTALL_ACTION_NAME

    override fun fromVersion(): Version = VERSION_0_1_0

    override fun install(context: CreateMemberCommand?):
            Either<InstallationAbortedDueToActionRequired, Unit> {
        val member = context?.let { memberWriteFacade.createMember(it).assertRight() }
                ?: return Either.Left(InstallationAbortedDueToActionRequired("Missing admin data"))
        privilegeFacade.updatePrivileges(member.memberId, setOf(Privilege.ADMINISTRATOR))
        return Either.Right(Unit)
    }
}