/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import org.jbb.boot.privilege.api.Privilege
import org.jbb.swanframework.health.DomainHealthCheck
import org.jbb.swanframework.installation.InstallationVerifier
import org.springframework.stereotype.Component

@Component
internal class AdministratorExistsHealthCheck(
        private val privilegeRepository: PrivilegeRepository,
        private val installationVerifier: InstallationVerifier,
) : DomainHealthCheck() {

    override fun getName(): String = "Administrator existence"

    override fun check(): Result =
            if (!installationVerifier.isInstalled()
                    || privilegeRepository.countByPrivilege(Privilege.ADMINISTRATOR) > 0) {
                Result.healthy()
            } else {
                Result.unhealthy("No member with administrator privileges found")
            }
}