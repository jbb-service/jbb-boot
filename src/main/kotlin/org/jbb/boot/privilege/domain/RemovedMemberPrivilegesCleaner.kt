/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import com.google.common.eventbus.Subscribe
import org.jbb.boot.member.api.MemberRemovedEvent
import org.jbb.swanframework.eventbus.DomainEventBusListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
internal class RemovedMemberPrivilegesCleaner(private val repository: PrivilegeRepository)
    : DomainEventBusListener {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @Subscribe
    fun removePrivilegesForMember(event: MemberRemovedEvent) {
        log.debug("Remove privileges entities for member id {}", event.memberId)
        repository.findAllByMemberId(event.memberId).forEach { repository.delete(it) }
    }
}