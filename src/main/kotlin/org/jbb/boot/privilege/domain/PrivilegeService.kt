/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import org.jbb.boot.privilege.api.Privilege
import org.jbb.boot.privilege.api.PrivilegeFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.dryrun.DryRunContext
import org.springframework.stereotype.Service

@Service
internal class PrivilegeService(
        private val repository: PrivilegeRepository,
        private val eventEmitter: PrivilegeEventEmitter,
        private val dryRunContext: DryRunContext
) : PrivilegeFacade {

    override fun updatePrivileges(memberId: MemberId, privileges: Set<Privilege>): Set<Privilege> {
        dryRunContext.completeIfDryRun()
        val currentPrivilegeEntities = repository.findAllByMemberId(memberId)
        val currentPrivileges = currentPrivilegeEntities.map { it.privilege }.toSet()
        currentPrivilegeEntities
                .filterNot { privileges.contains(it.privilege) }
                .forEach {
                    repository.delete(it)
                    eventEmitter.sendAboutRemoval(it)
                }
        privileges.filter { !currentPrivileges.contains(it) }
                .forEach {
                    getEntity(memberId, it).also {
                        repository.save(it)
                        eventEmitter.sendAboutAddition(it)
                    }
                }
        return privileges
    }

    override fun getPrivilegesForMember(memberId: MemberId): Set<Privilege> =
            repository.findAllByMemberId(memberId)
                    .map { it.privilege }.toSet()

    private fun getEntity(memberId: MemberId, privilege: Privilege): PrivilegeEntity =
            PrivilegeEntity(
                    memberId = memberId,
                    privilege = privilege
            )

}