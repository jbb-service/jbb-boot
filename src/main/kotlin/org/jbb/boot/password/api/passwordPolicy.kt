/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.api

import arrow.core.Either
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue
import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min

// Operations
interface PasswordPolicyFacade {

    fun getPasswordPolicy(): PasswordPolicy

    fun getBlacklist(): PasswordBlacklist

    fun changePasswordPolicy(passwordPolicy: PasswordPolicy):
            Either<ChangePasswordPolicyFailed, PasswordPolicy>

    fun putBlacklist(blacklist: PasswordBlacklist)
}

// Errors
class ChangePasswordPolicyFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
data class PasswordPolicy(
        @field:Valid val length: LengthRules,
        @field:Valid val characterRules: CharacterRules,
        @field:Valid val forbiddenRules: ForbiddenRules,
        @field:Valid val expirationRules: ExpirationRules,
)

data class LengthRules(
        @field:Min(1) val min: Int,
        @field:Min(1) val max: Int?
)

data class CharacterRules(
        @field:Min(1) @field:Max(4) val requiredTypes: Int,
        @field:Valid val requiredCharacters: RequiredCharacterRules,
)

data class RequiredCharacterRules(
        @field:Min(0) val lowerCase: Int,
        @field:Min(0) val upperCase: Int,
        @field:Min(0) val digit: Int,
        @field:Min(0) val special: Int,
)

data class ForbiddenRules(
        val rejectUsername: Boolean,
        @field:Min(0) val rejectLastPasswords: Int,
        val useBlacklist: Boolean,
        val blacklistMatchBackwards: Boolean,
)

data class ExpirationRules(@field:Min(1) val days: Int?)

data class PasswordBlacklist(val passwords: List<String>)

// Events
class PasswordPolicyChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "PasswordPolicyChangedEvent() ${super.toString()}"
    }
}

class PasswordBlacklistChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "PasswordBlacklistChangedEvent() ${super.toString()}"
    }
}