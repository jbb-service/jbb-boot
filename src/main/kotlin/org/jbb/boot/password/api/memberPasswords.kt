/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.api

import arrow.core.Either
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue

// Operations
interface MemberPasswordFacade {

    fun verifyMemberPassword(memberId: MemberId, password: CharArray):
            Either<InvalidCurrentPassword, Unit>

    fun getPasswordExpirationReason(memberId: MemberId): PasswordExpirationReason?

    fun validatePasswordForNewMember(password: CharArray): List<ValidationIssue>

    fun getPasswordHash(memberId: MemberId): String?

    fun changePassword(
            memberId: MemberId,
            newPassword: CharArray
    ): Either<ChangePasswordFailed, Unit>

    fun setExpirationStatus(memberId: MemberId, expired: Boolean)
}

// Errors
class InvalidCurrentPassword : RuntimeException()

class ChangePasswordFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
enum class PasswordExpirationReason {
    POLICY, EXPLICIT_EXPIRATION
}

// Events
class PasswordChangedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "PasswordChangedEvent(memberId='$memberId') ${super.toString()}"
    }
}

class PasswordExpirationSetEvent(val memberId: MemberId, val expired: Boolean) : DomainEvent() {
    override fun toString(): String {
        return "PasswordExpirationSetEvent(memberId='$memberId', expired=$expired) ${super.toString()}"
    }
}