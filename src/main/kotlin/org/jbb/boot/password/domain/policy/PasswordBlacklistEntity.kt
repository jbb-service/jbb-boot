/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain.policy

import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "JBB_PASSWORD_BLACKLIST")
internal data class PasswordBlacklistEntity(
        @field:Column(name = "password")
        val password: String,
) : BaseEntity()