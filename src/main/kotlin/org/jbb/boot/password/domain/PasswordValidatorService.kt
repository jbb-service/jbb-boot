/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain

import org.cryptacular.bean.BCryptHashBean
import org.jbb.boot.password.api.*
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.security.BCRYPT_STRENGTH
import org.passay.*
import org.passay.dictionary.ArrayWordList
import org.passay.dictionary.Dictionary
import org.passay.dictionary.WordListDictionary
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

internal data class MemberData(val memberId: MemberId, val username: String)

internal sealed interface PolicyError

internal data class TooShort(val min: Int) : PolicyError
internal data class TooLong(val max: Int) : PolicyError
internal data class InsufficientAmountOfCharacterTypes(val requiredTypes: Int) : PolicyError
internal data class InsufficientLowerCase(val required: Int) : PolicyError
internal data class InsufficientUpperCase(val required: Int) : PolicyError
internal data class InsufficientDigit(val required: Int) : PolicyError
internal data class InsufficientSpecial(val required: Int) : PolicyError
internal object UsernameDetected : PolicyError
internal data class LastPasswordDetected(val lastPasswords: Int) : PolicyError
internal data class WordFromBlackListDetected(val word: String) : PolicyError
internal data class ReversedWordFromBlackListDetected(val word: String) : PolicyError

@Component
internal class PasswordValidatorService(
        private val passwordPolicyFacade: PasswordPolicyFacade,
        private val passwordRepository: PasswordRepository,
) {

    fun validateNewPasswordForMember(memberData: MemberData, password: CharArray):
            List<PolicyError> = validatePassword(memberData, password)

    fun validatePasswordForNewMember(password: CharArray):
            List<PolicyError> = validatePassword(memberData = null, password)

    private fun validatePassword(memberData: MemberData?, password: CharArray): List<PolicyError> {
        val passwordPolicy = passwordPolicyFacade.getPasswordPolicy()
        val passwordData = passwordPolicy.buildPasswordData(memberData, password)
        return PasswordValidator(passwordPolicy.toInternalRules(password))
                .validate(passwordData).details.map { it.toPolicyError(passwordPolicy) }
    }

    private fun PasswordPolicy.buildPasswordData(
            memberData: MemberData?,
            password: CharArray
    ): PasswordData {
        val passwordData = memberData?.let { PasswordData(it.username, password.concatToString()) }
                ?: PasswordData(password.concatToString())
        val passwordReferences = this.forbiddenRules.rejectLastPasswords.takeIf { it > 0 }
                ?.let { memberData }
                ?.let {
                    passwordRepository.findAllByMemberIdOrderByCreatedAtDesc(
                            it.memberId,
                            PageRequest.of(0, this.forbiddenRules.rejectLastPasswords)
                    )
                }
                ?.map { PasswordData.HistoricalReference(it.password) } ?: emptyList()
        passwordData.passwordReferences = passwordReferences
        return passwordData
    }

    private fun PasswordPolicy.toInternalRules(password: CharArray): List<Rule> =
            this.length.toInternalRules() +
                    this.characterRules.toInternalRules() +
                    this.forbiddenRules.toInternalRules(password)

    private fun LengthRules.toInternalRules(): List<Rule> = listOf(
            LengthRule(
                    this.min,
                    this.max ?: Integer.MAX_VALUE
            )
    )

    private fun CharacterRules.toInternalRules(): List<Rule> =
            listOfNotNull(
                    this.takeIf { it.requiredTypes > 1 }
                            ?.let {
                                CharacterCharacteristicsRule(
                                        it.requiredTypes, listOf(
                                        CharacterRule(EnglishCharacterData.LowerCase, 1),
                                        CharacterRule(EnglishCharacterData.UpperCase, 1),
                                        CharacterRule(EnglishCharacterData.Digit, 1),
                                        CharacterRule(EnglishCharacterData.Special, 1)
                                )
                                ).also { it.reportRuleFailures = false }
                            },
                    this.requiredCharacters.lowerCase.takeIf { it > 0 }
                            ?.let { CharacterRule(EnglishCharacterData.LowerCase, it) },
                    this.requiredCharacters.upperCase.takeIf { it > 0 }
                            ?.let { CharacterRule(EnglishCharacterData.UpperCase, it) },
                    this.requiredCharacters.digit.takeIf { it > 0 }
                            ?.let { CharacterRule(EnglishCharacterData.Digit, it) },
                    this.requiredCharacters.special.takeIf { it > 0 }
                            ?.let { CharacterRule(EnglishCharacterData.Special, it) },
            )

    private fun ForbiddenRules.toInternalRules(password: CharArray): List<Rule> = listOfNotNull(
            this.takeIf { it.rejectUsername }?.let { UsernameRule().apply { } },
            this.takeIf { it.rejectLastPasswords > 0 }?.takeIf { password.isNotEmpty() }
                    ?.let { DigestHistoryRule(BCryptHashBean(BCRYPT_STRENGTH)) },
            this.takeIf { it.useBlacklist }?.let {
                DictionaryRule(buildDictionary())
                        .also { it.isMatchBackwards = this.blacklistMatchBackwards }
            },
    )

    private fun buildDictionary(): Dictionary = WordListDictionary(
            ArrayWordList(passwordPolicyFacade.getBlacklist().passwords.toTypedArray())
    )

    private fun RuleResultDetail.toPolicyError(passwordPolicy: PasswordPolicy) =
            when (this.errorCode) {
                LengthRule.ERROR_CODE_MIN -> TooShort(passwordPolicy.length.min)
                LengthRule.ERROR_CODE_MAX -> TooLong(passwordPolicy.length.max!!)
                CharacterCharacteristicsRule.ERROR_CODE -> InsufficientAmountOfCharacterTypes(
                        passwordPolicy.characterRules.requiredTypes
                )
                EnglishCharacterData.LowerCase.errorCode -> InsufficientLowerCase(passwordPolicy.characterRules.requiredCharacters.lowerCase)
                EnglishCharacterData.UpperCase.errorCode -> InsufficientUpperCase(passwordPolicy.characterRules.requiredCharacters.upperCase)
                EnglishCharacterData.Digit.errorCode -> InsufficientDigit(passwordPolicy.characterRules.requiredCharacters.digit)
                EnglishCharacterData.Special.errorCode -> InsufficientSpecial(passwordPolicy.characterRules.requiredCharacters.special)
                UsernameRule.ERROR_CODE -> UsernameDetected
                HistoryRule.ERROR_CODE -> LastPasswordDetected(passwordPolicy.forbiddenRules.rejectLastPasswords)
                DictionaryRule.ERROR_CODE -> WordFromBlackListDetected(this.parameters["matchingWord"] as String)
                DictionaryRule.ERROR_CODE_REVERSED -> ReversedWordFromBlackListDetected(this.parameters["matchingWord"] as String)
                else -> throw IllegalStateException("Unknown error code: ${this.errorCode}. RuleResultDetail: $this")
            }

}