/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.domain.policy

import arrow.core.Either
import org.jbb.boot.password.api.*
import org.jbb.boot.password.domain.policy.PasswordPolicyTranslator.toEntities
import org.jbb.boot.password.domain.policy.PasswordPolicyTranslator.toEntity
import org.jbb.boot.password.domain.policy.PasswordPolicyTranslator.toModel
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.validation.ValidationService
import org.springframework.stereotype.Service

@Service
internal class PasswordPolicyService(
        private val policyRepository: PasswordPolicyRepository,
        private val blacklistRepository: PasswordBlacklistRepository,
        private val validationService: ValidationService,
        private val domainEventBus: DomainEventBus,
        private val dryRunContext: DryRunContext
) : PasswordPolicyFacade {

    override fun getPasswordPolicy(): PasswordPolicy =
            toModel(policyRepository.findAll().first())

    override fun getBlacklist(): PasswordBlacklist =
            toModel(blacklistRepository.findAllByOrderByPasswordAsc())

    override fun changePasswordPolicy(passwordPolicy: PasswordPolicy):
            Either<ChangePasswordPolicyFailed, PasswordPolicy> {
        val issues = validationService.validate(passwordPolicy)
        if (issues.isNotEmpty()) {
            return Either.Left(ChangePasswordPolicyFailed(issues))
        }
        dryRunContext.completeIfDryRun()

        val currentPolicy = policyRepository.findAll().firstOrNull() ?: toEntity(passwordPolicy)
        currentPolicy.run {
            minLength = passwordPolicy.length.min
            maxLength = passwordPolicy.length.max
            requiredCharacterTypes = passwordPolicy.characterRules.requiredTypes
            requiredLowerCase = passwordPolicy.characterRules.requiredCharacters.lowerCase
            requiredUpperCase = passwordPolicy.characterRules.requiredCharacters.upperCase
            requiredDigit = passwordPolicy.characterRules.requiredCharacters.digit
            requiredSpecial = passwordPolicy.characterRules.requiredCharacters.special
            rejectUsername = passwordPolicy.forbiddenRules.rejectUsername
            rejectLastPasswords = passwordPolicy.forbiddenRules.rejectLastPasswords
            expirationDays = passwordPolicy.expirationRules.days
            blacklistEnabled = passwordPolicy.forbiddenRules.useBlacklist
            blacklistMatchBackwards = passwordPolicy.forbiddenRules.blacklistMatchBackwards
        }
        policyRepository.save(currentPolicy)
        domainEventBus.post(PasswordPolicyChangedEvent())
        return Either.Right(toModel(currentPolicy))
    }

    override fun putBlacklist(blacklist: PasswordBlacklist) {
        dryRunContext.completeIfDryRun()
        blacklistRepository.deleteAll()
        blacklistRepository.saveAll(toEntities(blacklist))
        domainEventBus.post(PasswordBlacklistChangedEvent())
    }
}