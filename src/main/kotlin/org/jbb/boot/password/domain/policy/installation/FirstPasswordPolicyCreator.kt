/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain.policy.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_0_1
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.boot.password.api.*
import org.jbb.swanframework.arrow.assertRight
import org.springframework.stereotype.Component

@Component
internal class FirstPasswordPolicyCreator(
        private val passwordPolicyFacade: PasswordPolicyFacade
) : InstallationAction<Nothing> {

    override fun name() = "Creating password policy"

    override fun fromVersion(): Version = VERSION_0_0_1

    override fun install(context: Nothing?): Either<InstallationAbortedDueToActionRequired, Unit> {
        passwordPolicyFacade.changePasswordPolicy(
                PasswordPolicy(
                        length = LengthRules(min = 4, max = 128),
                        characterRules = CharacterRules(
                                requiredTypes = 1,
                                requiredCharacters = RequiredCharacterRules(
                                        lowerCase = 0,
                                        upperCase = 0,
                                        digit = 0,
                                        special = 0
                                )
                        ),
                        forbiddenRules = ForbiddenRules(
                                rejectUsername = false,
                                rejectLastPasswords = 0,
                                useBlacklist = false,
                                blacklistMatchBackwards = false
                        ),
                        expirationRules = ExpirationRules(days = null)
                )
        ).assertRight()

        passwordPolicyFacade.putBlacklist(
                PasswordBlacklist(
                        listOf(
                                "123456", "123456789", "password", "qwerty", "12345678",
                                "12345", "123123", "111111", "1234", "1234567890"
                        )
                )
        )
        return Either.Right(Unit)
    }


}