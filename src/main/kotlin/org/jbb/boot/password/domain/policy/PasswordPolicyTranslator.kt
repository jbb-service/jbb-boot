/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.domain.policy

import org.jbb.boot.password.api.*

internal object PasswordPolicyTranslator {

    fun toModel(entity: PasswordPolicyEntity): PasswordPolicy =
            PasswordPolicy(
                    length = LengthRules(entity.minLength, entity.maxLength),
                    characterRules = CharacterRules(
                            requiredTypes = entity.requiredCharacterTypes,
                            requiredCharacters = RequiredCharacterRules(
                                    lowerCase = entity.requiredLowerCase,
                                    upperCase = entity.requiredUpperCase,
                                    digit = entity.requiredDigit,
                                    special = entity.requiredSpecial
                            )
                    ),
                    forbiddenRules = ForbiddenRules(
                            rejectUsername = entity.rejectUsername,
                            rejectLastPasswords = entity.rejectLastPasswords,
                            useBlacklist = entity.blacklistEnabled,
                            blacklistMatchBackwards = entity.blacklistMatchBackwards,
                    ),
                    expirationRules = ExpirationRules(days = entity.expirationDays)
            )

    fun toEntity(policy: PasswordPolicy): PasswordPolicyEntity =
            PasswordPolicyEntity(
                    minLength = policy.length.min,
                    maxLength = policy.length.max,
                    requiredCharacterTypes = policy.characterRules.requiredTypes,
                    requiredLowerCase = policy.characterRules.requiredCharacters.lowerCase,
                    requiredUpperCase = policy.characterRules.requiredCharacters.upperCase,
                    requiredDigit = policy.characterRules.requiredCharacters.digit,
                    requiredSpecial = policy.characterRules.requiredCharacters.special,
                    rejectUsername = policy.forbiddenRules.rejectUsername,
                    rejectLastPasswords = policy.forbiddenRules.rejectLastPasswords,
                    expirationDays = policy.expirationRules.days,
                    blacklistEnabled = policy.forbiddenRules.useBlacklist,
                    blacklistMatchBackwards = policy.forbiddenRules.blacklistMatchBackwards,
            )

    fun toModel(blacklistEntities: List<PasswordBlacklistEntity>): PasswordBlacklist =
            PasswordBlacklist(passwords = blacklistEntities.map { it.password })

    fun toEntities(blacklist: PasswordBlacklist): List<PasswordBlacklistEntity> =
            blacklist.passwords.map { PasswordBlacklistEntity(it) }
}