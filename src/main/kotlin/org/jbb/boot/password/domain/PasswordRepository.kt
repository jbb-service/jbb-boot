/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain

import org.jbb.swanframework.application.MemberId
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
internal interface PasswordRepository : PagingAndSortingRepository<PasswordEntity?, String?> {
    @Query(
            "select p from PasswordEntity p where p.memberId = :memberId and " +
                    "p.applicableSince = (select max(x.applicableSince) from PasswordEntity x where x.memberId = p.memberId) "
    )
    fun findTheNewestByMemberId(@Param("memberId") memberId: MemberId): PasswordEntity?
    fun findAllByMemberId(memberId: MemberId): List<PasswordEntity>
    fun findAllByMemberIdOrderByCreatedAtDesc(
            memberId: MemberId,
            pageable: Pageable = Pageable.unpaged()
    ): List<PasswordEntity>

    fun countByMemberId(memberId: MemberId): Long
}