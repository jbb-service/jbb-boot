/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain.policy

import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Entity
@Table(name = "JBB_PASSWORD_POLICIES")
internal data class PasswordPolicyEntity(
        @field:Min(1) @field:Column(name = "min_length")
        var minLength: Int,

        @field:Min(1) @field:Column(name = "max_length", nullable = true)
        var maxLength: Int? = null,

        @field:Min(1) @field:Max(4) @field:Column(name = "required_character_types")
        var requiredCharacterTypes: Int,

        @field:Min(0) @field:Column(name = "required_lower_case")
        var requiredLowerCase: Int,

        @field:Min(0) @field:Column(name = "required_upper_case")
        var requiredUpperCase: Int,

        @field:Min(0) @field:Column(name = "required_digit")
        var requiredDigit: Int,

        @field:Min(0) @field:Column(name = "required_special")
        var requiredSpecial: Int,

        @field:Column(name = "reject_username")
        var rejectUsername: Boolean,

        @field:Min(0) @field:Column(name = "reject_last_passwords")
        var rejectLastPasswords: Int,

        @field:Min(1) @field:Column(name = "expiration_days", nullable = true)
        var expirationDays: Int?,

        @field:Column(name = "blacklist_enabled")
        var blacklistEnabled: Boolean,

        @field:Column(name = "blacklist_match_backwards")
        var blacklistMatchBackwards: Boolean,
) : BaseEntity()