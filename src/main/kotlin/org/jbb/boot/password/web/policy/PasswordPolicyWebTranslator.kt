/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.web.policy

import org.jbb.boot.password.api.*

internal object PasswordPolicyWebTranslator {

    fun toDto(policy: PasswordPolicy): PasswordPolicyDto = PasswordPolicyDto(
            length = PasswordLength(min = policy.length.min, max = policy.length.max),
            requiredCharacterTypes = policy.characterRules.requiredTypes,
            requiredCharacters = RequiredCharacters(
                    lowerCase = policy.characterRules.requiredCharacters.lowerCase,
                    upperCase = policy.characterRules.requiredCharacters.upperCase,
                    digit = policy.characterRules.requiredCharacters.digit,
                    special = policy.characterRules.requiredCharacters.special
            ),
            rejectUsername = policy.forbiddenRules.rejectUsername,
            rejectLastPasswords = policy.forbiddenRules.rejectLastPasswords,
            useBlacklist = policy.forbiddenRules.useBlacklist,
            expireAfterDays = policy.expirationRules.days,
    )

    fun toModel(
            dto: PasswordPolicyDto,
            blacklistMatchBackwards: Boolean
    ): PasswordPolicy = PasswordPolicy(
            length = LengthRules(dto.length.min, dto.length.max),
            characterRules = CharacterRules(
                    requiredTypes = dto.requiredCharacterTypes,
                    requiredCharacters = RequiredCharacterRules(
                            lowerCase = dto.requiredCharacters.lowerCase,
                            upperCase = dto.requiredCharacters.upperCase,
                            digit = dto.requiredCharacters.digit,
                            special = dto.requiredCharacters.special
                    )
            ),
            forbiddenRules = ForbiddenRules(
                    rejectUsername = dto.rejectUsername,
                    rejectLastPasswords = dto.rejectLastPasswords,
                    useBlacklist = dto.useBlacklist,
                    blacklistMatchBackwards = blacklistMatchBackwards
            ),
            expirationRules = ExpirationRules(days = dto.expireAfterDays)
    )
}