/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import org.jbb.boot.password.api.ChangePasswordFailed
import org.jbb.boot.password.api.ChangePasswordPolicyFailed
import org.jbb.boot.password.api.InvalidCurrentPassword
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@Order(DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice(annotations = [RestController::class])
internal class PasswordRestExceptionHandler {

    @ExceptionHandler(InvalidCurrentPassword::class)
    fun handle(ex: InvalidCurrentPassword): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(PasswordErrorInfo.INVALID_PASSWORD_FOR_CURRENT_MEMBER)
    }

    @ExceptionHandler(ChangePasswordFailed::class)
    fun handle(ex: ChangePasswordFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(PasswordErrorInfo.PASSWORD_UPDATE_FAILED, ex)
    }

    @ExceptionHandler(ChangePasswordPolicyFailed::class)
    fun handle(ex: ChangePasswordPolicyFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(PasswordErrorInfo.PASSWORD_POLICY_UPDATE_FAILED, ex)
    }
}