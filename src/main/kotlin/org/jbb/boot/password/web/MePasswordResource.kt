/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.password.api.ChangePasswordFailed
import org.jbb.boot.password.api.InvalidCurrentPassword
import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.jbb.swanframework.security.password.ExpiredPasswordIgnored
import org.jbb.swanframework.web.HttpRequestContext
import org.jbb.swanframework.web.RestConstants.API_V1
import org.jbb.swanframework.web.RestConstants.ME
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = MEMBER_PASSWORDS_API)
@RequestMapping(API_V1 + ME + PASSWORD, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class MePasswordResource(
        private val memberPasswordFacade: MemberPasswordFacade,
        private val httpRequestContext: HttpRequestContext,
) {

    @DryRunnable
    @ExpiredPasswordIgnored
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AUTHENTICATED)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Update my password",
            description = "Updates a password for a current member"
    )
    @Throws(InvalidCurrentPassword::class, ChangePasswordFailed::class)
    fun updateMyPassword(@RequestBody updateMyPasswordDto: UpdateMyPasswordDto) {
        val currentMember = httpRequestContext.getCurrentMember()
        memberPasswordFacade.verifyMemberPassword(
                currentMember!!.memberId,
                updateMyPasswordDto.currentPassword?.toCharArray() ?: CharArray(0)
        ).assertRight()
        memberPasswordFacade.changePassword(
                currentMember.memberId,
                updateMyPasswordDto.newPassword?.toCharArray() ?: CharArray(0)
        ).assertRight()
    }
}