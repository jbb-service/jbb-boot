/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.boot.password.api.ChangePasswordFailed
import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = MEMBER_PASSWORDS_API)
@RequestMapping(
        value = [API_V1 + MEMBERS + MEMBER_ID + PASSWORD],
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class PasswordResource(
        private val memberReadFacade: MemberReadFacade,
        private val memberPasswordFacade: MemberPasswordFacade
) {

    @DryRunnable
    @PatchMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Update and/or expire password for a member",
            description = "Updates and/or expires a password for a member"
    )
    @Throws(MemberNotFound::class, ChangePasswordFailed::class)
    fun updatePassword(
            @PathVariable(MEMBER_ID_VAR) memberId: MemberId,
            @RequestBody updatePasswordDto: UpdatePasswordDto
    ) {
        memberReadFacade.getMemberById(memberId).assertRight()
        updatePasswordDto.newPassword
                ?.let {
                    memberPasswordFacade.changePassword(memberId, it.toCharArray()).assertRight()
                }
        updatePasswordDto.expired
                ?.let { memberPasswordFacade.setExpirationStatus(memberId, it) }
    }
}