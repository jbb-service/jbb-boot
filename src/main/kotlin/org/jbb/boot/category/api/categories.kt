/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.api

import arrow.core.Either
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

// Operations
interface CategoryReadFacade {

    fun getCategory(categoryId: CategoryId): Category?

    fun getCategoryBySlug(slug: String): Category?

    fun getCategoriesTree(): List<CategoryWithChildren>

    fun getCategoriesDFS(): List<Category>

}

interface CategoryWriteFacade {

    fun createCategory(command: CreateCategoryCommand): Either<CategoryValidationFailed, Category>

    fun updateCategoryDetails(
            categoryId: CategoryId,
            details: CategoryDetails
    ): Either<CategoryUpdateFailed, Category>

    fun updateCategoryLocation(
            categoryId: CategoryId,
            location: CategoryLocation
    ): Either<CategoryUpdateFailed, Category>

    fun deleteCategory(
            categoryId: CategoryId,
            cascade: Boolean = false
    ): Either<CategoryNotFound, Unit>

}

// Errors
sealed class CategoryUpdateFailed : RuntimeException() {

    class CategoryNotFound(val categoryId: CategoryId) : CategoryUpdateFailed()

    class CategoryValidationFailed(override val issues: List<ValidationIssue>) :
            CategoryUpdateFailed(), ValidationFailed
}

class CategoryNotFound(val categoryId: CategoryId) : RuntimeException()

class CategoryValidationFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
typealias CategoryId = String

data class Category(
        val categoryId: CategoryId,
        val details: CategoryDetails,
        val location: CategoryLocation,
        val childCategoryIds: List<CategoryId>
)

data class CategoryWithChildren(
        val categoryId: CategoryId,
        val details: CategoryDetails,
        val location: CategoryLocation,
        val childCategories: List<CategoryWithChildren>
)

data class CategoryDetails(
        @field:NotBlank
        val name: String,
        @field:NotBlank
        val slug: String,
        val description: String?,
)

data class CategoryLocation(
        val parentCategoryId: CategoryId?,
        @field:Min(0)
        val position: Int,
)

data class CreateCategoryCommand(
        @field:Valid val details: CategoryDetails,
        val parentCategoryId: CategoryId?,
)

// Events
class CategoryCreatedEvent(val categoryId: CategoryId) : DomainEvent() {
    override fun toString(): String {
        return "CategoryCreatedEvent(categoryId='$categoryId') ${super.toString()}"
    }
}

class CategoryDetailsUpdatedEvent(val categoryId: CategoryId) : DomainEvent() {
    override fun toString(): String {
        return "CategoryDetailsUpdatedEvent(categoryId='$categoryId') ${super.toString()}"
    }
}

class CategoryLocationUpdatedEvent(val categoryId: CategoryId) : DomainEvent() {
    override fun toString(): String {
        return "CategoryLocationUpdatedEvent(categoryId='$categoryId') ${super.toString()}"
    }
}

class CategoryRemovedEvent(val categoryId: CategoryId) : DomainEvent() {
    override fun toString(): String {
        return "CategoryRemovedEvent(categoryId='$categoryId') ${super.toString()}"
    }
}