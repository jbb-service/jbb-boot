/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.domain

import org.jbb.boot.category.api.Category
import org.jbb.boot.category.api.CategoryDetails
import org.jbb.boot.category.api.CategoryLocation
import org.jbb.boot.category.api.CategoryWithChildren
import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.*

@Entity
@Table(name = "JBB_CATEGORY")
internal data class CategoryEntity(
        @field:Column(name = "name")
        var name: String,

        @field:Column(name = "slug")
        var slug: String,

        @field:Column(name = "description")
        var description: String? = null,

        @field:Column(name = "position")
        var position: Int,

        @field:ManyToOne(fetch = FetchType.LAZY)
        var parentCategory: CategoryEntity? = null,

        @field:OneToMany(mappedBy = "parentCategory", fetch = FetchType.EAGER)
        @field:OrderBy("position")
        var childrenCategories: List<CategoryEntity> = emptyList(),

        ) : BaseEntity() {

    fun toModel(): Category = Category(
            categoryId = id!!,
            details = CategoryDetails(
                    name = name,
                    slug = slug,
                    description = description
            ),
            location = CategoryLocation(
                    parentCategoryId = parentCategory?.id,
                    position = position
            ),
            childCategoryIds = childrenCategories.map { it.id!! }
    )

    fun toWithChildrenModel(): CategoryWithChildren = CategoryWithChildren(
            categoryId = id!!,
            details = CategoryDetails(
                    name = name,
                    slug = slug,
                    description = description
            ),
            location = CategoryLocation(
                    parentCategoryId = parentCategory?.id,
                    position = position
            ),
            childCategories = childrenCategories.map { it.toWithChildrenModel() }
    )

    override fun toString(): String {
        return "CategoryEntity(name='$name', slug='$slug', description=$description, position=$position) ${super.toString()}"
    }

}