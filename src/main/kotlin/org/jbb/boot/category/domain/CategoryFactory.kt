/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.domain

import org.jbb.boot.category.api.CreateCategoryCommand
import org.springframework.stereotype.Component

@Component
internal class CategoryFactory(private val repository: CategoryRepository) {

    fun toEntity(command: CreateCategoryCommand, parentCategory: CategoryEntity?) =
            CategoryEntity(
                    name = command.details.name,
                    slug = command.details.slug,
                    description = command.details.description,
                    position = parentCategory?.childrenCategories?.size
                            ?: repository.findTopCategories().size,
                    parentCategory = parentCategory,
                    childrenCategories = emptyList()
            )
}