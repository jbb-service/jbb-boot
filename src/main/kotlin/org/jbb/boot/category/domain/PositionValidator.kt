/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.domain

import org.jbb.boot.category.api.CategoryId
import org.jbb.boot.category.api.CategoryLocation
import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
internal class PositionValidator(private val categoryRepository: CategoryRepository) {

    fun validate(
            location: CategoryLocation,
            inContextOfCategory: CategoryEntity
    ): ValidationIssue? {
        val siblings = siblings(location.parentCategoryId)
        val movingCategory =
                if (inContextOfCategory.parentCategory?.id != location.parentCategoryId) 1 else 0
        if (location.position >= siblings.size + movingCategory) {
            return ValidationIssue("position", "too-large", "Position is too large")
        }
        return null
    }

    private fun siblings(parentCategoryId: CategoryId?) =
            parentCategoryId?.let { categoryRepository.findByIdOrNull(it) }
                    ?.childrenCategories ?: categoryRepository.findTopCategories()
}