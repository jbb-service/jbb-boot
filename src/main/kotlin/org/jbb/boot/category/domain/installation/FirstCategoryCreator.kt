/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.domain.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_12_0
import org.jbb.boot.category.api.CategoryDetails
import org.jbb.boot.category.api.CreateCategoryCommand
import org.jbb.boot.category.domain.CategoryService
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.swanframework.arrow.assertRight
import org.springframework.stereotype.Component

@Component
internal class FirstCategoryCreator(
        private val categoryService: CategoryService
) : InstallationAction<Nothing> {

    override fun name() = "Creating category"

    override fun fromVersion(): Version = VERSION_0_12_0

    override fun install(context: Nothing?):
            Either<InstallationAbortedDueToActionRequired, Unit> {
        categoryService.createCategory(
                CreateCategoryCommand(
                        details = CategoryDetails(
                                name = "Example category",
                                slug = "example-category",
                                description = "This is an example category"
                        ),
                        parentCategoryId = null
                )
        ).assertRight()
        return Either.Right(Unit)
    }
}