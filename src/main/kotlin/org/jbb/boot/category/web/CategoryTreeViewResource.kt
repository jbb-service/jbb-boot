/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.category.api.CategoryReadFacade
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = CATEGORIES_API)
@RequestMapping(API_V1 + CATEGORIES_TREE_VIEW, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class CategoryTreeViewResource(private val categoryReadFacade: CategoryReadFacade) {

    @GetMapping
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "Get all categories (as a tree)",
            description = "Gets all categories with its children details"
    )
    fun getCategoriesTreeView(): List<CategoryWithChildrenDto> =
            categoryReadFacade.getCategoriesTree().map { it.toDto() }

}