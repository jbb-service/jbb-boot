/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.web

import org.jbb.boot.category.api.CategoryNotFound
import org.jbb.boot.category.api.CategoryUpdateFailed
import org.jbb.boot.category.api.CategoryValidationFailed
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class CategoryErrorInfo(
        override val status: HttpStatus,
        override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    CATEGORY_NOT_FOUND(
            HttpStatus.NOT_FOUND,
            "CTGY-100",
            "Category not found",
            setOf(CategoryNotFound::class.java, CategoryUpdateFailed.CategoryNotFound::class.java)
    ),
    CATEGORY_VALIDATION_FAILED(
            HttpStatus.BAD_REQUEST, "CTGY-101",
            "Category validation failed",
            setOf(
                    CategoryValidationFailed::class.java,
                    CategoryUpdateFailed.CategoryValidationFailed::class.java
            )
    );

}