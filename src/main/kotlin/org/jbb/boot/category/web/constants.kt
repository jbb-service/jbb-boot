/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.web

internal const val CATEGORIES_API = "Categories API"

internal const val CATEGORIES = "/categories"
internal const val CATEGORY_ID_VAR = "categoryId"
internal const val CATEGORY_ID = "/{$CATEGORY_ID_VAR}"

internal const val DETAILS = "/details"
internal const val LOCATION = "/location"
internal const val CATEGORIES_TREE_VIEW = "/categories-tree-view"
