/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.web

import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.boot.category.api.CategoryDetails
import org.jbb.boot.category.api.CategoryId
import org.jbb.boot.category.api.CategoryLocation
import org.jbb.boot.category.api.CreateCategoryCommand

@Schema(name = "Category")
internal data class CategoryDto(
        val categoryId: CategoryId,
        val details: CategoryDetailsDto,
        val location: CategoryLocationDto,
        val childCategoryIds: List<CategoryId>
)

@Schema(name = "CategoryWithChildren")
internal data class CategoryWithChildrenDto(
        val categoryId: CategoryId,
        val details: CategoryDetailsDto,
        val location: CategoryLocationDto,
        val childCategories: List<SubcategoryDto>
)

@Schema(name = "Subcategory")
internal data class SubcategoryDto(
        val categoryId: CategoryId,
        val details: CategoryDetailsDto,
        val location: CategoryLocationDto
)

@Schema(name = "CategoryDetails")
internal data class CategoryDetailsDto(
        val name: String,
        val slug: String,
        val description: String?,
) {
    fun toModel() = CategoryDetails(
            name = name,
            slug = slug,
            description = description
    )
}

@Schema(name = "CategoryLocation")
internal data class CategoryLocationDto(
        val parentCategoryId: CategoryId?,
        val position: Int,
) {
    fun toModel() = CategoryLocation(
            parentCategoryId = parentCategoryId,
            position = position
    )
}

@Schema(name = "CreateCategoryRequest")
internal data class CreateCategoryRequestDto(
        val details: CategoryDetailsDto,
        val parentCategoryId: CategoryId?,
) {
    fun toModel() = CreateCategoryCommand(
            details = CategoryDetails(
                    name = details.name,
                    slug = details.slug,
                    description = details.description,
            ),
            parentCategoryId = parentCategoryId

    )
}
