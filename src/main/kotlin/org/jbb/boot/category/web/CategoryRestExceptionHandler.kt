/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.web

import org.jbb.boot.category.api.CategoryNotFound
import org.jbb.boot.category.api.CategoryUpdateFailed
import org.jbb.boot.category.api.CategoryValidationFailed
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@Order(DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice(annotations = [RestController::class])
internal class CategoryRestExceptionHandler {

    @ExceptionHandler(CategoryNotFound::class)
    fun handle(ex: CategoryNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CategoryErrorInfo.CATEGORY_NOT_FOUND)
    }

    @ExceptionHandler(CategoryUpdateFailed.CategoryNotFound::class)
    fun handle(ex: CategoryUpdateFailed.CategoryNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CategoryErrorInfo.CATEGORY_NOT_FOUND)
    }

    @ExceptionHandler(CategoryValidationFailed::class)
    fun handle(ex: CategoryValidationFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CategoryErrorInfo.CATEGORY_VALIDATION_FAILED, ex)
    }

    @ExceptionHandler(CategoryUpdateFailed.CategoryValidationFailed::class)
    fun handle(ex: CategoryUpdateFailed.CategoryValidationFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CategoryErrorInfo.CATEGORY_VALIDATION_FAILED, ex)
    }

}