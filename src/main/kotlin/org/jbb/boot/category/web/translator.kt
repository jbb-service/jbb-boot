/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.web

import org.jbb.boot.category.api.Category
import org.jbb.boot.category.api.CategoryDetails
import org.jbb.boot.category.api.CategoryLocation
import org.jbb.boot.category.api.CategoryWithChildren


internal fun Category.toDto(): CategoryDto = CategoryDto(
        categoryId = categoryId,
        details = details.toDto(),
        location = location.toDto(),
        childCategoryIds = childCategoryIds
)

internal fun CategoryDetails.toDto(): CategoryDetailsDto = CategoryDetailsDto(
        name = this.name,
        slug = this.slug,
        description = this.description
)

internal fun CategoryLocation.toDto(): CategoryLocationDto = CategoryLocationDto(
        parentCategoryId = parentCategoryId,
        position = position
)

internal fun CategoryWithChildren.toDto(): CategoryWithChildrenDto = CategoryWithChildrenDto(
        categoryId = categoryId,
        details = details.toDto(),
        location = location.toDto(),
        childCategories = childCategories.map {
            SubcategoryDto(
                    categoryId = it.categoryId,
                    details = it.details.toDto(),
                    location = it.location.toDto(),
            )
        }
)