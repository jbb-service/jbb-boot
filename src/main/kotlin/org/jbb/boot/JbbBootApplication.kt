/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot

import com.github.zafarkhaja.semver.Version
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JbbBootApplication

fun main(args: Array<String>) {
    runApplication<JbbBootApplication>(*args)
}

val VERSION_0_0_0: Version = Version.valueOf("0.0.0")!!
val VERSION_0_0_1: Version = Version.valueOf("0.0.1")!!
val VERSION_0_1_0: Version = Version.valueOf("0.1.0")!!
val VERSION_0_5_0: Version = Version.valueOf("0.5.0")!!
val VERSION_0_9_0: Version = Version.valueOf("0.9.0")!!
val VERSION_0_10_0: Version = Version.valueOf("0.10.0")!!
val VERSION_0_12_0: Version = Version.valueOf("0.12.0")!!
val VERSION_0_14_0: Version = Version.valueOf("0.14.0")!!