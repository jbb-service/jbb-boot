/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain.user

import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.boot.privilege.api.Privilege
import org.jbb.boot.privilege.api.PrivilegeFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.security.SecurityConstants
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component

@Component
internal class UserRoleResolver(
        private val privilegeFacade: PrivilegeFacade,
        private val memberPasswordFacade: MemberPasswordFacade
) {

    fun getRoles(memberId: MemberId): Collection<GrantedAuthority> = setOfNotNull(
            resolveAdministratorAuth(memberId),
            resolvePasswordExpiredAuth(memberId),
    )

    private fun resolveAdministratorAuth(memberId: MemberId) =
            privilegeFacade.getPrivilegesForMember(memberId)
                    .takeIf { it.contains(Privilege.ADMINISTRATOR) }
                    ?.let { SimpleGrantedAuthority(SecurityConstants.ROLE_ADMINISTRATOR) }

    private fun resolvePasswordExpiredAuth(memberId: MemberId) =
            memberPasswordFacade.getPasswordExpirationReason(memberId)
                    ?.let { SimpleGrantedAuthority(SecurityConstants.ROLE_PASSWORD_EXPIRED) }

}