/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain.user

import org.jbb.boot.lockout.api.MemberLockFacade
import org.jbb.boot.member.domain.MemberEntity
import org.jbb.boot.member.domain.MemberRepository
import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.boot.signin.api.LoginType.*
import org.jbb.boot.signin.api.SignInSettingsFacade
import org.jbb.swanframework.security.MemberUser
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
internal class DefaultUserDetailsService(
        private val memberRepository: MemberRepository,
        private val signInSettingsFacade: SignInSettingsFacade,
        private val memberPasswordFacade: MemberPasswordFacade,
        private val memberLockFacade: MemberLockFacade,
        private val userRoleResolver: UserRoleResolver,
) : UserDetailsService {

    override fun loadUserByUsername(login: String): UserDetails {
        val member = findMember(login)
        val passwordHash: String = memberPasswordFacade.getPasswordHash(member.id!!)
                ?: throw IllegalStateException("Missing password hash for member '${member.username}'")
        return MemberUser(
                memberId = member.id!!,
                username = member.username,
                password = passwordHash,
                accountNonLocked = memberLockFacade.getMemberActiveLock(member.id!!)
                        ?.let { false } ?: true,
                authorities = userRoleResolver.getRoles(member.id!!)
        )
    }

    private fun findMember(login: String): MemberEntity {
        val signInSettings = signInSettingsFacade.getSignInSettings()

        val memberByUsername =
                if (signInSettings.loginType in listOf(USERNAME, USERNAME_OR_EMAIL)) {
                    when (signInSettings.caseSensitive) {
                        true -> memberRepository.findByUsername(login)
                        false -> memberRepository.findByUsernameIgnoreCase(login)
                    }
                } else {
                    null
                }

        if (memberByUsername != null) {
            return memberByUsername
        }

        val memberByEmail = if (signInSettings.loginType in listOf(EMAIL, USERNAME_OR_EMAIL)) {
            when (signInSettings.caseSensitive) {
                true -> memberRepository.findByEmail(login)
                false -> memberRepository.findByEmailIgnoreCase(login)
            }
        } else {
            null
        }

        return memberByEmail
                ?: throw UsernameNotFoundException("Member not found by login '$login'")
    }
}