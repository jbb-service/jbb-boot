/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import arrow.core.Either
import org.jbb.boot.member.api.*
import org.jbb.boot.member.domain.MemberTranslator.toEntity
import org.jbb.boot.member.domain.MemberTranslator.toModel
import org.jbb.boot.member.domain.MemberTranslator.toPublicModel
import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.pagination.CursorPage
import org.jbb.swanframework.validation.ValidationService
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
internal class MemberService(
        private val repository: MemberRepository,
        private val validationService: ValidationService,
        private val usernameUniquenessPolicy: UsernameUniquenessPolicy,
        private val emailUniquenessPolicy: EmailUniquenessPolicy,
        private val domainEventBus: DomainEventBus,
        private val memberPasswordFacade: MemberPasswordFacade,
        private val dryRunContext: DryRunContext
) : MemberReadFacade, MemberWriteFacade {

    override fun getMemberById(memberId: MemberId): Either<MemberNotFound, Member> =
            repository.findByIdOrNull(memberId)?.let { Either.Right(toModel(it)) }
                    ?: Either.Left(MemberNotFound(memberId))

    override fun findMembers(limit: Int, joinBefore: Instant?): CursorPage<PublicMember, Instant> {
        val results = repository.findTopByCreatedAtBeforeOrderByCreatedAtDesc(
                createdAtBefore = joinBefore ?: Instant.now(),
                pageable = PageRequest.of(0, limit)
        )
        return CursorPage(
                content = results.content.map { toPublicModel(it) },
                nextCursor = if (results.numberOfElements == limit) results.last()?.createdAt else null
        )
    }

    @Transactional
    override fun createMember(command: CreateMemberCommand): Either<MemberCreationFailed, Member> {
        val issues = validationService.validate(command.toModel()).toMutableList()
        usernameUniquenessPolicy.validateUsernameUniqueness(command.username)
                ?.let { issues.add(it) }
        emailUniquenessPolicy.validateEmailUniqueness(command.email, null)
                ?.let { issues.add(it) }
        issues.addAll(memberPasswordFacade.validatePasswordForNewMember(command.password))
        if (issues.isNotEmpty()) {
            return Either.Left(MemberCreationFailed(issues))
        }
        dryRunContext.completeIfDryRun()
        val newMember = repository.save(toEntity(command))
        domainEventBus.post(MemberRegisteredEvent(newMember.id!!))
        memberPasswordFacade.changePassword(newMember.id!!, command.password).assertRight()
        return Either.Right(toModel(newMember))
    }

    @Transactional
    override fun deleteMember(memberId: MemberId): Either<MemberNotFound, Unit> {
        val memberToRemove = repository.findByIdOrNull(memberId)
                ?: return Either.Left(MemberNotFound(memberId))
        dryRunContext.completeIfDryRun()
        repository.delete(memberToRemove)
        domainEventBus.post(MemberRemovedEvent(memberId))
        return Either.Right(Unit)
    }

    @Transactional
    override fun changeEmail(
            memberId: MemberId,
            newEmail: String
    ): Either<MemberUpdateFailed, Member> {
        val entity = repository.findByIdOrNull(memberId)
                ?: return Either.Left(MemberUpdateFailed.MemberNotFound(memberId))
        val updatedMember = toModel(entity).copy(email = newEmail)
        val issues = validationService.validate(updatedMember).toMutableList()
        emailUniquenessPolicy.validateEmailUniqueness(newEmail, memberId)?.let { issues.add(it) }
        if (issues.isNotEmpty()) {
            return Either.Left(MemberUpdateFailed.ChangeEmailFailed(issues))
        }
        dryRunContext.completeIfDryRun()
        entity.email = newEmail
        repository.save(entity)
        domainEventBus.post(EmailChangedEvent(memberId))
        return Either.Right(toModel(entity))
    }

    private fun CreateMemberCommand.toModel() = Member(
            memberId = "",
            username = this.username,
            email = this.email,
            joinedAt = Instant.now()
    )
}