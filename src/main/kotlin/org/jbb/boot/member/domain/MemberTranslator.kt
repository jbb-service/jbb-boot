/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.Member
import org.jbb.boot.member.api.PublicMember

internal object MemberTranslator {

    fun toModel(entity: MemberEntity): Member = Member(
            memberId = entity.id!!,
            username = entity.username,
            email = entity.email,
            joinedAt = entity.createdAt!!
    )

    fun toPublicModel(entity: MemberEntity): PublicMember = PublicMember(
            memberId = entity.id!!,
            username = entity.username,
            joinedAt = entity.createdAt!!
    )

    fun toEntity(createMemberCommand: CreateMemberCommand): MemberEntity = MemberEntity(
            username = createMemberCommand.username,
            email = createMemberCommand.email
    )
}