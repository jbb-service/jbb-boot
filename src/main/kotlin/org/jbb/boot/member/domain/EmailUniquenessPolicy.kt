/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.stereotype.Component

@Component
internal class EmailUniquenessPolicy(private val memberRepository: MemberRepository) {

    fun validateEmailUniqueness(
            email: String,
            editedMemberId: MemberId?
    ): ValidationIssue? = ValidationIssue(
            name = "email",
            errorCode = "email-already-used",
            message = "email is already used"
    ).takeIf { editedMemberId?.let { isEmailBusy(email, it) } ?: isEmailBusy(email) }

    private fun isEmailBusy(email: String, editedMemberId: MemberId): Boolean =
            memberRepository.findByEmailIgnoreCase(email)
                    .takeIf { editedMemberId != it?.id } != null

    private fun isEmailBusy(email: String): Boolean = memberRepository.findByEmailIgnoreCase(email) != null
}