/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.time.Instant

@Repository
interface MemberRepository : PagingAndSortingRepository<MemberEntity, String> {
    fun findByUsernameIgnoreCase(username: String): MemberEntity?

    fun findByUsername(username: String): MemberEntity?

    fun findByEmailIgnoreCase(email: String): MemberEntity?

    fun findByEmail(email: String): MemberEntity?

    fun findTopByCreatedAtBeforeOrderByCreatedAtDesc(
            createdAtBefore: Instant,
            pageable: Pageable
    ): Slice<MemberEntity>
}