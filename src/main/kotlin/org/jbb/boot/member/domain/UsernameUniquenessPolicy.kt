/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.stereotype.Component

@Component
internal class UsernameUniquenessPolicy(private val memberRepository: MemberRepository) {

    fun validateUsernameUniqueness(username: String): ValidationIssue? = ValidationIssue(
            name = "username",
            errorCode = "username-already-used",
            message = "username is already used"
    ).takeIf { isUsernameBusy(username) }

    fun isUsernameBusy(username: String): Boolean =
            memberRepository.findByUsernameIgnoreCase(username) != null
}