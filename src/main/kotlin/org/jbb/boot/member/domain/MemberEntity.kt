/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import org.hibernate.validator.constraints.Length
import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "JBB_MEMBERS")
data class MemberEntity(
        @field:NotBlank @field:Length(min = 4, max = 32)
        var username: String,
        @field:Email @field:NotBlank
        var email: String
) : BaseEntity()