/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

const val ME_API = "Me API"
const val MEMBERS_API = "Members API"
const val MEMBER_PROFILES_API = "Member profiles API"

const val EMAIL = "/email"
const val MEMBERS = "/members"
const val MEMBER_ID_VAR = "memberId"
const val MEMBER_ID = "/{$MEMBER_ID_VAR}"