/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.api.MemberUpdateFailed
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.boot.password.api.InvalidCurrentPassword
import org.jbb.boot.password.api.MemberPasswordFacade
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.jbb.swanframework.web.HttpRequestContext
import org.jbb.swanframework.web.RestConstants.API_V1
import org.jbb.swanframework.web.RestConstants.ME
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = MEMBER_PROFILES_API)
@RequestMapping(API_V1 + ME + EMAIL, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class MeEmailResource(
        private val memberPasswordFacade: MemberPasswordFacade,
        private val memberWriteFacade: MemberWriteFacade,
        private val httpRequestContext: HttpRequestContext
) {

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize(IS_AUTHENTICATED)
    @Operation(summary = "Update my email", description = "Updates an email for a current member")
    @Throws(InvalidCurrentPassword::class)
    fun updateMyEmail(@RequestBody updateMyEmailDto: UpdateMyEmailDto) {
        val currentMember = httpRequestContext.getCurrentMember()
        memberPasswordFacade.verifyMemberPassword(
                currentMember!!.memberId,
                updateMyEmailDto.currentPassword?.toCharArray() ?: CharArray(0)
        ).assertRight()
        changeEmail(currentMember.memberId, updateMyEmailDto.newEmail ?: "")
    }

    private fun changeEmail(memberId: MemberId, newEmail: String) {
        try {
            memberWriteFacade.changeEmail(memberId, newEmail).assertRight()
        } catch (ex: MemberUpdateFailed.MemberNotFound) {
            throw IllegalStateException(
                    "There is some inconsistency between member data source and email DS", ex
            )
        }
    }
}