/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web.me

import org.jbb.swanframework.web.HttpRequestContext
import org.springframework.stereotype.Component

@Component
internal class MeDataProvider(private val httpRequestContext: HttpRequestContext) {

    fun getMeData(): MeDataDto = MeDataDto(
            sessionId = httpRequestContext.getCurrentSessionId(),
            currentMember = getMemberContext()
    )

    private fun getMemberContext(): MemberContextDto? =
            httpRequestContext.getCurrentMember()
                    ?.let {
                        MemberContextDto(
                                memberId = it.memberId,
                                administrator = it.isAdministrator(),
                                hasPasswordExpired = it.hasPasswordExpired()
                        )
                    }
}