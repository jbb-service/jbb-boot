/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import org.jbb.boot.member.api.MemberCreationFailed
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberUpdateFailed
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@Order(DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice(annotations = [RestController::class])
internal class MemberRestExceptionHandler {

    @ExceptionHandler(MemberNotFound::class)
    fun handle(ex: MemberNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @ExceptionHandler(MemberUpdateFailed.MemberNotFound::class)
    fun handle(ex: MemberUpdateFailed.MemberNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @ExceptionHandler(MemberCreationFailed::class)
    fun handle(ex: MemberCreationFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(MemberErrorInfo.MEMBER_CREATION_FAILED, ex)
    }

    @ExceptionHandler(MemberUpdateFailed.ChangeEmailFailed::class)
    fun handle(ex: MemberUpdateFailed.ChangeEmailFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(MemberErrorInfo.EMAIL_UPDATE_FAILED, ex)
    }
}