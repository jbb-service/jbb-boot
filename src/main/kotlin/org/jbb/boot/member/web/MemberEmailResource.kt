/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.api.MemberUpdateFailed
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.boot.member.web.MemberWebTranslator.toDto
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = MEMBER_PROFILES_API)
@RequestMapping(
        API_V1 + MEMBERS + MEMBER_ID + EMAIL,
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class MemberEmailResource(private val memberWriteFacade: MemberWriteFacade) {

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(summary = "Update email for a member", description = "Updates an email for a member")
    @Throws(MemberUpdateFailed.MemberNotFound::class, MemberUpdateFailed.ChangeEmailFailed::class)
    fun updateEmail(
            @PathVariable(MEMBER_ID_VAR) memberId: MemberId,
            @RequestBody updateEmailDto: UpdateEmailDto
    ): MemberDto {
        return toDto(
                memberWriteFacade.changeEmail(memberId, updateEmailDto.newEmail ?: "")
                        .assertRight()
        )
    }
}