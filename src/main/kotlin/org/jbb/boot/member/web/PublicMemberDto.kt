/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.swanframework.application.MemberId
import java.time.Instant

@Schema(name = "PublicMember")
internal data class PublicMemberDto(
        val memberId: MemberId,
        val username: String,
        val joinedAt: Instant
)

@Schema(name = "PublicMemberPage")
internal data class PagePublicMemberDto(
        val parameters: PageParametersPublicMemberDto,
        val nextCursor: String?,
        val content: List<PublicMemberDto>
)

@Schema(name = "PublicMemberPageParameter")
internal data class PageParametersPublicMemberDto(
        val limit: Int?,
        val cursor: String?
)