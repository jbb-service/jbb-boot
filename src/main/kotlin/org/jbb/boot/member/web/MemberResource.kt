/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.api.MemberCreationFailed
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.boot.member.web.MemberWebTranslator.toCreateCommand
import org.jbb.boot.member.web.MemberWebTranslator.toDto
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.Instant
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern

@RestController
@Validated
@Tag(name = MEMBERS_API)
@RequestMapping(value = [API_V1 + MEMBERS], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class MemberResource(
        private val memberReadFacade: MemberReadFacade,
        private val memberWriteFacade: MemberWriteFacade
) {

    @GetMapping
    @PreAuthorize(PERMIT_ALL)
    @Operation(summary = "List members", description = "Finds a members")
    fun findMembers(
            @RequestParam("limit") @Min(1) @Max(100) limit: Int?,
            @RequestParam("cursor") @Pattern(regexp = "^[0-9]+$") cursor: String?
    ): PagePublicMemberDto {
        val joinBefore = cursor?.let { Instant.ofEpochSecond(it.toLong()) }
        val members = memberReadFacade.findMembers(limit ?: 100, joinBefore)
        return PagePublicMemberDto(
                parameters = PageParametersPublicMemberDto(
                        limit = limit,
                        cursor = cursor
                ),
                nextCursor = members.nextCursor?.epochSecond?.toString(),
                content = members.content.map { MemberWebTranslator.toPublicDto(it) }
        )
    }

    @DryRunnable
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize(PERMIT_ALL)
    @Operation(summary = "Register a new member", description = "Creates new member")
    @Throws(MemberCreationFailed::class)
    fun createMember(@RequestBody createMemberDto: CreateMemberDto): MemberDto =
            memberWriteFacade.createMember(toCreateCommand(createMemberDto)).assertRight()
                    .let { toDto(it) }

    @GetMapping(MEMBER_ID)
    @PreAuthorize(PERMIT_ALL)
    @Operation(summary = "Get a member details", description = "Gets member by id")
    @Throws(MemberNotFound::class)
    fun getMember(@PathVariable(MEMBER_ID_VAR) memberId: MemberId): MemberDto =
            toDto(memberReadFacade.getMemberById(memberId).assertRight())

    @DryRunnable
    @DeleteMapping(MEMBER_ID)
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(summary = "Delete a member", description = "Removes member by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Throws(MemberNotFound::class)
    fun deleteMember(@PathVariable(MEMBER_ID_VAR) memberId: MemberId) =
            memberWriteFacade.deleteMember(memberId).assertRight()
}