/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web.me

import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.swanframework.application.MemberId

@Schema(name = "CurrentMemberData")
internal data class MemberContextDto(
        val memberId: MemberId?,
        val administrator: Boolean,
        val hasPasswordExpired: Boolean,
)