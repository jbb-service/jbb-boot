/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.member.web

import org.jbb.boot.member.api.MemberCreationFailed
import org.jbb.boot.member.api.MemberNotFound
import org.jbb.boot.member.api.MemberUpdateFailed
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class MemberErrorInfo(
        override val status: HttpStatus,
        override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    MEMBER_NOT_FOUND(
            HttpStatus.NOT_FOUND,
            "MMBR-100",
            "Member not found",
            setOf(MemberNotFound::class.java, MemberUpdateFailed.MemberNotFound::class.java)
    ),
    MEMBER_CREATION_FAILED(
            HttpStatus.BAD_REQUEST, "MMBR-101", "Member creation failed",
            setOf(MemberCreationFailed::class.java)
    ),
    EMAIL_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST, "MMBR-102", "Email update failed",
            setOf(MemberUpdateFailed.ChangeEmailFailed::class.java)
    );

}