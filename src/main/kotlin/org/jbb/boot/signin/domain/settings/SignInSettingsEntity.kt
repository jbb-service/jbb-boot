/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.domain.settings

import org.jbb.boot.signin.api.LoginType
import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
@Table(name = "JBB_SIGN_IN_SETTINGS")
internal data class SignInSettingsEntity(

        @field:NotNull
        @field:Enumerated(EnumType.STRING)
        var loginType: LoginType,

        @field:NotNull
        var caseSensitive: Boolean,
) : BaseEntity()