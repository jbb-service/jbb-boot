/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.domain.installation

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_10_0
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.boot.signin.api.LoginType
import org.jbb.boot.signin.api.SignInSettings
import org.jbb.boot.signin.api.SignInSettingsFacade
import org.springframework.stereotype.Component

@Component
internal class FirstSignInSettingsCreator(
        private val signInSettingsFacade: SignInSettingsFacade
) : InstallationAction<Nothing> {

    override fun name() = "Creating sign in settings"

    override fun fromVersion(): Version = VERSION_0_10_0

    override fun install(context: Nothing?): Either<InstallationAbortedDueToActionRequired, Unit> {
        signInSettingsFacade.updateSignInSettings(
                SignInSettings(
                        loginType = LoginType.USERNAME,
                        caseSensitive = false
                )
        )
        return Either.Right(Unit)
    }
}