/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.domain

import org.jbb.boot.signin.api.SignInFailedEvent
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.security.NewRootCauseException
import org.jbb.swanframework.security.SignInFailureHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest

@Component
internal class SignInFailedEventEmitter(private val domainEventBus: DomainEventBus) :
        SignInFailureHandler {

    override fun handle(
            login: String,
            memberId: MemberId?,
            request: HttpServletRequest
    ): NewRootCauseException? {
        domainEventBus.post(SignInFailedEvent(memberId, login))
        return null
    }
}