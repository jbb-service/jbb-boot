/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.domain

import org.jbb.boot.signin.api.SignInSuccessEvent
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.security.MemberUser
import org.jbb.swanframework.security.SignInSuccessHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest

@Component
internal class SignInSuccessEventEmitter(private val domainEventBus: DomainEventBus) :
        SignInSuccessHandler {

    override fun handle(user: MemberUser, request: HttpServletRequest) {
        domainEventBus.post(SignInSuccessEvent(user.memberId, request.session.id))
    }
}