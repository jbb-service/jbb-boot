/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.signin.domain.settings

import org.jbb.boot.signin.api.SignInSettings
import org.jbb.boot.signin.api.SignInSettingsChangedEvent
import org.jbb.boot.signin.api.SignInSettingsFacade
import org.jbb.boot.signin.domain.settings.SignInSettingsTranslator.toEntity
import org.jbb.boot.signin.domain.settings.SignInSettingsTranslator.toModel
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.springframework.stereotype.Service

@Service
internal class SignInSettingsService(
        private val repository: SignInSettingsRepository,
        private val domainEventBus: DomainEventBus,
        private val dryRunContext: DryRunContext
) : SignInSettingsFacade {

    override fun getSignInSettings(): SignInSettings =
            toModel(repository.findAll().first())

    override fun updateSignInSettings(signInSettings: SignInSettings): SignInSettings {
        val currentSettings = repository.findAll().firstOrNull() ?: toEntity(signInSettings)
        currentSettings.run {
            loginType = signInSettings.loginType
            caseSensitive = signInSettings.caseSensitive
        }
        dryRunContext.completeIfDryRun()
        repository.save(currentSettings)
        domainEventBus.post(SignInSettingsChangedEvent())
        return toModel(currentSettings)
    }
}