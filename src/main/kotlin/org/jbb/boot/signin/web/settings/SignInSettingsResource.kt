/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web.settings

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.signin.api.SignInSettingsFacade
import org.jbb.boot.signin.web.SIGN_IN_SETTINGS
import org.jbb.boot.signin.web.SIGN_IN_SETTINGS_API
import org.jbb.boot.signin.web.settings.SignInSettingsWebTranslator.toDto
import org.jbb.boot.signin.web.settings.SignInSettingsWebTranslator.toModel
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = SIGN_IN_SETTINGS_API)
@RequestMapping(API_V1 + SIGN_IN_SETTINGS, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class SignInSettingsResource(
        private val signInSettingsFacade: SignInSettingsFacade,
) {

    @Operation(summary = "Get sign in settings", description = "Gets a sign in settings")
    @PreAuthorize(PERMIT_ALL)
    @GetMapping
    fun getSignInSettings(): SignInSettingsDto =
            toDto(signInSettingsFacade.getSignInSettings())

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(summary = "Update sign in settings", description = "Updates a sign in settings")
    fun updateSignInSettings(
            @RequestBody @Validated settingsDto: SignInSettingsDto
    ): SignInSettingsDto {
        val settings = toModel(settingsDto)
        return toDto(signInSettingsFacade.updateSignInSettings(settings))
    }
}