/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web.settings

import org.jbb.boot.signin.api.SignInSettings

internal object SignInSettingsWebTranslator {

    fun toDto(settings: SignInSettings): SignInSettingsDto = SignInSettingsDto(
            loginType = settings.loginType,
            loginCaseSensitive = settings.caseSensitive
    )

    fun toModel(dto: SignInSettingsDto): SignInSettings = SignInSettings(
            loginType = dto.loginType,
            caseSensitive = dto.loginCaseSensitive
    )


}