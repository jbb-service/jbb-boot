/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.swanframework.security.SecurityConstants.API_V1_SIGN_OUT
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = AUTHENTICATION_API)
@RequestMapping(API_V1_SIGN_OUT, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class SignOutResource {

    @PostMapping
    @Operation(summary = "Sign out", description = "Signs out current member from system")
    @PreAuthorize(IS_AUTHENTICATED)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun signOut() {
        throw IllegalStateException("Controller only for documentation purposes. "
                + "Requests to that endpoint are handled by Spring Security before reaching Spring MVC")
    }
}