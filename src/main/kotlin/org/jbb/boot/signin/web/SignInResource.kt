/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.swanframework.security.SecurityConstants.API_V1_SIGN_IN
import org.jbb.swanframework.security.SecurityConstants.LOGIN_PARAM
import org.jbb.swanframework.security.SecurityConstants.PASSWORD_PARAM
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = AUTHENTICATION_API)
@RequestMapping(API_V1_SIGN_IN, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class SignInResource {

    @PostMapping(consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    @Operation(summary = "Sign in", description = "Signs in a member into system")
    @PreAuthorize(PERMIT_ALL)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun signIn(
            @RequestParam(LOGIN_PARAM) login: String?,
            @RequestParam(PASSWORD_PARAM) password: String?
    ) {
        throw IllegalStateException(
                "Controller only for documentation purposes. "
                        + "Requests to that endpoint are handled by Spring Security before reaching Spring MVC"
        )
    }
}