/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.board.api

import arrow.core.Either
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue

// Operations
interface BoardSettingsFacade {

    fun getBoardSettings(): BoardSettings

    fun updateBoardSettings(boardSettings: BoardSettings): Either<UpdateBoardSettingsFailed, BoardSettings>

}

// Errors
class UpdateBoardSettingsFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
const val FIRST_BOARD_NAME_ACTION_NAME = "Setting board name"

data class BoardSettings(
        val boardName: String,
        val boardDescription: String = "",
        val welcomeMessage: String = "",
)

// Events
class BoardSettingsChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "BoardSettingsChangedEvent() ${super.toString()}"
    }
}
