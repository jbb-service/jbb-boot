/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.board.api.BoardSettingsFacade
import org.jbb.boot.board.api.UpdateBoardSettingsFailed
import org.jbb.boot.board.web.BoardSettingsWebTranslator.toDto
import org.jbb.boot.board.web.BoardSettingsWebTranslator.toModel
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = BOARD_SETTINGS_API)
@RequestMapping(API_V1 + BOARD_SETTINGS, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class BoardSettingsResource(
        private val boardSettingsFacade: BoardSettingsFacade,
) {

    @Operation(summary = "Get board settings", description = "Gets a board settings")
    @PreAuthorize(PERMIT_ALL)
    @GetMapping
    fun getBoardSettings(): BoardSettingsDto =
            toDto(boardSettingsFacade.getBoardSettings())

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(summary = "Update board settings", description = "Updates a board settings")
    @Throws(UpdateBoardSettingsFailed::class)
    fun updateBoardSettings(@RequestBody settingsDto: BoardSettingsDto): BoardSettingsDto {
        val settings = toModel(settingsDto)
        return toDto(boardSettingsFacade.updateBoardSettings(settings).assertRight())
    }

}