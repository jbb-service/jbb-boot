/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.web

import org.jbb.boot.board.api.BoardSettings

internal object BoardSettingsWebTranslator {

    fun toDto(settings: BoardSettings): BoardSettingsDto = BoardSettingsDto(
            boardName = settings.boardName,
            boardDescription = settings.boardDescription,
            welcomeMessage = settings.welcomeMessage,
    )

    fun toModel(dto: BoardSettingsDto): BoardSettings = BoardSettings(
            boardName = dto.boardName,
            boardDescription = dto.boardDescription,
            welcomeMessage = dto.welcomeMessage,
    )

}