/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.web

import org.jbb.boot.board.api.UpdateBoardSettingsFailed
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@Order(DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice(annotations = [RestController::class])
internal class BoardRestExceptionHandler {

    @ExceptionHandler(UpdateBoardSettingsFailed::class)
    fun handle(ex: UpdateBoardSettingsFailed): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(BoardErrorInfo.BOARD_SETTINGS_UPDATE_FAILED, ex)
    }

}