/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.board.web

import org.jbb.boot.board.api.UpdateBoardSettingsFailed
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class BoardErrorInfo(
        override val status: HttpStatus,
        override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    BOARD_SETTINGS_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST, "BARD-100",
            "Board settings update failed", setOf(UpdateBoardSettingsFailed::class.java)
    )

}