/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.domain.installation

import arrow.core.Either
import arrow.core.right
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_14_0
import org.jbb.boot.board.api.BoardSettings
import org.jbb.boot.board.api.BoardSettingsFacade
import org.jbb.boot.board.api.FIRST_BOARD_NAME_ACTION_NAME
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.springframework.stereotype.Component

@Component
internal class BoardNameInstallationAction(
        private val boardSettingsFacade: BoardSettingsFacade
) : InstallationAction<BoardSettings> {

    override fun name() = FIRST_BOARD_NAME_ACTION_NAME

    override fun fromVersion(): Version = VERSION_0_14_0

    override fun install(context: BoardSettings?):
            Either<InstallationAbortedDueToActionRequired, Unit> {
        context?.let { boardSettingsFacade.updateBoardSettings(it).right() }
                ?: return Either.Left(InstallationAbortedDueToActionRequired("Missing board name"))
        return Either.Right(Unit)
    }
}