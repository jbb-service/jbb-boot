/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.board.domain

import arrow.core.Either
import org.jbb.boot.board.api.BoardSettings
import org.jbb.boot.board.api.BoardSettingsChangedEvent
import org.jbb.boot.board.api.BoardSettingsFacade
import org.jbb.boot.board.api.UpdateBoardSettingsFailed
import org.jbb.boot.board.domain.BoardSettingsFactory.toEntity
import org.jbb.boot.board.domain.BoardSettingsFactory.toModel
import org.jbb.boot.board.domain.BoardSettingsFactory.updateWith
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.validation.ValidationService
import org.springframework.stereotype.Service

@Service
internal class BoardSettingsService(
        private val repository: BoardSettingsRepository,
        private val validationService: ValidationService,
        private val dryRunContext: DryRunContext,
        private val domainEventBus: DomainEventBus,
) : BoardSettingsFacade {

    override fun updateBoardSettings(boardSettings: BoardSettings):
            Either<UpdateBoardSettingsFailed, BoardSettings> {
        val entity = (repository.findAll().firstOrNull() ?: toEntity(boardSettings))
                .updateWith(boardSettings)
        val issues = validationService.validate(entity)
        if (issues.isNotEmpty()) {
            return Either.Left(UpdateBoardSettingsFailed(issues))
        }
        dryRunContext.completeIfDryRun()

        return Either.Right(toModel(repository.save(entity)))
                .also { domainEventBus.post(BoardSettingsChangedEvent()) }
    }

    override fun getBoardSettings(): BoardSettings = toModel(repository.findAll().first())
}