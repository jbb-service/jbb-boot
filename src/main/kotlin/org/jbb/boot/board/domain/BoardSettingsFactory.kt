/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.domain

import org.jbb.boot.board.api.BoardSettings

internal object BoardSettingsFactory {

    fun toModel(entity: BoardSettingsEntity) = BoardSettings(
            boardName = entity.boardName,
            boardDescription = entity.boardDescription,
            welcomeMessage = entity.welcomeMessage,
    )

    fun BoardSettingsEntity.updateWith(boardSettings: BoardSettings) = this.also {
        this.boardName = boardSettings.boardName
        this.boardDescription = boardSettings.boardDescription
        this.welcomeMessage = boardSettings.welcomeMessage
    }

    fun toEntity(boardSettings: BoardSettings): BoardSettingsEntity =
            BoardSettingsEntity(
                    boardName = boardSettings.boardName,
                    boardDescription = boardSettings.boardDescription,
                    welcomeMessage = boardSettings.welcomeMessage,
            )
}