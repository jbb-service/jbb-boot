/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.web

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotNull

@Schema(name = "InstallationRequest")
internal data class InstallationRequestDto(
        @field:NotNull val board: InstallationBoardDto? = null,
        @field:NotNull val administrator: InstallationAdminDto? = null,
        @field:NotNull val database: InstallationDatabaseDto? = null,
)