/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.web

import org.jbb.boot.installation.api.AlreadyInstalled
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@Order(RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice
internal class BootInstallationExceptionHandler {

    @ExceptionHandler(AlreadyInstalled::class)
    fun handle(ex: AlreadyInstalled): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(InstallationErrorInfo.ALREADY_INSTALLED)
    }

}