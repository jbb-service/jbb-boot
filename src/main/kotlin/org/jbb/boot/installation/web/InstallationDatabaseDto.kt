/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.web

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.swagger.v3.oas.annotations.media.DiscriminatorMapping
import io.swagger.v3.oas.annotations.media.Schema

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes(
        JsonSubTypes.Type(value = InstallationH2DatabaseDto::class, name = "h2"),
        JsonSubTypes.Type(value = InstallationPostgresDatabaseDto::class, name = "postgres")
)
@Schema(
        name = "InstallationDatabaseRequest",
        discriminatorProperty = "type",
        discriminatorMapping = [
            DiscriminatorMapping(value = "h2", schema = InstallationH2DatabaseDto::class),
            DiscriminatorMapping(
                    value = "postgres",
                    schema = InstallationPostgresDatabaseDto::class
            )]
)
internal sealed class InstallationDatabaseDto

@Schema(name = "InstallationH2DatabaseRequest")
internal data class InstallationH2DatabaseDto(
        val username: String?,
        val password: String?
) : InstallationDatabaseDto()

@Schema(name = "InstallationPostgresDatabaseRequest")
internal data class InstallationPostgresDatabaseDto(
        val host: String?,
        val port: Int?,
        val databaseName: String?,
        val username: String?,
        val password: String?
) : InstallationDatabaseDto()