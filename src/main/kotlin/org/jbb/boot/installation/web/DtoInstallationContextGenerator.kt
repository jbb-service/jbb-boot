/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.web

import org.jbb.boot.board.api.BoardSettings
import org.jbb.boot.board.api.FIRST_BOARD_NAME_ACTION_NAME
import org.jbb.boot.installation.api.*
import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.privilege.api.CREATE_ADMIN_INSTALL_ACTION_NAME
import org.springframework.stereotype.Component

@Component
internal class DtoInstallationContextGenerator :
        InstallationContextGenerator<InstallationRequestDto> {

    var context: InstallationRequestDto? = null

    override fun buildContext(data: InstallationRequestDto): Map<InstallationActionName, Any> =
            mapOf(
                    CREATE_BOOTSTRAP_FILE_ACTION_NAME to data.toDatabaseDetails(),
                    CREATE_ADMIN_INSTALL_ACTION_NAME to CreateMemberCommand(
                            username = data.administrator!!.username ?: "",
                            email = data.administrator.email ?: "",
                            password = data.administrator.password?.toCharArray()
                                    ?: CharArray(0)
                    ),
                    FIRST_BOARD_NAME_ACTION_NAME to BoardSettings(
                            boardName = data.board?.boardName ?: ""
                    )
            ).also { context = data }

    override fun type() = InstallationRequestDto::class.java

    fun InstallationRequestDto.toDatabaseDetails(): DatabaseDetails =
            when (this.database) {
                is InstallationH2DatabaseDto -> H2DatabaseDetails(
                        username = this.database.username ?: "",
                        password = this.database.password ?: ""
                )
                is InstallationPostgresDatabaseDto -> PostgresDatabaseDetails(
                        host = this.database.host ?: "",
                        port = this.database.port ?: 5432,
                        databaseName = this.database.databaseName ?: "",
                        username = this.database.username ?: "",
                        password = this.database.password ?: ""
                )
                null -> H2DatabaseDetails("", "")
            }

    override fun getLastProcessedContext(): InstallationRequestDto? = context

}