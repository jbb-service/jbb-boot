/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.domain.bootstrap

import org.apache.commons.lang3.RandomStringUtils
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.TransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.*
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
@ComponentScan
@EnableTransactionManagement
internal class DatabaseConfiguration {

    @Bean
    fun dataSource(
            bootstrapProvider: BootstrapProvider,
            dsBuilderFactory: DataSourceBuilderFactory
    ): DataSource =
            bootstrapProvider.getBootstrap()
                    ?.let { dsBuilderFactory.getDataSourceBuilder(it).build() }
                    ?: embeddedH2DataSource()

    private fun embeddedH2DataSource(): DataSource = DataSourceBuilder.create()
            .driverClassName("org.h2.Driver")
            .url("jdbc:h2:mem:${RandomStringUtils.randomAlphanumeric(8)};DB_CLOSE_ON_EXIT=FALSE")
            .username("sa")
            .password("sa")
            .build()

    @Bean
    fun entityManagerFactory(
            bootstrapProvider: BootstrapProvider,
            dataSource: DataSource
    ): LocalContainerEntityManagerFactoryBean =
            LocalContainerEntityManagerFactoryBean().apply {
                this.dataSource = dataSource
                this.setPackagesToScan("org.jbb")
                this.jpaVendorAdapter = HibernateJpaVendorAdapter().apply {
                    setJpaProperties(additionalProperties(bootstrapProvider.getBootstrap()))
                }
            }

    @Bean
    fun transactionManager(entityManagerFactory: EntityManagerFactory): TransactionManager =
            JpaTransactionManager(entityManagerFactory)

    internal fun additionalProperties(bootstrap: Bootstrap?) = Properties().apply {
        setProperty(
                "hibernate.hbm2ddl.auto",
                if (bootstrap.isConfigured()) "update" else "create-only"
        )
        setProperty("hibernate.dialect", bootstrap.dialect())
    }

    fun Bootstrap?.isConfigured() = this != null

    fun Bootstrap?.dialect() = when (this?.database) {
        is H2DatabaseBoostrap, null -> "org.hibernate.dialect.H2Dialect"
        is PostgresDatabaseBoostrap -> "org.hibernate.dialect.PostgreSQLDialect"
    }

}