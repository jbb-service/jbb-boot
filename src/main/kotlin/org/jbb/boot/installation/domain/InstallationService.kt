/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain

import arrow.core.Either
import org.jbb.boot.installation.api.*
import org.jbb.boot.installation.domain.InstallationStepStatus.*
import org.jbb.swanframework.application.AppMetadataProperties
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.eventbus.DomainEventBus
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Instant


@Component
internal class InstallationService(
        private val installActions: List<InstallationAction<*>>,
        private val appMetadataProperties: AppMetadataProperties,
        private val logRepository: InstallationLogRepository,
        private val stepRepository: InstallationStepRepository,
        private val domainEventBus: DomainEventBus
) : InstallationFacade {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun install(installationContext: Map<InstallationActionName, Any>): Either<AlreadyInstalled, Unit> {
        val finishStepsNames = stepRepository.findAll()
                .sortedBy { it.finishedAt }
                .filter { it.status == SUCCESS }
                .map { it.actionName }
        val stepsToRun = installActions
                .filterNot { finishStepsNames.contains(it.name()) }
                .sortedBy { it.fromVersion() }
        if (stepsToRun.isEmpty()) {
            return Either.Left(AlreadyInstalled())
        }
        runSteps(stepsToRun, installationContext)
        return Either.Right(Unit)
    }

    private fun runSteps(
            stepsToRun: List<InstallationAction<*>>,
            installationContext: Map<InstallationActionName, Any>
    ) {
        try {
            stepsToRun.forEach { runStep(it, installationContext[it.name()]) }
            domainEventBus.post(InstallationUpgradePerformedEvent(appMetadataProperties.version))
            logRepository.save(InstallationLogEntity(appMetadataProperties.version))
        } catch (ex: InstallationAbortedDueToActionRequired) {
            log.warn("Installation has been aborted. Reason: {}", ex.message)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun runStep(action: InstallationAction<*>, context: Any?) {
        val step = InstallationStepEntity(
                actionName = action.name(),
                fromVersion = action.fromVersion().toString(),
                ranOnVersion = appMetadataProperties.version,
                startedAt = Instant.now(),
                finishedAt = Instant.now(), //to be changed...
                status = RUNNING,
                errorMessage = null
        )
        try {
            (action as InstallationAction<Any>).install(context).assertRight()
            stepRepository.save(
                    step.copy(
                            status = SUCCESS,
                            finishedAt = Instant.now()
                    )
            )
        } catch (ex: InstallationAbortedDueToActionRequired) {
            stepRepository.save(
                    step.copy(
                            status = ACTION_REQUIRED,
                            finishedAt = Instant.now(),
                            errorMessage = ex.message
                    )
            )
            throw ex
        } catch (ex: Throwable) {
            stepRepository.save(
                    step.copy(
                            status = FAILED,
                            finishedAt = Instant.now(),
                            errorMessage = ex.message
                    )
            )
            throw ex
        }
    }

}