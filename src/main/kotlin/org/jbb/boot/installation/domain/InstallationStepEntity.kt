/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.domain

import org.jbb.swanframework.jpa.BaseEntity
import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "JBB_INSTALLATION_STEPS")
internal data class InstallationStepEntity(
        @field:Column(name = "action_name")
        val actionName: String,

        @field:Column(name = "from_version")
        val fromVersion: String,

        @field:Column(name = "ran_on_version")
        val ranOnVersion: String,

        @field:Column(name = "started_at")
        val startedAt: Instant,

        @field:Column(name = "finished_at")
        val finishedAt: Instant,

        @field:Enumerated(EnumType.STRING)
        @field:Column(name = "status")
        val status: InstallationStepStatus,

        @field:Column(name = "error_message")
        val errorMessage: String? = null,
) : BaseEntity()

internal enum class InstallationStepStatus {
        RUNNING, SUCCESS, FAILED, ACTION_REQUIRED
}