/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain

import org.jbb.swanframework.installation.InstallationVerifier
import org.springframework.stereotype.Component

@Component
internal class DefaultInstallationVerifier(private val logRepository: InstallationLogRepository) :
        InstallationVerifier {

    override fun isInstalled(): Boolean = logRepository.count() > 0
}