/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

internal data class Bootstrap(val database: DatabaseBootstrap)

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes(
        JsonSubTypes.Type(value = H2DatabaseBoostrap::class, name = "h2"),
        JsonSubTypes.Type(value = PostgresDatabaseBoostrap::class, name = "postgres")
)
internal sealed class DatabaseBootstrap {
    abstract val username: String
    abstract val password: String
    abstract fun driverClassName(): String
}

internal data class H2DatabaseBoostrap(
        override val username: String,
        override val password: String
) : DatabaseBootstrap() {
    override fun driverClassName() = "org.h2.Driver"
    fun url(pathToFile: String) = "jdbc:h2:file:$pathToFile;DB_CLOSE_ON_EXIT=FALSE"
}

internal data class PostgresDatabaseBoostrap(
        val host: String,
        val databaseName: String,
        val port: Int,
        override val username: String,
        override val password: String
) : DatabaseBootstrap() {
    override fun driverClassName() = "org.postgresql.Driver"
    fun url() = "jdbc:postgresql://$host:$port/$databaseName"
}