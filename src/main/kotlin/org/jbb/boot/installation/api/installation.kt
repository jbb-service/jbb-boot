/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.api

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.jbb.swanframework.eventbus.DomainEvent

// Operations
interface InstallationFacade {

    fun install(installationContext: Map<InstallationActionName, Any>):
            Either<AlreadyInstalled, Unit>
}

interface InstallationAction<C : Any> {

    fun name(): InstallationActionName

    fun fromVersion(): Version

    fun install(context: C?): Either<InstallationAbortedDueToActionRequired, Unit>

}

interface InstallationContextGenerator<T> {
    fun buildContext(data: T): Map<InstallationActionName, Any>

    fun getLastProcessedContext(): T?

    fun type(): Class<T>
}

// Errors
class AlreadyInstalled : RuntimeException()

class InstallationAbortedDueToActionRequired(msg: String) : RuntimeException(msg)

// Models
typealias InstallationActionName = String

const val CREATE_BOOTSTRAP_FILE_ACTION_NAME = "Creating bootstrap file"

sealed class DatabaseDetails

data class H2DatabaseDetails(val username: String, val password: String) : DatabaseDetails()
data class PostgresDatabaseDetails(
        val host: String,
        val port: Int,
        val databaseName: String,
        val username: String,
        val password: String
) : DatabaseDetails()

// Events
class InstallationUpgradePerformedEvent(val version: String) : DomainEvent() {
    override fun toString(): String {
        return "InstallationUpgradePerformedEvent(version='$version') ${super.toString()}"
    }

}