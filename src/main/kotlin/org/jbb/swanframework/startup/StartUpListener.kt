/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.startup

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture


@Component
@ConditionalOnProperty(
        value = ["jbb.swan-framework.start-up.early-server-start"],
        havingValue = "true"
)
internal class StartUpListener : ApplicationListener<ContextRefreshedEvent> {
    companion object {
        val promise = CompletableFuture<ContextRefreshedEvent>()
    }

    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        promise.complete(event)
    }
}