/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.startup

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.server.WebServer
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(
        value = ["jbb.swan-framework.start-up.early-server-start"],
        havingValue = "true"
)
internal class ProgressBeanPostProcessor(private val objectMapper: ObjectMapper) :
        BeanPostProcessor {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any? {
        return if (bean is TomcatServletWebServerFactory) wrap(bean) else bean
    }

    private fun wrap(factory: TomcatServletWebServerFactory): TomcatServletWebServerFactory? {
        factory.addContextValves(ProgressValve(objectMapper))
        return object : TomcatServletWebServerFactory() {
            override fun getWebServer(vararg initializers: ServletContextInitializer): WebServer? {
                val container: WebServer = factory.getWebServer(*initializers)
                log.info("Eagerly starting {}", container)
                container.start()
                return container
            }
        }
    }
}