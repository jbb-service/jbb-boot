/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import org.jbb.swanframework.application.MemberId
import java.time.Instant
import java.util.*

open class DomainEvent protected constructor() {
    val eventId: String?
    val createdAt: Instant?
    var publishedAt: Instant? = null
        internal set
    var generatedByMemberId: MemberId? = null
        internal set
    var generatedByIpAddress: String? = null
        internal set
    var generatedInSessionId: String? = null
        internal set
    var traceId: String? = null
        internal set

    init {
        eventId = UUID.randomUUID().toString()
        createdAt = Instant.now()
    }

    override fun toString(): String {
        return "DomainEvent(" +
                "eventId=$eventId, " +
                "createdAt=$createdAt, " +
                "publishedAt=$publishedAt, " +
                "generatedByMemberId=$generatedByMemberId, " +
                "generatedByIpAddress=$generatedByIpAddress, " +
                "generatedInSessionId=$generatedInSessionId, " +
                "traceId=$traceId" +
                ")"
    }
}