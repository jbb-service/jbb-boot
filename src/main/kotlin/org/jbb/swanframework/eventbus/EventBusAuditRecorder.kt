/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import com.google.common.eventbus.Subscribe
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
internal class EventBusAuditRecorder {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @Subscribe
    fun logDomainEvent(domainEvent: DomainEvent) {
        log.info("Published domain event: {}", domainEvent)
    }
}