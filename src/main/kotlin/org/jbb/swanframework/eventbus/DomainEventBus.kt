/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import com.google.common.eventbus.EventBus
import org.jbb.swanframework.web.HttpRequestContext
import org.jbb.swanframework.web.traceid.TraceIdUtils
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class DomainEventBus internal constructor(
        exceptionHandler: EventExceptionHandler,
        auditRecorder: EventBusAuditRecorder,
        httpRequestContext: HttpRequestContext
) : EventBus(exceptionHandler) {

    private val httpRequestContext: HttpRequestContext

    init {
        register(auditRecorder)
        this.httpRequestContext = httpRequestContext
    }

    override fun post(event: Any) = if (event is DomainEvent) {
        includeMetaData(event)
        super.post(event)
    } else {
        throw IllegalArgumentException(
                "You should post only DomainEvent through DomainEventBus, not: ${event.javaClass}"
        )
    }

    private fun includeMetaData(event: DomainEvent) =
            event.apply {
                generatedByMemberId = httpRequestContext.getCurrentMember()?.memberId
                generatedByIpAddress = httpRequestContext.getCurrentIpAddress()
                generatedInSessionId = httpRequestContext.getCurrentSessionId()
                traceId = TraceIdUtils.getCurrentTraceId()
                publishedAt = Instant.now()
            }
}