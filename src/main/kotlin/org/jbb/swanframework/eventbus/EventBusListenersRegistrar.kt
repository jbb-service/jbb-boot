/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
internal class EventBusListenersRegistrar(
        private val context: ApplicationContext,
        private val domainEventBus: DomainEventBus
) {

    @PostConstruct
    fun registerAllListeners() =
            context.getBeansOfType(DomainEventBusListener::class.java)
                    .values.forEach { domainEventBus.register(it) }
}