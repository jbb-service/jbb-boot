/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health

import com.codahale.metrics.health.HealthCheck
import com.codahale.metrics.health.HealthCheckRegistry
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.Instant
import javax.annotation.PostConstruct

@Component
@EnableScheduling
internal class HealthCheckManager(private val healthCheckRegistry: HealthCheckRegistry) {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    private var healthResults: Map<String, HealthCheck.Result> = emptyMap()
    private var lastRunDateTime: Instant = Instant.now()

    fun getHealthResult(): HealthResult = HealthResult(
            lastCheckedAt = lastRunDateTime,
            status = resolveStatus()
    )

    @PostConstruct
    @Scheduled(fixedDelay = 30000)
    fun runHealthChecks() {
        lastRunDateTime = Instant.now()
        healthResults = healthCheckRegistry.runHealthChecks()
        logHealthResults()
    }

    private fun logHealthResults() {
        if (getHealthResult().status.isHealthy()) {
            log.debug("Health checks have been updated. Current status is: healthy")
        } else {
            log.error("Health checks have been updated. Current status is: unhealthy")
        }
        healthResults.keys.forEach { healthCheckName -> logHealthResult(healthCheckName) }
    }

    private fun logHealthResult(healthCheckName: String) {
        healthResults[healthCheckName]?.let {
            if (it.isHealthy) {
                log.debug("Health check '{}' is healthy", healthCheckName)
            } else {
                log.error("Health check '{}' is unhealthy with message: '{}' and details: {}",
                        healthCheckName, it.message, it.details, it.error)
            }
        }
    }

    private fun resolveStatus(): HealthStatus {
        val healthy = healthResults.values.stream().allMatch { obj: HealthCheck.Result -> obj.isHealthy }
        return if (healthy) HealthStatus.HEALTHY else HealthStatus.UNHEALTHY
    }
}