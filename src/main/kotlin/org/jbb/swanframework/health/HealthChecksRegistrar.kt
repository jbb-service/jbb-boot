/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health

import com.codahale.metrics.health.HealthCheckRegistry
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
internal class HealthChecksRegistrar(
        private val context: ApplicationContext,
        private val healthCheckRegistry: HealthCheckRegistry
) {

    @PostConstruct
    fun registerAllHealthChecks() {
        context.getBeansOfType(DomainHealthCheck::class.java).values
                .forEach { healthCheckRegistry.register(it.getName(), it) }
    }
}