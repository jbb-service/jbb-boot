/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health.checks

import com.codahale.metrics.health.jvm.ThreadDeadlockHealthCheck
import org.jbb.swanframework.health.DomainHealthCheck
import org.springframework.stereotype.Component

@Component
internal class DeadlockHealthCheck : DomainHealthCheck() {
    private val threadDeadlockHealthCheck = ThreadDeadlockHealthCheck()

    override fun getName(): String = "Thread deadlocks"

    override fun check(): Result = threadDeadlockHealthCheck.execute()
}