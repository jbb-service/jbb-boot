/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health.web

import org.jbb.swanframework.application.AppMetadataProperties
import org.jbb.swanframework.health.HealthResult
import org.springframework.stereotype.Component

@Component
internal class HealthTranslator(private val appMetadataProperties: AppMetadataProperties) {

    fun toDto(healthResult: HealthResult) =
            HealthDto(
                    appName = appMetadataProperties.name,
                    appVersion = appMetadataProperties.version,
                    status = healthResult.status,
                    lastCheckedAt = healthResult.lastCheckedAt
            )

}