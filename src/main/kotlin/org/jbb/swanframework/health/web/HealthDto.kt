/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health.web

import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.swanframework.health.HealthStatus
import java.time.Instant

@Schema(name = "Health")
internal data class HealthDto(
        val appName: String,
        val appVersion: String,
        val status: HealthStatus,
        val lastCheckedAt: Instant
)