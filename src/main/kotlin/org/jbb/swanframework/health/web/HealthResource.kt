/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.swanframework.health.HealthCheckService
import org.jbb.swanframework.installation.AvailableBeforeInstallation
import org.jbb.swanframework.security.SecurityConstants
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

internal const val HEALTH = "/health"

@RestController
@Tag(name = "Application health API")
@RequestMapping(API_V1 + HEALTH, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class HealthResource(
        private val healthCheckService: HealthCheckService,
        private val healthTranslator: HealthTranslator
) {

    @AvailableBeforeInstallation
    @PreAuthorize(SecurityConstants.PERMIT_ALL)
    @Operation(
            summary = "Get app health status", description = "Gets application health status",
            responses = [
                ApiResponse(
                        description = "Application is healthy", responseCode = "200",
                        content = [
                            Content(
                                    mediaType = "application/json",
                                    schema = Schema(implementation = HealthDto::class)
                            )]
                ),
                ApiResponse(
                        description = "Application is unhealthy", responseCode = "500",
                        content = [Content(
                                mediaType = "application/json",
                                schema = Schema(implementation = HealthDto::class)
                        )]
                )
            ]
    )
    @GetMapping
    fun getHealth(): ResponseEntity<HealthDto> {
        val result = healthTranslator.toDto(healthCheckService.latestHealthResult())
        return if (result.status.isHealthy()) {
            ResponseEntity.ok(result)
        } else {
            ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(result)
        }
    }
}