/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.jpa

import org.jbb.swanframework.health.DomainHealthCheck
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.jdbc.IncorrectResultSetColumnCountException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.support.JdbcUtils
import org.springframework.stereotype.Component
import java.sql.ResultSet
import javax.annotation.PostConstruct
import javax.sql.DataSource

private const val DEFAULT_QUERY = "SELECT 1"

@Component
@ConditionalOnBean(value = [DataSource::class])
internal class DatabaseHealthCheck(private val dataSource: DataSource) : DomainHealthCheck() {
    private lateinit var jdbcTemplate: JdbcTemplate

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @PostConstruct
    fun createJdbcTemplate() {
        jdbcTemplate = JdbcTemplate(dataSource)
    }

    override fun getName(): String {
        return "Database status"
    }

    override fun check(): Result {
        return try {
            jdbcTemplate.query(DEFAULT_QUERY, SingleColumnRowMapper())
            Result.healthy()
        } catch (e: Exception) {
            log.error("Database health check failed", e)
            Result.unhealthy(e)
        }
    }

    private class SingleColumnRowMapper : RowMapper<Any> {
        override fun mapRow(rs: ResultSet, rowNum: Int): Any? {
            val metaData = rs.metaData
            val columns = metaData.columnCount
            if (columns != 1) {
                throw IncorrectResultSetColumnCountException(1, columns)
            }
            return JdbcUtils.getResultSetValue(rs, 1)
        }
    }

}