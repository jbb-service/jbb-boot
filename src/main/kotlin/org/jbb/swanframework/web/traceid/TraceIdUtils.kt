/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.traceid

import org.slf4j.MDC
import java.util.*

private const val TRACE_ID_KEY = "traceId"

object TraceIdUtils {

    internal fun generateNewTraceId(): String = UUID.randomUUID().toString()
            .also { MDC.put(TRACE_ID_KEY, it) }

    fun getCurrentTraceId(): String? = MDC.get(TRACE_ID_KEY)

    internal fun cleanTraceId() {
        MDC.remove(TRACE_ID_KEY)
    }
}
