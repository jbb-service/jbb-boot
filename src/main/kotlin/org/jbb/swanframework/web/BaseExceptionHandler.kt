/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.ConversionNotSupportedException
import org.springframework.beans.TypeMismatchException
import org.springframework.context.MessageSource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.http.converter.HttpMessageNotWritableException
import org.springframework.security.access.AccessDeniedException
import org.springframework.util.CollectionUtils
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import org.springframework.web.HttpMediaTypeNotAcceptableException
import org.springframework.web.HttpMediaTypeNotSupportedException
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.MissingPathVariableException
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.ServletRequestBindingException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.context.request.async.AsyncRequestTimeoutException
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.multipart.support.MissingServletRequestPartException
import org.springframework.web.servlet.NoHandlerFoundException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*
import javax.validation.ConstraintViolation
import javax.validation.ConstraintViolationException

@RestControllerAdvice
internal class BaseExceptionHandler(
        private val messageSource: MessageSource,
        private val notReadableExceptionHandlers: List<NotReadableExceptionHandler<*>>
) : ResponseEntityExceptionHandler() {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @ExceptionHandler(Exception::class)
    protected fun handleExceptionInternal(
            ex: Exception,
            request: WebRequest?
    ): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.INTERNAL_ERROR, ex)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    protected fun handle(
            ex: ConstraintViolationException,
            request: WebRequest
    ): ResponseEntity<Any> {
        val validationErrorResponse: ErrorResponse = createFrom(CommonErrorInfo.VALIDATION_ERROR)
        val details = fieldErrorDetails(ex.constraintViolations)
        return buildResponseEntity(validationErrorResponse.addDetails(details))
    }

    @ExceptionHandler(AccessDeniedException::class)
    protected fun handle(ex: AccessDeniedException?, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(
                if (request.userPrincipal == null) CommonErrorInfo.UNAUTHORIZED
                else CommonErrorInfo.FORBIDDEN
        )
    }

    override fun handleHttpRequestMethodNotSupported(
            ex: HttpRequestMethodNotSupportedException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest
    ): ResponseEntity<Any> {
        val supportedMethods = ex.supportedHttpMethods ?: emptySet()
        if (supportedMethods.isNotEmpty()) {
            headers.allow = supportedMethods
        }
        return buildResponseEntity(CommonErrorInfo.METHOD_NOT_SUPPORTED, headers)
    }

    override fun handleHttpMediaTypeNotSupported(
            ex: HttpMediaTypeNotSupportedException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val mediaTypes = ex.supportedMediaTypes
        if (!CollectionUtils.isEmpty(mediaTypes)) {
            headers.accept = mediaTypes
        }
        return buildResponseEntity(CommonErrorInfo.UNSUPPORTED_MEDIA_TYPE, headers)
    }

    override fun handleHttpMediaTypeNotAcceptable(
            ex: HttpMediaTypeNotAcceptableException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.NOT_ACCEPTABLE_MEDIA_TYPE)
    }

    override fun handleMissingPathVariable(ex: MissingPathVariableException,
                                           headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.MISSING_PATH_VARIABLE)
    }

    override fun handleMissingServletRequestParameter(
            ex: MissingServletRequestParameterException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.MISSING_REQUEST_PARAMETER)
    }

    override fun handleServletRequestBindingException(
            ex: ServletRequestBindingException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.REQUEST_BINDING_ERROR)
    }

    override fun handleConversionNotSupported(
            ex: ConversionNotSupportedException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.CONVERSION_NOT_SUPPORTED, ex)
    }

    override fun handleTypeMismatch(ex: TypeMismatchException,
                                    headers: HttpHeaders,
                                    status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        var errorResponse: ErrorResponse = createFrom(CommonErrorInfo.TYPE_MISMATCH)
        if (ex is MethodArgumentTypeMismatchException) {
            errorResponse = errorResponse
                    .addDetail(ErrorDetail(ex.name, "path-variable-type-invalid",
                            "failed to convert path variable to required type"))
        }
        return buildResponseEntity(errorResponse)
    }

    @Suppress("UNCHECKED_CAST")
    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException,
                                              headers: HttpHeaders, status: HttpStatus,
                                              request: WebRequest): ResponseEntity<Any> {
        val causeEx = ex.cause
        for (handler in notReadableExceptionHandlers) {
            if (handler.getSupportedClass().isAssignableFrom(causeEx!!.javaClass)) {
                val errorResponse = (handler as NotReadableExceptionHandler<Throwable>).handle(causeEx)
                return ResponseEntity(errorResponse, headers,
                        HttpStatus.valueOf(errorResponse.status))
            }
        }
        return buildResponseEntity(CommonErrorInfo.MESSAGE_NOT_READABLE)
    }

    override fun handleHttpMessageNotWritable(
            ex: HttpMessageNotWritableException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.MESSAGE_NOT_WRITABLE)
    }

    override fun handleMethodArgumentNotValid(
            ex: MethodArgumentNotValidException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val validationErrorResponse: ErrorResponse = createFrom(CommonErrorInfo.VALIDATION_ERROR)
        val details = fieldErrorDetails(ex.bindingResult)
        return buildResponseEntity(validationErrorResponse.addDetails(details))
    }

    override fun handleMissingServletRequestPart(
            ex: MissingServletRequestPartException,
            headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.MISSING_REQUEST_PART)
    }

    override fun handleBindException(ex: BindException, headers: HttpHeaders,
                                     status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val bindErrorResponse: ErrorResponse = createFrom(CommonErrorInfo.BIND_ERROR)
        val details = fieldErrorDetails(ex)
        return buildResponseEntity(bindErrorResponse.addDetails(details))
    }

    override fun handleNoHandlerFoundException(
            ex: NoHandlerFoundException, headers: HttpHeaders, status: HttpStatus,
            request: WebRequest): ResponseEntity<Any> {
        return buildResponseEntity(CommonErrorInfo.NO_HANDLER_FOUND)
    }

    override fun handleAsyncRequestTimeoutException(
            ex: AsyncRequestTimeoutException, headers: HttpHeaders, status: HttpStatus,
            webRequest: WebRequest): ResponseEntity<Any>? {
        if (webRequest is ServletWebRequest) {
            val servletWebRequest = webRequest
            val request = servletWebRequest.request
            val response = servletWebRequest.response
            if (response != null && response.isCommitted) {
                if (logger.isErrorEnabled) {
                    logger.error(
                            "Async timeout for " + request.method + " [" + request
                                    .requestURI
                                    + "]")
                }
                return null
            }
        }
        return buildResponseEntity(CommonErrorInfo.ASYNC_REQUEST_TIMEOUT, ex)
    }

    private fun buildResponseEntity(errorResponse: ErrorResponse): ResponseEntity<Any> {
        return ResponseEntity(errorResponse, HttpHeaders(), HttpStatus.resolve(errorResponse.status)!!)
    }

    private fun buildResponseEntity(errorInfo: ErrorInfo): ResponseEntity<Any> {
        val errorResponse: ErrorResponse = createFrom(errorInfo)
        return ResponseEntity(errorResponse, HttpHeaders(), errorInfo.status)
    }

    private fun buildResponseEntity(errorInfo: ErrorInfo, e: Exception): ResponseEntity<Any> {
        log.error("Client request finished with exception", e)
        val errorResponse: ErrorResponse = createFrom(errorInfo)
        return ResponseEntity(errorResponse, HttpHeaders(), errorInfo.status)
    }

    private fun buildResponseEntity(errorInfo: ErrorInfo, headers: HttpHeaders): ResponseEntity<Any> {
        val errorResponse: ErrorResponse = createFrom(errorInfo)
        return ResponseEntity(errorResponse, headers, errorInfo.status)
    }

    private fun fieldErrorDetails(bindingResult: BindingResult): List<ErrorDetail> =
            bindingResult.fieldErrors.map {
                ErrorDetail(
                        property = it.field,
                        violationCode = it.code,
                        message = messageSource.getMessage(it, Locale.getDefault())
                )
            }

    private fun fieldErrorDetails(violations: Set<ConstraintViolation<*>>): List<ErrorDetail> =
            violations.map {
                ErrorDetail(
                        property = it.propertyPath.last().name,
                        violationCode = it.propertyPath.toString(),
                        message = it.message
                )
            }

}