/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.google.common.collect.Lists
import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.web.errorcode.ERROR_CODES_PATH
import org.jbb.swanframework.web.traceid.TraceIdUtils
import org.springframework.http.ResponseEntity
import java.time.Instant
import javax.validation.constraints.*

@Schema(name = "ErrorRFC7807")
@JsonPropertyOrder(value = ["type", "errorCode", "title", "status", "instance", "timestamp", "details"])
data class ErrorResponse(
        @field:Min(400)
        @field:Max(599)
        @field:NotNull
        val status: Int, // compliant with RFC 7807

        @field:NotBlank
        val title: String, // compliant with RFC 7807

        @field:Pattern(regexp = "^[A-Z]{4}-[0-9]{3}$")
        @field:NotNull
        val errorCode: String, // internal RFC 7807 extension

        @field:NotNull
        val timestamp: Instant, // internal RFC 7807 extension

        val details: List<ErrorDetail>, // internal RFC 7807 extension
) {

    @field:Pattern(regexp = "^$ERROR_CODES_PATH/[A-Z]{4}-[0-9]{3}$")
    @field:NotNull
    val type: String = "$ERROR_CODES_PATH/$errorCode" // compliant with RFC 7807

    @field:Pattern(regexp = "^urn:uuid:.+$")
    @field:NotNull
    val instance: String = "urn:uuid:${TraceIdUtils.getCurrentTraceId()}" // compliant with RFC 7807

    fun addDetail(errorDetail: ErrorDetail): ErrorResponse =
            this.copy(details = this.details.plus(errorDetail))

    fun addDetails(errorDetails: Collection<ErrorDetail>): ErrorResponse =
            this.copy(details = this.details.plus(errorDetails))
}

fun getErrorResponseEntity(errorInfo: ErrorInfo): ResponseEntity<ErrorResponse> {
    val errorResponse = createFrom(errorInfo)
    return ResponseEntity(errorResponse, errorInfo.status)
}

fun createFrom(errorInfo: ErrorInfo, details: List<ErrorDetail>) =
        ErrorResponse(
                status = errorInfo.status.value(),
                errorCode = errorInfo.errorCode,
                title = errorInfo.message,
                timestamp = Instant.now(),
                details = details
        )

fun createFrom(errorInfo: ErrorInfo) = createFrom(errorInfo, Lists.newArrayList())

fun getErrorResponseEntity(errorInfo: ErrorInfo,
                           validationFailed: ValidationFailed): ResponseEntity<ErrorResponse> {
    val errorResponse = createFrom(errorInfo, mapToErrorDetails(validationFailed))
    return ResponseEntity(errorResponse, errorInfo.status)
}

private fun mapToErrorDetails(validationFailed: ValidationFailed): List<ErrorDetail> =
        validationFailed.issues
                .map {
                    ErrorDetail(
                            property = it.name,
                            violationCode = it.errorCode,
                            message = it.message ?: ""
                    )
                }