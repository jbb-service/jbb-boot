/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.notreadable

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import org.jbb.swanframework.web.*
import org.springframework.stereotype.Component

@Component
internal class UnrecognizedPropertyHandler : NotReadableExceptionHandler<UnrecognizedPropertyException> {
    override fun getSupportedClass(): Class<UnrecognizedPropertyException> {
        return UnrecognizedPropertyException::class.java
    }

    override fun handle(ex: UnrecognizedPropertyException): ErrorResponse {
        val errorResponse = createFrom(CommonErrorInfo.UNRECOGNIZED_PROPERTY)
        return errorResponse.addDetail(ErrorDetail("property", null, ex.propertyName))
    }
}