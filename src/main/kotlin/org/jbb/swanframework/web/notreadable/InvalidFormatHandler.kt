/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.notreadable

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import org.jbb.swanframework.web.*
import org.springframework.stereotype.Component

@Component
internal class InvalidFormatHandler : NotReadableExceptionHandler<InvalidFormatException> {
    override fun getSupportedClass(): Class<InvalidFormatException> {
        return InvalidFormatException::class.java
    }

    override fun handle(ex: InvalidFormatException): ErrorResponse {
        var errorResponse = createFrom(CommonErrorInfo.INVALID_FORMAT_PROPERTY)
        val path = ex.path
        if (path.isNotEmpty()) {
            val fieldName = path[0].fieldName
            errorResponse = errorResponse
                    .addDetail(ErrorDetail("property", null, fieldName))
        }
        return errorResponse
    }

}