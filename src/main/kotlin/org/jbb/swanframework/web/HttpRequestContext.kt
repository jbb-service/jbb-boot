/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import org.jbb.swanframework.security.MemberUser
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetails
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest

@Component
class HttpRequestContext {

    @PostConstruct
    fun setStrategy() =
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL)

    fun getCurrentMember(): MemberUser? =
            SecurityContextHolder.getContext().authentication?.principal
                    .takeIf { it is MemberUser }
                    ?.let { it as MemberUser }

    fun getCurrentHttpRequest(): HttpServletRequest? = RequestContextHolder.getRequestAttributes()
            .takeIf { it is ServletRequestAttributes }
            ?.let { (it as ServletRequestAttributes).request }

    fun getCurrentIpAddress(): String? = getCurrentHttpRequest()?.remoteAddr
            ?: getWebAuthenticationDetails()?.remoteAddress

    fun getCurrentSessionId(): String? =
            getCurrentHttpRequest()?.let { it.getSession(false)?.id }
                    ?: getWebAuthenticationDetails()?.sessionId

    private fun getWebAuthenticationDetails(): WebAuthenticationDetails? =
            SecurityContextHolder.getContext()?.authentication?.details
                    ?.takeIf { it is WebAuthenticationDetails }
                    ?.let { it as WebAuthenticationDetails }

}