/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.errorcode

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.swanframework.installation.AvailableBeforeInstallation
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants.API_V1
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

const val ERROR_CODES_PATH = "$API_V1/error-codes"

@RestController
@Tag(name = "Error codes API")
@RequestMapping(value = [ERROR_CODES_PATH], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class ErrorCodeResource(private val errorInfoResolver: ErrorInfoResolver) {

    @AvailableBeforeInstallation
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "List error codes",
            description = "Gets error codes returned by application with its definition"
    )
    @GetMapping
    fun getErrorCodes(
            @RequestParam(value = "status", required = false) status: Int?,
            @RequestParam(value = "errorCodeContains", required = false) errorCodeContains: String?,
    ): List<ErrorCodeDto> =
            errorInfoResolver.getAllErrorInfosSorted()
                    .filter { status == null || it.status.value() == status }
                    .filter {
                        errorCodeContains.isNullOrBlank() ||
                                it.errorCode.contains(errorCodeContains, ignoreCase = true)
                    }
                    .map {
                        ErrorCodeDto(
                                errorCode = it.errorCode,
                                status = it.status.value(),
                                title = it.message
                        )
                    }

    internal class ErrorCodeNotFound : RuntimeException()

    @AvailableBeforeInstallation
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "Get error code",
            description = "Get single error code returned by application",
            responses = [
                ApiResponse(
                        responseCode = "500",
                        content = [Content(
                                mediaType = "application/json",
                                schema = Schema(implementation = ErrorResponse::class)
                        )]
                )]
    )
    @GetMapping("/{errorCode}")
    @Throws(ErrorCodeNotFound::class)
    fun getErrorCode(@PathVariable("errorCode") errorCode: String): ErrorCodeDto =
            errorInfoResolver.getAllErrorInfosSorted()
                    .find { it.errorCode == errorCode }
                    ?.let {
                        ErrorCodeDto(
                                errorCode = it.errorCode,
                                status = it.status.value(),
                                title = it.message
                        )
                    }
                    ?: throw ErrorCodeNotFound()

    @ExceptionHandler(ErrorCodeNotFound::class)
    private fun handle(ex: ErrorCodeNotFound): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CommonErrorInfo.ERROR_CODE_NOT_FOUND)
    }
}

