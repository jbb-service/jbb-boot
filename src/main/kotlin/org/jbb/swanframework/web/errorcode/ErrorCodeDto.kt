/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.errorcode

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.*

@Schema(name = "ErrorCode")
internal data class ErrorCodeDto(
        @field:Min(400)
        @field:Max(599)
        @field:NotNull
        val status: Int,

        @field:Pattern(regexp = "^[A-Z]{4}-[0-9]{3}$")
        @field:NotNull
        val errorCode: String,

        @field:NotBlank
        val title: String,
)