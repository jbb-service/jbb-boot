/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

object RestConstants {
    const val DOMAIN_REST_CONTROLLER_ADVICE_ORDER = 1
    const val API = "/api"
    const val API_V1 = "$API/v1"
    const val ME = "/me"
}