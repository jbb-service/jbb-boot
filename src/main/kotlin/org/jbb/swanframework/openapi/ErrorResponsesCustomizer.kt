/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.openapi

import com.google.common.collect.HashMultimap
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.media.Content
import io.swagger.v3.oas.models.media.MediaType
import io.swagger.v3.oas.models.media.Schema
import io.swagger.v3.oas.models.responses.ApiResponse
import org.jbb.swanframework.installation.AvailableBeforeInstallation
import org.jbb.swanframework.security.SecurityConstants
import org.jbb.swanframework.security.password.ExpiredPasswordIgnored
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorInfo
import org.jbb.swanframework.web.errorcode.ErrorInfoResolver
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod

@Component
internal class ErrorResponsesCustomizer(
        private val errorInfoResolver: ErrorInfoResolver
) : OperationCustomizer {

    override fun customize(operation: Operation, handlerMethod: HandlerMethod): Operation {
        val errorInfoMap = HashMultimap.create<Int, ErrorInfo>()
        handlerMethod.method.exceptionTypes
                .mapNotNull { errorInfoResolver.findErrorInfoForException(it) }
                .forEach { errorInfoMap.put(it.status.value(), it) }

        val authExpression = handlerMethod.method.getAnnotation(PreAuthorize::class.java)?.value
        if (SecurityConstants.PERMIT_ALL != authExpression) {
            errorInfoMap.put(CommonErrorInfo.UNAUTHORIZED)
        }
        if (SecurityConstants.IS_AN_ADMINISTRATOR == authExpression) {
            errorInfoMap.put(CommonErrorInfo.FORBIDDEN)
        }
        if (handlerMethod.method.getAnnotation(AvailableBeforeInstallation::class.java) == null) {
            errorInfoMap.put(CommonErrorInfo.NOT_INSTALLED)
        }
        if (handlerMethod.method.getAnnotation(ExpiredPasswordIgnored::class.java) == null) {
            errorInfoMap.put(CommonErrorInfo.MEMBER_PASSWORD_EXPIRED)
        }
        errorInfoMap.put(CommonErrorInfo.BAD_CREDENTIALS)
        errorInfoMap.put(CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)
        errorInfoMap.put(CommonErrorInfo.INTERNAL_ERROR)
        errorInfoMap.put(CommonErrorInfo.NOT_READY)
        errorInfoMap.keySet()
                .forEach { updateErrorResponsesInOperation(operation, it, errorInfoMap[it]) }
        return operation
    }

    private fun HashMultimap<Int, ErrorInfo>.put(errorInfo: ErrorInfo) =
            this.put(errorInfo.status.value(), errorInfo)

    private fun updateErrorResponsesInOperation(
            operation: Operation, httpStatus: Int,
            errorInfos: Collection<ErrorInfo>
    ) {
        val apiResponse = ApiResponse()
        apiResponse.description = errorInfos.map { it.formattedMessage }
                .sorted()
                .joinToString(";</br>")
        val content = Content()
        val mediaType = MediaType()
        val schema: Schema<*> = Schema<Any?>()
        schema.`$ref` = "#/components/schemas/ErrorRFC7807"
        mediaType.schema = schema
        content["application/json"] = mediaType
        apiResponse.content = content
        operation.responses[httpStatus.toString()] = apiResponse
    }

}