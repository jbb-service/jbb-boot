/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.openapi

import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.security.SecurityRequirement
import org.jbb.swanframework.security.SecurityConstants
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.stereotype.Component
import org.springframework.util.CollectionUtils
import org.springframework.web.method.HandlerMethod

@Component
internal class SecuritySchemesCustomizer : OperationCustomizer {

    override fun customize(operation: Operation, handlerMethod: HandlerMethod): Operation {
        if (!CollectionUtils.containsAny(
                        operation.tags, listOf(
                        SecurityConstants.API_V1_SIGN_IN,
                        SecurityConstants.API_V1_SIGN_OUT
                )
                )) {
            operation.addSecurityItem(SecurityRequirement().addList("BasicAuth"))
        }
        return operation
    }
}