/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.openapi

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityScheme
import org.jbb.swanframework.application.AppMetadataProperties
import org.springdoc.core.GroupedOpenApi
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
internal class OpenApiAutoConfiguration(private val appMetadataProperties: AppMetadataProperties) {

    @Bean
    fun groupedOpenApi(operationCustomizers: List<OperationCustomizer>): GroupedOpenApi {
        return GroupedOpenApi.builder()
                .pathsToMatch("/api/**")
                .group("jbb-" + appMetadataProperties.name + "-rest-api")
                .apply { operationCustomizers.forEach { this.addOperationCustomizer(it) } }
                .build()
    }

    @Bean
    fun openAPI(): OpenAPI {
        return OpenAPI()
                .info(
                        Info().title("jBB " + appMetadataProperties.name + " API")
                                .description(
                                        "RESTful endpoints for java bulletin board - " + appMetadataProperties.name
                                                + " service"
                                )
                                .version(appMetadataProperties.version)
                )
                .components(
                        Components().addSecuritySchemes(
                                "BasicAuth",
                                SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")
                        )
                )
    }



}