/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.context.SecurityContextPersistenceFilter
import org.springframework.security.web.csrf.CookieCsrfTokenRepository
import org.springframework.security.web.csrf.CsrfFilter
import org.springframework.web.filter.CharacterEncodingFilter

private val IGNORED_RESOURCES = arrayOf("/fonts/**", "/webjars/**")

@Configuration
@Import(SecurityAutoConfiguration::class)
internal class UiSecurityConfiguration(private val traceIdFilter: TraceIdFilter) :
        WebSecurityConfigurerAdapter() {

    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers(*IGNORED_RESOURCES)
    }

    override fun configure(http: HttpSecurity) {
        http.addFilterBefore(traceIdFilter, SecurityContextPersistenceFilter::class.java)
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository())
        val characterEncodingFilter = CharacterEncodingFilter()
        characterEncodingFilter.encoding = "UTF-8"
        characterEncodingFilter.setForceEncoding(true)
        http.addFilterBefore(characterEncodingFilter, CsrfFilter::class.java)
    }

}