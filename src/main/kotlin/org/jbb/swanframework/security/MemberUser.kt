/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.jbb.swanframework.application.MemberId
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User

class MemberUser(
        username: String,
        password: String,
        accountNonLocked: Boolean,
        authorities: Collection<GrantedAuthority>,
        val memberId: MemberId
) :
        User(username, password, true, true, true, accountNonLocked, authorities) {

    fun isAdministrator(): Boolean =
            authorities.any { it == SimpleGrantedAuthority(SecurityConstants.ROLE_ADMINISTRATOR) }

    fun hasPasswordExpired(): Boolean =
            authorities.any { it == SimpleGrantedAuthority(SecurityConstants.ROLE_PASSWORD_EXPIRED) }
}