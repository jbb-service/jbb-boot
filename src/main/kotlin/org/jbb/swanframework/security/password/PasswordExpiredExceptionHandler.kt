/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security.password

import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.RestConstants
import org.jbb.swanframework.web.getErrorResponseEntity
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

internal class PasswordExpired : RuntimeException()

@Order(RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice
internal class PasswordExpiredExceptionHandler {

    @ExceptionHandler(PasswordExpired::class)
    fun handle(ex: PasswordExpired): ResponseEntity<ErrorResponse> {
        return getErrorResponseEntity(CommonErrorInfo.MEMBER_PASSWORD_EXPIRED)
    }

}