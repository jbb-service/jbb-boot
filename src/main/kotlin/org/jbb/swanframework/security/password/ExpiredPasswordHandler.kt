/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.security.password

import org.jbb.swanframework.web.HttpRequestContext
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class ExpiredPasswordHandler(private val httpRequestContext: HttpRequestContext) :
        HandlerInterceptor {
    override fun preHandle(
            request: HttpServletRequest,
            response: HttpServletResponse,
            handler: Any
    ): Boolean {
        val excludeAnnotationPresent = if (handler is HandlerMethod) {
            handler.hasMethodAnnotation(ExpiredPasswordIgnored::class.java)
        } else false
        val passwordExpired = httpRequestContext.getCurrentMember()?.hasPasswordExpired() ?: false
        if (passwordExpired && !excludeAnnotationPresent) {
            throw PasswordExpired()
        }
        return true
    }
}