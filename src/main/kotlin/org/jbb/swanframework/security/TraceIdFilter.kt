/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.security

import org.jbb.swanframework.web.traceid.TraceIdUtils.cleanTraceId
import org.jbb.swanframework.web.traceid.TraceIdUtils.generateNewTraceId
import org.jbb.swanframework.web.traceid.TraceIdUtils.getCurrentTraceId
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class TraceIdFilter : OncePerRequestFilter() {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun doFilterInternal(
            request: HttpServletRequest,
            response: HttpServletResponse,
            filterChain: FilterChain
    ) {
        response.addHeader("trace-id", generateNewTraceId())
        log.debug("Processing of request with traceId {} started", getCurrentTraceId())
        filterChain.doFilter(request, response)
        log.debug("Processing of request with traceId {} finished", getCurrentTraceId())
        cleanTraceId()
    }

}