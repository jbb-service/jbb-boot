/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class ApiAuthenticationSuccessHandler(
        private val signInSuccessHandlers: List<SignInSuccessHandler>
) : AuthenticationSuccessHandler {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun onAuthenticationSuccess(request: HttpServletRequest,
                                         httpServletResponse: HttpServletResponse,
                                         authentication: Authentication) {
        val user = authentication.principal as MemberUser
        log.debug("Member {} with id {} sign in successfully", user.username, user.memberId)
        SecurityContextHolder.getContext().authentication = authentication
        request.session
                .setAttribute(SecurityConstants.SESSION_CONTEXT_ATTRIBUTE_NAME, SecurityContextHolder.getContext())
        httpServletResponse.status = HttpStatus.NO_CONTENT.value()
        signInSuccessHandlers.forEach { it.handle(user, request) }
    }
}