/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

object SecurityConstants {
    const val ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR"
    const val ROLE_PASSWORD_EXPIRED = "ROLE_PASSWORD_EXPIRED"
    const val PERMIT_ALL = "permitAll()"
    const val IS_AUTHENTICATED = "isAuthenticated()"
    const val IS_AN_ADMINISTRATOR = "hasRole('$ROLE_ADMINISTRATOR')"
    const val API_V1_SIGN_IN = "/api/v1/sign-in"
    const val API_V1_SIGN_OUT = "/api/v1/sign-out"
    const val LOGIN_PARAM = "login"
    const val PASSWORD_PARAM = "password"
    const val SESSION_CONTEXT_ATTRIBUTE_NAME = "SPRING_SECURITY_CONTEXT"
}