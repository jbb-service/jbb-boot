/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.context.SecurityContextPersistenceFilter
import org.springframework.security.web.savedrequest.NullRequestCache

@Configuration
@Import(SecurityAutoConfiguration::class)
@Order(2)
internal class BasicAuthSecurityConfiguration(
        private val apiAuthenticationEntryPoint: ApiAuthenticationEntryPoint,
        private val traceIdFilter: TraceIdFilter,
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.addFilterBefore(traceIdFilter, SecurityContextPersistenceFilter::class.java)
        http
                .requestMatcher(BasicRequestMatcher())
                .csrf().disable()
                .httpBasic()
                .realmName("jBB API")
                .authenticationEntryPoint(apiAuthenticationEntryPoint)
                .and()
                .requestCache().requestCache(NullRequestCache())
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(apiAuthenticationEntryPoint)
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }
}