/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.jbb.swanframework.application.MemberId
import org.springframework.security.core.AuthenticationException
import javax.servlet.http.HttpServletRequest

typealias NewRootCauseException = AuthenticationException

interface SignInFailureHandler {
    fun handle(
            login: String,
            memberId: MemberId?,
            request: HttpServletRequest
    ): NewRootCauseException?
}