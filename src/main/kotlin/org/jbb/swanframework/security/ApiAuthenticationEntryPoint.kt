/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import com.google.common.collect.ImmutableMap
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.createFrom
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpResponse
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.LockedException
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class ApiAuthenticationEntryPoint(
        private val messageConverter: MappingJackson2HttpMessageConverter) : AuthenticationEntryPoint {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    private val errorInfoMapping: Map<Class<out AuthenticationException>, CommonErrorInfo> = ImmutableMap.of(
            BadCredentialsException::class.java, CommonErrorInfo.BAD_CREDENTIALS,
            LockedException::class.java, CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED
    )


    override fun commence(httpServletRequest: HttpServletRequest,
                          httpServletResponse: HttpServletResponse, e: AuthenticationException) {
        val errorResponse = createFrom(getErrorInfo(e))
        val outputMessage: ServerHttpResponse = ServletServerHttpResponse(httpServletResponse)
        outputMessage.setStatusCode(HttpStatus.valueOf(errorResponse.status))
        messageConverter.write(errorResponse, MediaType.APPLICATION_JSON, outputMessage)
    }

    private fun getErrorInfo(e: AuthenticationException): CommonErrorInfo =
            (errorInfoMapping[e.javaClass] ?: CommonErrorInfo.UNAUTHORIZED)
                    .also {
                        if (it == CommonErrorInfo.UNAUTHORIZED) log.warn(
                                "Unexpected authentication error. Rollback error info to generic unauthorized error", e)
                    }
}