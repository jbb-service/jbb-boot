/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.security.SecurityConstants.LOGIN_PARAM
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class ApiAuthenticationFailureHandler(
        private val apiAuthenticationEntryPoint: ApiAuthenticationEntryPoint,
        private val userDetailsService: UserDetailsService,
        private val signInFailureHandlers: List<SignInFailureHandler>
) : AuthenticationFailureHandler {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun onAuthenticationFailure(
            request: HttpServletRequest, response: HttpServletResponse,
            ex: AuthenticationException
    ) {
        val login = request.getParameter(LOGIN_PARAM) ?: ""
        val memberId = getMemberIdFor(login)
        log.debug(
                "Sign in attempt failure for member with login '{}' (memberId: {})", login,
                memberId
        )
        val finalEx = signInFailureHandlers.mapNotNull { it.handle(login, memberId, request) }
                .firstOrNull() ?: ex
        apiAuthenticationEntryPoint.commence(request, response, finalEx)
    }

    private fun getMemberIdFor(login: String): MemberId? {
        return try {
            (userDetailsService.loadUserByUsername(login) as MemberUser).memberId
        } catch (ex: UsernameNotFoundException) {
            null
        }
    }
}