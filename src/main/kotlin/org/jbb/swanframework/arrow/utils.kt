/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.arrow

import arrow.core.Either

fun <T> Either<Exception, T>.assertRight(): T {
    return when (this) {
        is Either.Left -> throw this.value
        is Either.Right -> this.value
    }
}