/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.application.directory

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
@EnableConfigurationProperties(AppDirectoryProperties::class)
internal class ApplicationDirectoryAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(value = [AppDirectory::class])
    fun defaultAppDirectory(appDirectoryProperties: AppDirectoryProperties) =
            DefaultAppDirectory(appDirectoryProperties)
}