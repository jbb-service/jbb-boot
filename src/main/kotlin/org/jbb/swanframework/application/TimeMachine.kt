/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.application

import com.google.common.annotations.VisibleForTesting
import java.time.Clock
import java.time.Instant
import java.time.ZoneId

object TimeMachine {
    private var clock = Clock.systemDefaultZone()
    private val zoneId = ZoneId.systemDefault()

    fun now(): Instant {
        return Instant.now(clock)
    }

    /**
     * Only for testing purposes. DO NOT use in a production
     */
    @VisibleForTesting
    fun useFixedClockAt(instant: Instant) {
        clock = Clock.fixed(instant, zoneId)
    }

    /**
     * Only for testing purposes. DO NOT use in a production
     */
    @VisibleForTesting
    fun useSystemDefaultZoneClock() {
        clock = Clock.systemDefaultZone()
    }
}