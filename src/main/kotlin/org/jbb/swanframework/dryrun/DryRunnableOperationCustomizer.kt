/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.dryrun

import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.media.Schema
import io.swagger.v3.oas.models.parameters.Parameter
import io.swagger.v3.oas.models.responses.ApiResponse
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod

@Component
internal class DryRunnableOperationCustomizer : OperationCustomizer {
    override fun customize(operation: Operation, handlerMethod: HandlerMethod): Operation {
        if (!handlerMethod.hasMethodAnnotation(DryRunnable::class.java)) {
            return operation
        }

        operation.addParametersItem(Parameter().apply {
            this.name = "dryRun"
            this.required = false
            this.schema = Schema<Boolean>().apply {
                type = "boolean"
                setDefault(false)
            }
            this.`in` = "query"
        })

        val apiResponse = ApiResponse()
                .apply {
                    description =
                            "No Content. If `dryRun` request param is `true` and request is correct then response header `dry-run-success` will be `true` and operation won't be performed"
                }
        operation.responses["204"] = apiResponse
        return operation
    }
}