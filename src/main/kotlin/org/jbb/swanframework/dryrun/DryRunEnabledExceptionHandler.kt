/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.dryrun

import org.jbb.swanframework.web.RestConstants
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@Order(RestConstants.DOMAIN_REST_CONTROLLER_ADVICE_ORDER)
@ControllerAdvice
internal class DryRunEnabledExceptionHandler {

    @ExceptionHandler(DryRunEnabled::class)
    fun handle(ex: DryRunEnabled): ResponseEntity<Void> {
        val headers = HttpHeaders().apply { add("dry-run-success", "true") }
        return ResponseEntity(headers, HttpStatus.NO_CONTENT)
    }

}