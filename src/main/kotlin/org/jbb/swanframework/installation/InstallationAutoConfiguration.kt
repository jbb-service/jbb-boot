/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.installation

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@ComponentScan
@EnableConfigurationProperties(InstallationCheckProperties::class)
internal class InstallationAutoConfiguration(
        private val preInstallationApiInterceptor: PreInstallationApiInterceptor,
) : WebMvcConfigurer {

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(preInstallationApiInterceptor)
    }

    @Bean
    @ConditionalOnMissingBean(value = [InstallationVerifier::class])
    fun defaultInstallationVerifier() = object : InstallationVerifier {
        override fun isInstalled() = true

    }
}