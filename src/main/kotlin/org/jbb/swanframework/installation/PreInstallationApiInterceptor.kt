/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.installation

import org.jbb.swanframework.web.utils.PathResolver
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import org.springframework.util.AntPathMatcher
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
internal class PreInstallationApiInterceptor(
        private val installationCheckProperties: InstallationCheckProperties,
        @Lazy private val installationVerifier: InstallationVerifier,
        @Lazy private val pathResolver: PathResolver,
) : HandlerInterceptor {

    private val antPathMatcher = AntPathMatcher()

    override fun preHandle(
            request: HttpServletRequest, response: HttpServletResponse,
            handler: Any
    ): Boolean {
        if (installationVerifier.isInstalled()) {
            return true
        }
        val path = pathResolver.getRequestPathWithinApplication()
        val pathMatches = installationCheckProperties.include
                ?.let { antPathMatcher.match(it, path!!) } ?: false
        val excludeMatches = installationCheckProperties.exclude
                .any { antPathMatcher.match(it, path!!) }
        val excludeAnnotationPresent = if (handler is HandlerMethod) {
            handler.hasMethodAnnotation(AvailableBeforeInstallation::class.java)
        } else false
        if (pathMatches && !excludeMatches && !excludeAnnotationPresent) {
            throw NotInstalled()
        }
        return true
    }
}
