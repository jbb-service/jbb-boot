/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.e2e.health

import net.serenitybdd.junit5.SerenityTest
import net.thucydides.core.annotations.Steps
import org.junit.jupiter.api.Test

@SerenityTest
class HealthStories {

    @Steps
    lateinit var healthSteps: HealthSteps

    @Test
    fun `Getting health should return 200 status`() {
        // when
        val response = healthSteps.getHealth()

        // then
        response.then().statusCode(200)
    }
}