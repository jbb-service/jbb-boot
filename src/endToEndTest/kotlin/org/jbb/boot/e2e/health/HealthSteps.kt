/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.e2e.health

import io.restassured.response.Response
import net.thucydides.core.annotations.Step
import net.thucydides.core.steps.ScenarioSteps
import org.jbb.boot.e2e.RequestUtils
import org.jbb.boot.e2e.StepContainer

@StepContainer
class HealthSteps : ScenarioSteps() {

    @Step("Get health")
    fun getHealth(): Response = RequestUtils.prepareBasicRequest()
            .`when`()
            .get("/api/v1/health")
            .andReturn()
}