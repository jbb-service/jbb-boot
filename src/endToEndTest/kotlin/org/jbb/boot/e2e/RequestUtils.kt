/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.e2e

import io.restassured.filter.log.LogDetail
import io.restassured.specification.RequestSpecification
import net.serenitybdd.rest.SerenityRest

object RequestUtils {

    init {
        SerenityRest.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL)
                .enablePrettyPrinting(true)
    }

    fun baseUrl(): String = System.getProperty("baseUrl")

    fun adminUsername(): String = System.getProperty("adminUsername")

    fun adminPassword(): String = System.getProperty("adminPassword")

    fun prepareBasicRequest(): RequestSpecification = SerenityRest
            .rest().baseUri(baseUrl())

    fun prepareMemberRequest(username: String, password: String) = SerenityRest.rest()
            .baseUri(System.getProperty(baseUrl()))
            .auth().basic(username, password)

    fun prepareAdminRequest(): RequestSpecification = SerenityRest.rest()
            .baseUri(System.getProperty(baseUrl()))
            .auth().basic(adminUsername(), adminPassword())
}