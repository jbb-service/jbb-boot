/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.e2e

/**
 * Annotation to simplify creation of Serenity Step Classes
 *
 * Serenity expects all classes and methods to be open. By adding this annotation, and using the
 * gradle-allopen plugin, only this annotation needs to be added to classes that contain @Step from
 * Serenity
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class StepContainer