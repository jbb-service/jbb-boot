/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot

import com.google.common.io.Files
import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.swanframework.openapi.API_V3_SWAGGER_DOCS
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import java.io.File


@ActiveProfiles("disableAutoInstall")
internal class GenerateOpenAPIYamlIT : AbstractBaseResourceIT() {

    @Test
    fun generateOpenAPIFile() {
        val openapiResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`()["$API_V3_SWAGGER_DOCS.yaml"]
        val file = File("build/docs/openapi.yaml")
        Files.createParentDirs(file)
        Files.touch(file)
        File("build/docs/openapi.yaml").writeText(openapiResponse.body.asString())
    }

}