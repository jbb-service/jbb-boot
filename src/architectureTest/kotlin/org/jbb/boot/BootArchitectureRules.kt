/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot

import com.tngtech.archunit.base.DescribedPredicate
import com.tngtech.archunit.core.domain.JavaClass
import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.core.domain.JavaMethod
import com.tngtech.archunit.core.domain.JavaModifier
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchCondition
import com.tngtech.archunit.lang.ConditionEvents
import com.tngtech.archunit.lang.Priority
import com.tngtech.archunit.lang.SimpleConditionEvent
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition
import com.tngtech.archunit.library.Architectures
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition
import io.swagger.v3.oas.annotations.Operation
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.jpa.BaseEntity
import org.slf4j.Logger
import org.springframework.context.annotation.Configuration
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.RestController
import javax.persistence.Entity
import javax.persistence.Table

object BootArchitectureRules {
    private const val TECH_LIBS_LAYER = "Swan framework Layer"
    private const val BOOT_APP_LAYER = "Boot Application Layer"
    private const val BOOT_API_LAYER = "Boot API Layer"
    private const val BOOT_DOMAIN_LAYER = "Boot Domain Layer"
    private const val BOOT_WEB_LAYER = "Boot Web Layer"

    private const val TECH_LIBS_PACKAGES = "org.jbb.swanframework.."
    private const val BOOT_APP_PACKAGES = "org.jbb.boot.."
    private const val BOOT_API_PACKAGES = "org.jbb.boot.(*).api.."
    private const val BOOT_SERVICES_PACKAGES = "org.jbb.boot.(*).domain.."
    private const val BOOT_WEB_PACKAGES = "org.jbb.boot.(*).web.."

    @ArchTest
    fun testLayeredArchitecture(classes: JavaClasses?) {
        Architectures.layeredArchitecture()
                .layer(TECH_LIBS_LAYER).definedBy(TECH_LIBS_PACKAGES)
                .layer(BOOT_APP_LAYER).definedBy(BOOT_APP_PACKAGES)
                .layer(BOOT_API_LAYER).definedBy(BOOT_API_PACKAGES)
                .layer(BOOT_DOMAIN_LAYER).definedBy(BOOT_SERVICES_PACKAGES)
                .layer(BOOT_WEB_LAYER).definedBy(BOOT_WEB_PACKAGES)

                .whereLayer(TECH_LIBS_LAYER)
                .mayOnlyBeAccessedByLayers(BOOT_APP_LAYER)

                .whereLayer(BOOT_API_LAYER)
                .mayOnlyBeAccessedByLayers(BOOT_DOMAIN_LAYER, BOOT_WEB_LAYER)

                .whereLayer(BOOT_DOMAIN_LAYER).mayNotBeAccessedByAnyLayer()

                .whereLayer(BOOT_WEB_LAYER).mayNotBeAccessedByAnyLayer()

                .check(classes)
    }

    @ArchTest
    fun allClassesInApiLayerMustBePublic(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).classes()
                .that().resideInAPackage(BOOT_API_PACKAGES)
                .should().bePublic()
                .check(classes)
    }

    @ArchTest
    fun allExceptionsInApiLayerMustBeRuntime(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).classes()
                .that().resideInAPackage(BOOT_API_PACKAGES)
                .and().areAssignableTo(Exception::class.java)
                .should().beAssignableTo(RuntimeException::class.java)
                .check(classes)
    }

    @ArchTest
    fun allThrowablesInApiLayerMustNotBeError(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).noClasses()
                .that().resideInAPackage(BOOT_API_PACKAGES)
                .should().beAssignableTo(Error::class.java)
                .check(classes)
    }

    @ArchTest
    fun allExceptionsInApiLayerDoNotHaveExceptionSuffix(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).classes()
                .that().resideInAPackage(BOOT_API_PACKAGES).and()
                .areAssignableTo(Exception::class.java)
                .should().haveSimpleNameNotEndingWith("Exception")
                .check(classes)
    }

    @ArchTest
    fun serviceLayerShouldNotUseWebLayer(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).noClasses()
                .that().resideInAPackage(BOOT_SERVICES_PACKAGES)
                .should().accessClassesThat().resideInAPackage(BOOT_WEB_PACKAGES)
                .check(classes)
    }

    @ArchTest
    fun webLayerShouldNotUseServiceLayer(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).noClasses()
                .that().resideInAPackage(BOOT_WEB_PACKAGES)
                .should().accessClassesThat().resideInAPackage(BOOT_SERVICES_PACKAGES)
                .check(classes)
    }

    @ArchTest
    fun controllersShouldNotUseRepositoriesDirectly(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).noClasses().that().areAnnotatedWith(Controller::class.java)
                .should().accessClassesThat().areAnnotatedWith(Repository::class.java)
                .check(classes)
    }

    @ArchTest
    fun restControllersShouldNotUseRepositoriesDirectly(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.HIGH).noClasses().that().areAnnotatedWith(RestController::class.java)
                .should().accessClassesThat().areAnnotatedWith(Repository::class.java)
                .check(classes)
    }

    @ArchTest
    fun controllerNameShouldEndsWithController(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).classes().that().areAnnotatedWith(Controller::class.java)
                .should().haveNameMatching(".*Controller")
                .check(classes)
    }

    @ArchTest
    fun restControllerNameShouldEndsWithResource(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).classes().that().areAnnotatedWith(RestController::class.java)
                .should().haveNameMatching(".*Resource")
                .check(classes)
    }

    @ArchTest
    fun entitiesShouldExtendBaseEntity(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.MEDIUM).classes().that(areEntity())
                .should().beAssignableTo(BaseEntity::class.java)
                .check(classes)
    }

    @ArchTest
    fun entityNameShouldEndsWithEntity(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).classes().that(areEntity())
                .should().haveNameMatching(".*Entity")
                .check(classes)
    }

    @ArchTest
    fun loggerFieldShouldBeFinalAndHaveNameLog(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).fields().that()
                .haveRawType(Logger::class.java)
                .should().haveName("log").andShould().beFinal()
                .check(classes)
    }

    @ArchTest
    fun allClassesMustBeInOrgJbbPackage(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.MEDIUM).classes()
                .should().resideInAPackage("org.jbb..").check(classes)
    }

    @ArchTest
    fun springConfigurationClassNameShouldEndsWithConfiguration(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).classes().that()
                .areAnnotatedWith(Configuration::class.java)
                .should().haveNameMatching(".*Configuration")
                .check(classes)
    }

    @ArchTest
    fun domainEventClassNameShouldEndsWithEvent(classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.LOW).classes().that().areAssignableTo(DomainEvent::class.java)
                .should().haveNameMatching(".*Event")
                .check(classes)
    }

    @ArchTest
    fun libModulesCannotHaveCycle(classes: JavaClasses?) {
        SlicesRuleDefinition.slices().matching(TECH_LIBS_PACKAGES).namingSlices("$1 swan framework")
                .`as`(TECH_LIBS_LAYER).should().beFreeOfCycles().check(classes)
    }

    @ArchTest
    fun apiModuleCannotUseAnotherApiModule(classes: JavaClasses?) {
        SlicesRuleDefinition.slices().matching(BOOT_API_PACKAGES).namingSlices("$1 api")
                .`as`(BOOT_API_LAYER).should().notDependOnEachOther().check(classes)
    }

    @ArchTest
    fun serviceModuleCannotUseAnotherServiceModule(classes: JavaClasses?) {
        SlicesRuleDefinition.slices().matching(BOOT_SERVICES_PACKAGES).namingSlices("$1 service")
                .`as`(BOOT_DOMAIN_LAYER).should().notDependOnEachOther().check(classes)
    }

    @ArchTest
    fun webModuleCannotUseAnotherWebModule(classes: JavaClasses?) {
        SlicesRuleDefinition.slices().matching(BOOT_WEB_PACKAGES).namingSlices("$1 web")
                .`as`(BOOT_WEB_LAYER).should().notDependOnEachOther().check(classes)
    }

    @ArchTest
    fun publicMethodsOfRestControllersShouldHaveOperation(
            classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.MEDIUM).classes().that().areAnnotatedWith(RestController::class.java)
                .should(havePublicMethodAnnotatedWith(Operation::class.java))
                .check(classes)
    }

    @ArchTest
    fun publicMethodsOfRestControllersShouldHavePreAuthorize(
            classes: JavaClasses?) {
        ArchRuleDefinition.priority(Priority.MEDIUM).classes().that().areAnnotatedWith(RestController::class.java)
                .should(havePublicMethodAnnotatedWith(PreAuthorize::class.java))
                .check(classes)
    }

    private fun areEntity(): DescribedPredicate<JavaClass> {
        return object : DescribedPredicate<JavaClass>("Entity class") {
            override fun apply(javaClass: JavaClass): Boolean {
                return javaClass.isAnnotatedWith(Entity::class.java) ||
                        javaClass.isAnnotatedWith(Table::class.java)
            }
        }
    }

    private fun havePublicMethodAnnotatedWith(annotation: Class<out Annotation?>): ArchCondition<JavaClass> {
        return object : ArchCondition<JavaClass>(
                "have public method annotated with @" + annotation.name) {
            override fun check(javaClass: JavaClass, conditionEvents: ConditionEvents) {
                javaClass.methods.stream()
                        .filter { method: JavaMethod -> method.modifiers.contains(JavaModifier.PUBLIC) }
                        .filter { method: JavaMethod -> !method.isAnnotatedWith(annotation) }
                        .forEach { method: JavaMethod ->
                            conditionEvents.add(SimpleConditionEvent(javaClass, false,
                                    "method " + method.fullName + " is not annotated with @"
                                            + annotation
                                            .simpleName))
                        }
            }
        }
    }
}