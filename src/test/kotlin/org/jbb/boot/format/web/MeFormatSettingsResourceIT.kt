/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.api.Member
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_ME_FORMAT_SETTINGS = "/api/v1/me/format-settings"

internal class MeFormatSettingsResourceIT : AbstractBaseResourceIT() {
    private lateinit var testMember: Member

    @BeforeEach
    fun setUpMember() {
        testMember = testMemberHelper.createMember()
    }

    @Test
    fun memberShouldBeAbleToUpdateHisOwnFormatsToSystemDefault() {
        // given
        val newSettings = memberFormatSettingsDto(true, null, true, null)

        // when
        val putResponse = given()
                .auth().with(testMember.httpBasic())
                .body(newSettings)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        putResponse.then().status(HttpStatus.OK)

        // when
        val getResponse = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_ME_FORMAT_SETTINGS]

        // then
        getResponse.then().status(HttpStatus.OK)
        Assertions.assertThat(getResponse.`as`(MemberFormatSettingsDto::class.java))
                .isEqualTo(newSettings)
    }

    @Test
    fun memberShouldBeAbleToUpdateHisOwnFormatsFormatToCustom() {
        // given
        val newSettings = memberFormatSettingsDto(
                false,
                "dd.MM.yyyy HH:mm:ss", false, "HH mm ss"
        )

        // when
        val putResponse = given()
                .auth().with(testMember.httpBasic())
                .body(newSettings)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        putResponse.then().status(HttpStatus.OK)

        // when
        val getResponse = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_ME_FORMAT_SETTINGS]

        // then
        getResponse.then().status(HttpStatus.OK)
        Assertions.assertThat(getResponse.`as`(MemberFormatSettingsDto::class.java))
                .isEqualTo(newSettings)
    }

    @Test
    fun shouldFailed_whenNotValidCustomDateTimeFormatProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(false, "dd.MM.yyyyTrh1 HH:mm:ss", true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail("dateTimeFormat", null, "date time format is not valid")
        )
    }

    @Test
    fun shouldFailed_whenNotValidCustomDurationFormatProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(true, null, false, "HHTrh1!:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail("durationFormat", null, "duration format is not valid")
        )
    }

    @Test
    fun shouldFailed_whenDateTimeDefaultEnabled_andCustomFormatProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(true, "dd.MM.yyyy HH:mm:ss", true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail(
                        "dateTimeFormat", null,
                        "you should use default OR provide a custom pattern"
                )
        )
    }

    @Test
    fun shouldFailed_whenDateTimeDefaultDisabled_andCustomFormatNotProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(false, null, true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail(
                        "dateTimeFormat", null,
                        "you should use default OR provide a custom pattern"
                )
        )
    }

    @Test
    fun shouldFailed_whenDurationDefaultEnabled_andCustomFormatProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(true, null, true, "HH:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail(
                        "durationFormat", null,
                        "you should use default OR provide a custom pattern"
                )
        )
    }

    @Test
    fun shouldFailed_whenDurationDefaultDisabled_andCustomFormatNotProvided() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(true, null, false, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(
                response, FormatErrorInfo.MEMBER_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail(
                        "durationFormat", null,
                        "you should use default OR provide a custom pattern"
                )
        )
    }

    @Test
    fun shouldFailedWith401_whenNotSignInUserCalledEndpoint() {
        // when
        val response = given()
                .body(memberFormatSettingsDto(true, null, true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_FORMAT_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    private fun memberFormatSettingsDto(dateTimeUseDefault: Boolean,
                                        dateTimeCustomValue: String?,
                                        durationDefault: Boolean,
                                        durationCustomValue: String?) =
            MemberFormatSettingsDto(
                    dateTimeFormat = MemberFormatDto(
                            useDefault = dateTimeUseDefault,
                            customValue = dateTimeCustomValue
                    ),
                    durationFormat = MemberFormatDto(
                            useDefault = durationDefault,
                            customValue = durationCustomValue
                    )
            )

}