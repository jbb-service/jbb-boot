/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.web.MemberErrorInfo
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_MEMBER_FORMAT_SETTINGS = "/api/v1/members/{memberId}/format-settings"

internal class MemberFormatSettingsResourceIT : AbstractBaseResourceIT() {

    @Test
    fun adminShouldBeAbleToUpdateFormatSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // given
        val newSettings = memberFormatSettingsDto(
                false,
                "dd.MM.yyyy HH:mm:ss", false, "HH:mm:ss"
        )

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(newSettings)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId)

        // then
        response.then().status(HttpStatus.OK)

        // when
        val getResponse = given()
                .auth().with(adminHttpBasic)
                .`when`()[API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId]

        // then
        val settings = getResponse.`as`(MemberFormatSettingsDto::class.java)
        Assertions.assertThat(settings).isEqualTo(newSettings)
    }

    @Test
    fun shouldReturnMemberNotFound_whenTryingToUpdatePasswordForNotExistingMember() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(
                        memberFormatSettingsDto(
                                false,
                                "dd.MM.yyyy HH:mm:ss", false, "HH:mm:ss"
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_FORMAT_SETTINGS, "not-existing-member-id")

        // then
        assertErrorInfo(response, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldFailedWith401_whenNotSignInUserCalledPutEndpoint() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .body(memberFormatSettingsDto(true, null, true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun shouldFailedWith401_whenNotSignInUserCalledGetEndpoint() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .`when`()[API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId]

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun shouldFailedWith403_whenStandardUserCalledPutEndpoint() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(memberFormatSettingsDto(true, null, true, null))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun shouldFailedWith403_whenStandardUserCalledGetEndpoint() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_MEMBER_FORMAT_SETTINGS, testMember.memberId]

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    private fun memberFormatSettingsDto(dateTimeUseDefault: Boolean,
                                        dateTimeCustomValue: String?,
                                        durationDefault: Boolean,
                                        durationCustomValue: String?) =
            MemberFormatSettingsDto(
                    dateTimeFormat = MemberFormatDto(
                            useDefault = dateTimeUseDefault,
                            customValue = dateTimeCustomValue
                    ),
                    durationFormat = MemberFormatDto(
                            useDefault = durationDefault,
                            customValue = durationCustomValue
                    )
            )

}