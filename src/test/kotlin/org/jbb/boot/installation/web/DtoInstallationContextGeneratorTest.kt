/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.web

import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.installation.api.CREATE_BOOTSTRAP_FILE_ACTION_NAME
import org.jbb.boot.installation.api.H2DatabaseDetails
import org.jbb.boot.installation.api.PostgresDatabaseDetails
import org.junit.jupiter.api.Test


internal class DtoInstallationContextGeneratorTest {

    private val contextGenerator = DtoInstallationContextGenerator()

    @Test
    fun shouldReturnEmptyLastProcessedContext_whenGeneratorWasNotCalled() {
        // given
        contextGenerator.context = null

        // when
        val lastProcessedContext = contextGenerator.getLastProcessedContext()

        // then
        assertThat(lastProcessedContext).isNull()
    }

    @Test
    fun shouldBuildH2DatabaseDetailsCorrectly() {
        // given
        val dto = InstallationRequestDto(
                administrator = InstallationAdminDto("admin", "admin@gmail.com", "pass"),
                database = InstallationH2DatabaseDto("omc", "omc2"),
        )

        // when
        val installationMap = contextGenerator.buildContext(dto)

        // then
        assertThat(installationMap[CREATE_BOOTSTRAP_FILE_ACTION_NAME]).isEqualTo(
                H2DatabaseDetails(
                        username = "omc",
                        password = "omc2"
                )
        )
        assertThat(contextGenerator.context).isEqualTo(dto)
    }

    @Test
    fun shouldBuildPostgresDatabaseDetailsCorrectly() {
        // given
        val dto = InstallationRequestDto(
                administrator = InstallationAdminDto("admin", "admin@gmail.com", "pass"),
                database = InstallationPostgresDatabaseDto(
                        host = "localhost",
                        port = 4444,
                        databaseName = "default",
                        username = "omc",
                        password = "omc2"
                ),
        )

        // when
        val installationMap = contextGenerator.buildContext(dto)

        // then
        assertThat(installationMap[CREATE_BOOTSTRAP_FILE_ACTION_NAME]).isEqualTo(
                PostgresDatabaseDetails("localhost", 4444, "default", "omc", "omc2")
        )
        assertThat(contextGenerator.context).isEqualTo(dto)
    }

    @Test
    fun shouldBuildDummyH2DatabaseDetails_whenNoDatabaseDetailsProvidedInDto() {
        // given
        val dto = InstallationRequestDto(
                administrator = InstallationAdminDto("admin", "admin@gmail.com", "pass"),
                database = null,
        )

        // when
        val installationMap = contextGenerator.buildContext(dto)

        // then
        assertThat(installationMap[CREATE_BOOTSTRAP_FILE_ACTION_NAME]).isEqualTo(
                H2DatabaseDetails(
                        username = "",
                        password = ""
                )
        )
        assertThat(contextGenerator.context).isEqualTo(dto)
    }
}