/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles

const val API_V1_MEMBERS = "/api/v1/members"
const val API_V1_INSTALLATION = "/api/v1/installation"

@ActiveProfiles("disableAutoInstall")
internal class InstallationResourceIT : AbstractBaseResourceIT() {

    @Test
    fun shouldInstall_whenCallInstallationEndpoint() {
        // when
        val memberResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`()[API_V1_MEMBERS]

        // then
        assertErrorInfo(memberResponse, CommonErrorInfo.NOT_INSTALLED)

        // when
        val installationStatusBeforeResponse = given()
                .`when`()[API_V1_INSTALLATION]

        // then
        installationStatusBeforeResponse.then().status(HttpStatus.NOT_FOUND)

        // when
        val installationResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(installationDto())
                .`when`().post(API_V1_INSTALLATION)

        // then
        installationResponse.then().status(HttpStatus.OK)

        // when
        val installationAgainResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(installationDto())
                .`when`().post(API_V1_INSTALLATION)

        // then
        assertErrorInfo(installationAgainResponse, InstallationErrorInfo.ALREADY_INSTALLED)

        // when
        val installationStatusAfterResponse = given()
                .`when`()[API_V1_INSTALLATION]

        // then
        installationStatusAfterResponse.then().status(HttpStatus.OK)

        // when
        val memberAgainResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`()[API_V1_MEMBERS]

        // then
        memberAgainResponse.then().status(HttpStatus.OK)
    }

    private fun installationDto() = InstallationRequestDto(
            board = InstallationBoardDto("My board"),
            administrator = InstallationAdminDto(
                    "administrator",
                    "admin@admin.com",
                    "administrator"
            ),
            database = InstallationH2DatabaseDto("dba", "dba")
    )

}