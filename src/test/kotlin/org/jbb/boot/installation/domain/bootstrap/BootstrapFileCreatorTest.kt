/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import arrow.core.Either
import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.installation.api.H2DatabaseDetails
import org.jbb.boot.installation.api.InstallationContextGenerator
import org.jbb.boot.installation.api.PostgresDatabaseDetails
import org.jbb.swanframework.application.RestartManager
import org.jbb.swanframework.application.directory.AppDirectory
import org.jbb.swanframework.arrow.assertRight
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.io.TempDir
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.nio.file.Path

@ExtendWith(MockitoExtension::class)
internal class BootstrapFileCreatorTest {
    @Mock
    private lateinit var appDirectory: AppDirectory

    @Mock
    private lateinit var contextGenerator: InstallationContextGenerator<*>

    @Mock
    private lateinit var restartManager: RestartManager

    private lateinit var bootstrapFileCreator: BootstrapFileCreator

    @BeforeEach
    internal fun setUp() {
        bootstrapFileCreator =
                BootstrapFileCreator(appDirectory, contextGenerator, ObjectMapper(), restartManager)
    }

    @Test
    fun shouldThrowInstallationAbortedException_whenNoContextProvided() {
        // when
        val installationResult = bootstrapFileCreator.install(null)

        // then
        assertThat(
                installationResult is Either.Left
                        && installationResult.value.message == "Missing database details"
        ).isTrue
    }

    @Test
    fun shouldCreateBootstrapFileAndAbortCurrentInstallation_whenH2ContextProvided_andNoBoostrapExists(
            @TempDir tempDir: Path
    ) {
        // given
        given(appDirectory.configPath()).willReturn(tempDir)
        given(contextGenerator.getLastProcessedContext()).willReturn("context")

        // when
        val installationResult = bootstrapFileCreator.install(H2DatabaseDetails("dba", "dba"))

        assertThat(tempDir.resolve("bootstrap.json")).exists()
        assertThat(tempDir.resolve("auto-install.json.tmp")).exists()
        assertThat(
                installationResult is Either.Left && installationResult.value.message
                        == "No longer installation without bootstrap file necessary. Ignore it"
        ).isTrue
    }

    @Test
    fun shouldCreateBootstrapFileAndAbortCurrentInstallation_whenPostgresContextProvided_andNoBoostrapExists(
            @TempDir tempDir: Path
    ) {
        // given
        given(appDirectory.configPath()).willReturn(tempDir)
        given(contextGenerator.getLastProcessedContext()).willReturn("context")

        // when
        val installationResult = bootstrapFileCreator.install(
                PostgresDatabaseDetails(
                        host = "localhost",
                        port = 5432,
                        databaseName = "def",
                        username = "dba",
                        password = "dba"
                )
        )

        // then
        assertThat(
                installationResult is Either.Left && installationResult.value.message
                        == "No longer installation without bootstrap file necessary. Ignore it"
        )

        assertThat(tempDir.resolve("bootstrap.json")).exists()
        assertThat(tempDir.resolve("auto-install.json.tmp")).exists()
    }

    @Test
    fun shouldPassInstall_whenBootstrapAlreadyExists(@TempDir tempDir: Path) {
        // given
        given(appDirectory.configPath()).willReturn(tempDir)
        tempDir.resolve("bootstrap.json").toFile().createNewFile()

        // when then
        bootstrapFileCreator.install(H2DatabaseDetails("dba", "dba")).assertRight()
    }
}