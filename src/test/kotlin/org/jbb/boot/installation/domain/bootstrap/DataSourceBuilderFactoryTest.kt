/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import com.zaxxer.hikari.HikariDataSource
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.application.directory.AppDirectory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.io.TempDir
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.nio.file.Path

@ExtendWith(MockitoExtension::class)
internal class DataSourceBuilderFactoryTest {

    @Mock
    private lateinit var appDirectory: AppDirectory

    @InjectMocks
    private lateinit var dataSourceBuilderFactory: DataSourceBuilderFactory

    @Test
    fun shouldReturnProperPrefilledDSBuilder_forH2Database(@TempDir tempDir: Path) {
        // given
        given(appDirectory.path()).willReturn(tempDir)

        // when
        val builder = dataSourceBuilderFactory.getDataSourceBuilder(
                Bootstrap(
                        H2DatabaseBoostrap(
                                username = "aaa",
                                password = "bbb"
                        )
                )
        )

        // then
        val ds = builder.build() as HikariDataSource
        assertThat(ds.driverClassName).isEqualTo("org.h2.Driver")
        assertThat(ds.jdbcUrl).isEqualTo("jdbc:h2:file:${tempDir.resolve("database")};DB_CLOSE_ON_EXIT=FALSE")
        assertThat(ds.username).isEqualTo("aaa")
        assertThat(ds.password).isEqualTo("bbb")
    }

    @Test
    fun shouldReturnProperPrefilledDSBuilder_forPostgresDatabase() {
        // when
        val builder = dataSourceBuilderFactory.getDataSourceBuilder(
                Bootstrap(
                        PostgresDatabaseBoostrap(
                                host = "localhost",
                                databaseName = "default",
                                port = 4444,
                                username = "omc",
                                password = "pass"
                        )
                )
        )

        // then
        val ds = builder.build() as HikariDataSource
        assertThat(ds.driverClassName).isEqualTo("org.postgresql.Driver")
        assertThat(ds.jdbcUrl).isEqualTo("jdbc:postgresql://localhost:4444/default")
        assertThat(ds.username).isEqualTo("omc")
        assertThat(ds.password).isEqualTo("pass")
    }
}