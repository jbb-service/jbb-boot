/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain

import arrow.core.Either
import com.github.zafarkhaja.semver.Version
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.installation.api.InstallationAbortedDueToActionRequired
import org.jbb.boot.installation.api.InstallationAction
import org.jbb.swanframework.application.AppMetadataProperties
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.eventbus.DomainEventBus
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.any
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.jupiter.MockitoExtension
import java.time.Instant

@ExtendWith(MockitoExtension::class)
internal class InstallationServiceTest {

    @Mock
    private lateinit var appMetadataProperties: AppMetadataProperties

    @Mock
    private lateinit var logRepository: InstallationLogRepository

    @Mock
    private lateinit var stepRepository: InstallationStepRepository

    @Mock
    private lateinit var domainEventBus: DomainEventBus

    private val installedStep1 = InstallationStepEntity(
            actionName = "action1",
            fromVersion = Version.forIntegers(1).toString(),
            ranOnVersion = Version.forIntegers(1).toString(),
            startedAt = Instant.now(),
            finishedAt = Instant.now(),
            status = InstallationStepStatus.SUCCESS,
            errorMessage = null
    )

    private val installAction1 = mock(InstallationAction::class.java).also {
        given(it.name()).willReturn("action1")
    }

    private val installAction2 = mock(InstallationAction::class.java).also {
        given(it.name()).willReturn("action2")
        given(it.fromVersion()).willReturn(Version.valueOf("1.0.0"))
    }

    @Test
    fun returnAlreadyInstalled_whenNoStepsToRun() {
        // given
        val installationService =
                InstallationService(
                        listOf(installAction1),
                        appMetadataProperties,
                        logRepository,
                        stepRepository,
                        domainEventBus
                )

        given(stepRepository.findAll()).willReturn(listOf(installedStep1))

        // when
        val result = installationService.install(emptyMap())

        // then
        assertThat(result.isLeft()).isTrue
    }

    @Test
    fun shouldConsumeInstallationAbortedDueToActionRequired() {
        // given
        val installationService =
                InstallationService(
                        listOf(installAction2),
                        appMetadataProperties,
                        logRepository,
                        stepRepository,
                        domainEventBus
                )

        given(appMetadataProperties.version).willReturn("1.0.0")
        given(stepRepository.findAll()).willReturn(emptyList())
        given(installAction2.install(any())).willReturn(
                Either.Left(InstallationAbortedDueToActionRequired(""))
        )

        // when
        val result = installationService.install(emptyMap())

        // then
        assertThat(result.isRight()).isTrue
    }

    @Test
    fun shouldStopInstallation_andThrowException_whenInstallationOfActionFailed() {
        // given
        val installationService =
                InstallationService(
                        listOf(installAction2),
                        appMetadataProperties,
                        logRepository,
                        stepRepository,
                        domainEventBus
                )

        given(appMetadataProperties.version).willReturn("1.0.0")
        given(stepRepository.findAll()).willReturn(emptyList())
        given(installAction2.install(any())).willThrow(IllegalStateException(""))

        // when then
        assertThrows(IllegalStateException::class.java) {
            installationService.install(emptyMap()).assertRight()

        }
    }
}