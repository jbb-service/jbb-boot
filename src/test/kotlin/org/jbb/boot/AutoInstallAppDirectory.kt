/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot

import org.jbb.swanframework.application.directory.AppDirectory
import org.jbb.swanframework.application.directory.CONFIG
import org.springframework.util.FileSystemUtils
import java.nio.file.Files
import java.nio.file.Path
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


internal class AutoInstallAppDirectory(private val useAutoInstallFile: Boolean) : AppDirectory {
    private val appPath: Path = Files.createTempDirectory("jbb-test-")
    private val appConfigPath: Path = appPath.resolve(CONFIG)

    init {
        Files.createDirectory(appConfigPath)
    }

    @PostConstruct
    override fun create(): Boolean {
        if (useAutoInstallFile) {
            val autoInstallJson =
                    this::class.java.classLoader.getResource("auto-install.json").readText()
            Files.write(appConfigPath.resolve("auto-install.json"), autoInstallJson.toByteArray())
        }
        return true
    }

    override fun path(): Path = appPath

    override fun configPath(): Path = appConfigPath

    @PreDestroy
    fun destroy() {
        FileSystemUtils.deleteRecursively(appPath)
    }
}