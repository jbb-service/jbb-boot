/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot

import org.jbb.boot.installation.domain.AutoInstaller
import org.jbb.swanframework.application.RestartManager
import org.springframework.context.annotation.*

@Configuration
@Profile("!disableAutoInstall")
internal class AutoInstallConfiguration {

    @Bean
    fun autoInstallAppDirectory(): AutoInstallAppDirectory {
        return AutoInstallAppDirectory(true)
    }

    @Bean
    @Primary
    fun dummyRestartManager(@Lazy autoInstaller: AutoInstaller): RestartManager {
        return object : RestartManager() {
            override fun restartApp() {
                autoInstaller.installIfPossible()
            }
        }
    }

}

@Configuration
@Profile("disableAutoInstall")
internal class NoAutoInstallConfiguration {

    @Bean
    fun autoInstallAppDirectory(): AutoInstallAppDirectory {
        return AutoInstallAppDirectory(false)
    }

    @Bean
    @Primary
    fun dummyRestartManager(@Lazy autoInstaller: AutoInstaller): RestartManager {
        return object : RestartManager() {
            override fun restartApp() {
                autoInstaller.installIfPossible()
            }
        }
    }
}