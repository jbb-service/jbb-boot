/*
 * Copyright (C) 2022 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.board.web

import io.restassured.module.mockmvc.RestAssuredMockMvc
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType

const val API_V1_BOARD_SETTINGS = "/api/v1/board-settings"

internal class BoardSettingsResourceIT : AbstractBaseResourceIT() {

    @Test
    fun everybodyShouldBeAbleToGetBoardSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_BOARD_SETTINGS)

        // then
        val boardSettings = response.`as`(BoardSettingsDto::class.java)

        assertThat(boardSettings.boardName).isNotEmpty
        assertThat(boardSettings.boardDescription).isNotNull
        assertThat(boardSettings.welcomeMessage).isNotNull
    }

    @Test
    fun adminShouldBeAbleToUpdateBoardSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(BoardSettingsDto("board name", "example description", "example welcome"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_BOARD_SETTINGS)

        // then
        val boardSettings = response.`as`(BoardSettingsDto::class.java)

        assertThat(boardSettings.boardName).isEqualTo("board name")
        assertThat(boardSettings.boardDescription).isEqualTo("example description")
        assertThat(boardSettings.welcomeMessage).isEqualTo("example welcome")
    }


    @Test
    fun boardSettingsUpdate_shouldFailed_whenEmptyBoardNameProvided() {
        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(BoardSettingsDto("", "example description", "example welcome"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_BOARD_SETTINGS)

        // then
        assertErrorInfo(
                response, BoardErrorInfo.BOARD_SETTINGS_UPDATE_FAILED,
                ErrorDetail("boardName", null, "must not be blank"),
                ErrorDetail("boardName", null, "length must be between 1 and 255")
        )
    }

    @Test
    fun simpleUserShouldNotBeAbleToUpdateBoardSettings() {
        // given
        val member = testMemberHelper.createMember()

        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(member.httpBasic())
                .body(BoardSettingsDto("name", "", ""))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_BOARD_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUpdateBoardSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .body(BoardSettingsDto("name", "", ""))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_BOARD_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}