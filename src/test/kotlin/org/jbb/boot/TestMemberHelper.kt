/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot

import org.apache.commons.lang3.RandomStringUtils
import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.Member
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.swanframework.arrow.assertRight
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.request.RequestPostProcessor

private val pass = "pass"

@Component
class TestMemberHelper(private val memberWriteFacade: MemberWriteFacade) {

    fun createMember(): Member = memberWriteFacade.createMember(createMemberCommand()).assertRight()

    private fun createMemberCommand() = CreateMemberCommand(
            username = RandomStringUtils.randomAlphabetic(10),
            email = RandomStringUtils.randomAlphabetic(5) + "@gmail.com",
            password = pass.toCharArray()
    )

}

fun Member.password(): String = pass
fun Member.httpBasic(): RequestPostProcessor = httpBasic(this.username, pass)