/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.signin.web.settings

import io.restassured.module.mockmvc.RestAssuredMockMvc
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.password
import org.jbb.boot.signin.api.LoginType
import org.jbb.boot.signin.api.SignInSettings
import org.jbb.boot.signin.api.SignInSettingsFacade
import org.jbb.boot.signin.web.API_V1_SIGN_IN
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import java.util.*

const val API_V1_SIGN_IN_SETTINGS = "/api/v1/sign-in-settings"

internal class SignInSettingsResourceIT : AbstractBaseResourceIT() {

    @Autowired
    private lateinit var signInSettingsFacade: SignInSettingsFacade

    @Test
    fun everybodyShouldBeAbleToGetSignInSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_SIGN_IN_SETTINGS)

        // then
        val signInSettings = response.`as`(SignInSettingsDto::class.java)

        assertThat(signInSettings.loginType).isNotNull
        assertThat(signInSettings.loginCaseSensitive).isNotNull
    }

    @Test
    fun adminShouldBeAbleToUpdateSignInSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(SignInSettingsDto(LoginType.USERNAME, false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_SIGN_IN_SETTINGS)

        // then
        val signInSettings = response.`as`(SignInSettingsDto::class.java)
        assertThat(signInSettings.loginType).isEqualTo(LoginType.USERNAME)
        assertThat(signInSettings.loginCaseSensitive).isFalse
    }

    @Test
    fun simpleUserShouldNotBeAbleToUpdateSignInSettings() {
        // given
        val member = testMemberHelper.createMember()

        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(member.httpBasic())
                .body(SignInSettingsDto(LoginType.USERNAME, false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_SIGN_IN_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUpdateSignInSettings() {
        // when
        val response = RestAssuredMockMvc.given()
                .body(SignInSettingsDto(LoginType.USERNAME, false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_SIGN_IN_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun loginTypeUsernameCaseInsensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.USERNAME, false))
        val member = testMemberHelper.createMember()

        // when then
        assertThat(performSignIn(member.username, member.password()).statusCode).isEqualTo(204)
        assertThat(
                performSignIn(
                        member.username.uppercase(Locale.getDefault()),
                        member.password()
                ).statusCode
        ).isEqualTo(204)
        assertErrorInfo(
                performSignIn(member.email, member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertErrorInfo(
                performSignIn(member.email.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
    }

    @Test
    fun loginTypeUsernameCaseSensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.USERNAME, true))
        val member = testMemberHelper.createMember()

        // when then
        assertThat(performSignIn(member.username, member.password()).statusCode).isEqualTo(204)
        assertErrorInfo(
                performSignIn(member.username.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertErrorInfo(
                performSignIn(member.email, member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertErrorInfo(
                performSignIn(member.email.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
    }

    @Test
    fun loginTypeEmailCaseInsensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.EMAIL, false))
        val member = testMemberHelper.createMember()

        // when then
        assertErrorInfo(
                performSignIn(member.username, member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertErrorInfo(
                performSignIn(member.username.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertThat(performSignIn(member.email, member.password()).statusCode).isEqualTo(204)
        assertThat(
                performSignIn(
                        member.email.uppercase(Locale.getDefault()),
                        member.password()
                ).statusCode
        ).isEqualTo(204)
    }

    @Test
    fun loginTypeEmailCaseSensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.EMAIL, true))
        val member = testMemberHelper.createMember()

        // when then
        assertErrorInfo(
                performSignIn(member.username, member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertErrorInfo(
                performSignIn(member.username.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertThat(performSignIn(member.email, member.password()).statusCode).isEqualTo(204)
        assertErrorInfo(
                performSignIn(member.email.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
    }

    @Test
    fun loginTypeUsernameOrEmailCaseInsensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.USERNAME_OR_EMAIL, false))
        val member = testMemberHelper.createMember()

        // when then
        assertThat(performSignIn(member.username, member.password()).statusCode).isEqualTo(204)
        assertThat(
                performSignIn(
                        member.username.uppercase(Locale.getDefault()),
                        member.password()
                ).statusCode
        ).isEqualTo(204)
        assertThat(performSignIn(member.email, member.password()).statusCode).isEqualTo(204)
        assertThat(
                performSignIn(
                        member.email.uppercase(Locale.getDefault()),
                        member.password()
                ).statusCode
        ).isEqualTo(204)
    }

    @Test
    fun loginTypeUsernameOrEmailCaseSensitive_shouldWork() {
        // given
        setSignInSettings(SignInSettings(LoginType.USERNAME_OR_EMAIL, true))
        val member = testMemberHelper.createMember()

        // when then
        assertThat(performSignIn(member.username, member.password()).statusCode).isEqualTo(204)
        assertErrorInfo(
                performSignIn(member.username.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
        assertThat(performSignIn(member.email, member.password()).statusCode).isEqualTo(204)
        assertErrorInfo(
                performSignIn(member.email.uppercase(Locale.getDefault()), member.password()),
                CommonErrorInfo.BAD_CREDENTIALS
        )
    }

    @AfterEach
    internal fun setDefault() {
        setSignInSettings(SignInSettings(LoginType.USERNAME, false))
    }

    private fun setSignInSettings(signInSettings: SignInSettings) {
        signInSettingsFacade.updateSignInSettings(signInSettings)
    }

    private fun performSignIn(login: String, password: String) =
            RestAssuredMockMvc.given().contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .`when`().post(String.format(API_V1_SIGN_IN, login, password))

}