/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web

import io.swagger.v3.oas.annotations.Hidden
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Hidden
@Controller
@RequestMapping(value = ["/api/test"], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class TestAuthResource {

    @GetMapping("/default")
    @ResponseBody
    fun get(): String {
        return "default"
    }

    @PreAuthorize(PERMIT_ALL)
    @ResponseBody
    @GetMapping("/permit-all")
    fun getPermitAll(): String = "permitAll"

    @PreAuthorize(IS_AUTHENTICATED)
    @ResponseBody
    @GetMapping("/authenticated")
    fun getAuthenticated(): String = "authenticated"

    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @ResponseBody
    @GetMapping("/admin")
    fun getAdmin(): String {
        return "admin"
    }
}