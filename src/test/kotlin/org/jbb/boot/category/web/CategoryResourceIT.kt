/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.web

import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.response.MockMvcResponse
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.category.api.CategoryId
import org.jbb.boot.category.domain.CategoryRepository
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_CATEGORIES = "/api/v1/categories"
const val API_V1_CATEGORIES_ID = "$API_V1_CATEGORIES/{categoryId}"
const val API_V1_CATEGORIES_ID_DETAILS = "${API_V1_CATEGORIES_ID}/details"
const val API_V1_CATEGORIES_ID_LOCATION = "${API_V1_CATEGORIES_ID}/location"
const val API_V1_CATEGORIES_TREE_VIEW = "/api/v1/categories-tree-view"

internal class CategoryResourceIT : AbstractBaseResourceIT() {

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    @BeforeEach
    fun cleanUp() = categoryRepository.deleteAll()

    @Test
    fun everybodyShouldBeAbleToGetCategories() {
        // when
        val response = getCategories()

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK.value())
    }

    @Test
    fun shouldCreateAThreeTopCategories_andGetItAll() {
        // when
        createCategory(topCategoryCreateRequest("a"))
        createCategory(topCategoryCreateRequest("b"))
        createCategory(topCategoryCreateRequest("c"))

        val getResponse = getCategories()

        // then
        val categories = getResponse.extractListFromBody(CategoryDto::class.java)
        assertThat(categories[0].details.slug).isEqualTo("a")
        assertThat(categories[0].location).isEqualTo(CategoryLocationDto(null, 0))
        assertThat(categories[1].details.slug).isEqualTo("b")
        assertThat(categories[1].location).isEqualTo(CategoryLocationDto(null, 1))
        assertThat(categories[2].details.slug).isEqualTo("c")
        assertThat(categories[2].location).isEqualTo(CategoryLocationDto(null, 2))
    }

    @Test
    fun shouldCreateTopCategory_andTwoSubcategories_andGetItAll() {
        // when
        val createdTopCategory =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        createCategory(subcategoryCreateRequest("b", createdTopCategory.categoryId))
        createCategory(subcategoryCreateRequest("c", createdTopCategory.categoryId))

        val getResponse = getCategories()

        // then
        val categories = getResponse.extractListFromBody(CategoryDto::class.java)
        assertThat(categories[0].details.slug).isEqualTo("a")
        assertThat(categories[0].location).isEqualTo(CategoryLocationDto(null, 0))
        assertThat(categories[1].details.slug).isEqualTo("b")
        assertThat(categories[1].location).isEqualTo(
                CategoryLocationDto(
                        createdTopCategory.categoryId,
                        0
                )
        )
        assertThat(categories[2].details.slug).isEqualTo("c")
        assertThat(categories[2].location).isEqualTo(
                CategoryLocationDto(
                        createdTopCategory.categoryId,
                        1
                )
        )
    }

    @Test
    fun shouldNotCreateCategory_whenSlugIsBusy() {
        // given
        createCategory(topCategoryCreateRequest("a"))

        // when
        val response = createCategory(topCategoryCreateRequest("a"))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail(
                        "slug",
                        "slug-already-exists",
                        "Slug is already used in different category"
                )
        )
    }

    @Test
    fun shouldNotCreateCategory_whenParentCategoryIdIsNotFound() {
        // when
        val response = createCategory(subcategoryCreateRequest("a", "not-found"))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("parentCategoryId", null, "Parent category does not exist")
        )
    }

    @Test
    fun shouldNotCreateCategory_whenParentCategoryIdIsASubcategory() {
        // given
        val topCategory =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val subcategory = createCategory(
                subcategoryCreateRequest(
                        "b",
                        topCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        // when
        val response = createCategory(subcategoryCreateRequest("c", subcategory.categoryId))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("parentCategoryId", null, "Parent cannot have a parent")
        )
    }

    @Test
    fun shouldGetCategoryById() {
        // given
        val expectedCategory =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)

        // when
        val category =
                getCategory(expectedCategory.categoryId).extractFromBody(CategoryDto::class.java)

        // then
        assertThat(category).isEqualTo(expectedCategory)
    }

    @Test
    fun shouldNotGetCategoryById_whenItIsNotFound() {
        // when
        val response = getCategory("not-existing-id")

        // then
        assertErrorInfo(response, CategoryErrorInfo.CATEGORY_NOT_FOUND)
    }

    @Test
    fun shouldUpdateCategoryDetails() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val newDetailsDto = CategoryDetailsDto("new name", "new-slug", "new description")

        // when then
        updateDetails(category.categoryId, newDetailsDto).then().status(HttpStatus.OK)
    }

    @Test
    fun shouldNotUpdateCategoryDetails_whenCategoryNotFound() {
        // given
        val newDetailsDto = CategoryDetailsDto("new name", "new-slug", "new description")

        // when
        val response = updateDetails("not-found", newDetailsDto)

        // then
        assertErrorInfo(response, CategoryErrorInfo.CATEGORY_NOT_FOUND)
    }

    @Test
    fun shouldNotUpdateCategoryDetails_whenSlugIsBusy() {
        // given
        createCategory(topCategoryCreateRequest("a"))
        val category =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        val newDetailsDto = CategoryDetailsDto("new name", "a", "new description")

        // when
        val response = updateDetails(category.categoryId, newDetailsDto)

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail(
                        "slug",
                        "slug-already-exists",
                        "Slug is already used in different category"
                )
        )
    }

    @Test
    fun shouldNotUpdateLocation_whenPositionIsNegative() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)

        // when
        val response = updateLocation(category.categoryId, CategoryLocationDto(null, -1))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("position", null, "must be greater than or equal to 0")
        )
    }

    @Test
    fun shouldNotUpdateLocation_whenCategoryIsNotFound() {
        // when
        val response = updateLocation("not-found", CategoryLocationDto(null, 0))

        // then
        assertErrorInfo(response, CategoryErrorInfo.CATEGORY_NOT_FOUND)
    }

    @Test
    fun shouldNotUpdateLocation_whenParentIsNotFound() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)

        // when
        val response = updateLocation(category.categoryId, CategoryLocationDto("not-found", 0))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("parentCategoryId", null, "Parent category does not exist")
        )
    }

    @Test
    fun shouldNotUpdateLocation_whenParentIsAnCategoryBeingUpdated() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)

        // when
        val response =
                updateLocation(category.categoryId, CategoryLocationDto(category.categoryId, 0))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("parentCategoryId", null, "Category cannot be its own parent")
        )
    }

    @Test
    fun shouldNotBeAbleToUsePositionWhichDoesNotMakeSense() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)

        // when
        val response = updateLocation(category.categoryId, CategoryLocationDto(null, 1))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("position", "too-large", "Position is too large")
        )
    }

    @Test
    fun subCategory_cannotBeAParent() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne =
                createCategory(subcategoryCreateRequest("b", category.categoryId)).extractFromBody(
                        CategoryDto::class.java
                )
        val subCategoryTwo =
                createCategory(subcategoryCreateRequest("c", category.categoryId)).extractFromBody(
                        CategoryDto::class.java
                )

        // when
        val response = updateLocation(
                subCategoryTwo.categoryId,
                CategoryLocationDto(subCategoryOne.categoryId, 0)
        )

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail("parentCategoryId", null, "Parent cannot have a parent")
        )
    }

    @Test
    fun cannotMoveTopCategoryToSubcategory_whenItContainsChildren() {
        // given
        val category =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        createCategory(subcategoryCreateRequest("b", category.categoryId))
        createCategory(subcategoryCreateRequest("c", category.categoryId))
        val categoryTwo =
                createCategory(topCategoryCreateRequest("d")).extractFromBody(CategoryDto::class.java)

        // when
        val response =
                updateLocation(category.categoryId, CategoryLocationDto(categoryTwo.categoryId, 0))

        // then
        assertErrorInfo(
                response,
                CategoryErrorInfo.CATEGORY_VALIDATION_FAILED,
                ErrorDetail(
                        "parentCategoryId",
                        null,
                        "Cannot change top category to subcategory when it contains any child"
                )
        )
    }

    @Test
    fun swappingTwoTopCategories() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        // when
        updateLocation(categoryTwo.categoryId, CategoryLocationDto(null, 0))
                .then().status(HttpStatus.OK)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[0].location.position).isEqualTo(0)
        assertThat(categories[1].categoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[1].location.position).isEqualTo(1)
    }

    @Test
    fun swappingTwoSubcategories() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne = createCategory(
                subcategoryCreateRequest(
                        "b",
                        categoryOne.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val subCategoryTwo = createCategory(
                subcategoryCreateRequest(
                        "c",
                        categoryOne.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        // when
        updateLocation(subCategoryTwo.categoryId, CategoryLocationDto(categoryOne.categoryId, 0))
                .then().status(HttpStatus.OK)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)

        assertThat(categories[1].categoryId).isEqualTo(subCategoryTwo.categoryId)
        assertThat(categories[1].location.position).isEqualTo(0)
        assertThat(categories[1].location.parentCategoryId).isEqualTo(categoryOne.categoryId)

        assertThat(categories[2].categoryId).isEqualTo(subCategoryOne.categoryId)
        assertThat(categories[2].location.position).isEqualTo(1)
        assertThat(categories[2].location.parentCategoryId).isEqualTo(categoryOne.categoryId)
    }

    @Test
    fun movingSubcategoryToTheTop() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne = createCategory(
                subcategoryCreateRequest(
                        "c",
                        categoryTwo.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val subCategoryTwo = createCategory(
                subcategoryCreateRequest(
                        "d",
                        categoryTwo.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        // when
        updateLocation(subCategoryOne.categoryId, CategoryLocationDto(null, 1))
                .then().status(HttpStatus.OK)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)

        assertThat(categories[1].categoryId).isEqualTo(subCategoryOne.categoryId)
        assertThat(categories[1].location.position).isEqualTo(1)
        assertThat(categories[1].location.parentCategoryId).isNull()

        assertThat(categories[2].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[2].location.position).isEqualTo(2)
        assertThat(categories[2].location.parentCategoryId).isNull()

        assertThat(categories[3].categoryId).isEqualTo(subCategoryTwo.categoryId)
        assertThat(categories[3].location.position).isEqualTo(0)
        assertThat(categories[3].location.parentCategoryId).isEqualTo(categoryTwo.categoryId)
    }

    @Test
    fun removingNotExistingCategory_shouldFail() {
        // when
        val response = deleteCategory("not-existing")

        // then
        assertErrorInfo(response, CategoryErrorInfo.CATEGORY_NOT_FOUND)

    }

    @Test
    fun removingTopCategoryWithoutChildren() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        // when
        deleteCategory(categoryOne.categoryId).then().status(HttpStatus.NO_CONTENT)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)
    }

    @Test
    fun removingTopCategory_withChildren_withoutCascade() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne = createCategory(subcategoryCreateRequest("aa", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        val subCategoryTwo = createCategory(subcategoryCreateRequest("ab", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        // when
        deleteCategory(categoryOne.categoryId, cascade = false).then().status(HttpStatus.NO_CONTENT)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(subCategoryOne.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)

        assertThat(categories[1].categoryId).isEqualTo(subCategoryTwo.categoryId)
        assertThat(categories[1].location.parentCategoryId).isNull()
        assertThat(categories[1].location.position).isEqualTo(1)

        assertThat(categories[2].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[2].location.parentCategoryId).isNull()
        assertThat(categories[2].location.position).isEqualTo(2)
    }

    @Test
    fun removingTopCategory_withChildren_withCascade() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        createCategory(subcategoryCreateRequest("aa", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        createCategory(subcategoryCreateRequest("ab", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        // when
        deleteCategory(categoryOne.categoryId, cascade = true).then().status(HttpStatus.NO_CONTENT)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)
    }

    @Test
    fun removingSubcategory() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne = createCategory(subcategoryCreateRequest("aa", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        val subCategoryTwo = createCategory(subcategoryCreateRequest("ab", categoryOne.categoryId))
                .extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)

        // when
        deleteCategory(subCategoryOne.categoryId).then().status(HttpStatus.NO_CONTENT)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)

        assertThat(categories[1].categoryId).isEqualTo(subCategoryTwo.categoryId)
        assertThat(categories[1].location.parentCategoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[1].location.position).isEqualTo(0)

        assertThat(categories[2].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[2].location.parentCategoryId).isNull()
        assertThat(categories[2].location.position).isEqualTo(1)
    }

    @Test
    fun movingTopCategoryToTheSublevel() {
        // given
        val categoryOne =
                createCategory(topCategoryCreateRequest("a")).extractFromBody(CategoryDto::class.java)
        val categoryTwo =
                createCategory(topCategoryCreateRequest("b")).extractFromBody(CategoryDto::class.java)
        val subCategoryOne = createCategory(
                subcategoryCreateRequest(
                        "c",
                        categoryTwo.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val subCategoryTwo = createCategory(
                subcategoryCreateRequest(
                        "d",
                        categoryTwo.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        // when
        updateLocation(categoryOne.categoryId, CategoryLocationDto(categoryTwo.categoryId, 1))
                .then().status(HttpStatus.OK)

        val categories = getCategories().extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(categories[0].categoryId).isEqualTo(categoryTwo.categoryId)
        assertThat(categories[0].location.parentCategoryId).isNull()
        assertThat(categories[0].location.position).isEqualTo(0)

        assertThat(categories[1].categoryId).isEqualTo(subCategoryOne.categoryId)
        assertThat(categories[1].location.position).isEqualTo(0)
        assertThat(categories[1].location.parentCategoryId).isEqualTo(categoryTwo.categoryId)

        assertThat(categories[2].categoryId).isEqualTo(categoryOne.categoryId)
        assertThat(categories[2].location.position).isEqualTo(1)
        assertThat(categories[2].location.parentCategoryId).isEqualTo(categoryTwo.categoryId)

        assertThat(categories[3].categoryId).isEqualTo(subCategoryTwo.categoryId)
        assertThat(categories[3].location.position).isEqualTo(2)
        assertThat(categories[3].location.parentCategoryId).isEqualTo(categoryTwo.categoryId)
    }

    @Test
    fun shouldGetCategoryBySlug() {
        // when
        val getResponse = getCategoriesBySlug("not-existing")

        // then
        val categories = getResponse.extractListFromBody(CategoryDto::class.java)
        assertThat(categories).isEmpty()

        // when
        createCategory(topCategoryCreateRequest("de"))
        assertThat(getCategoriesBySlug("d").extractListFromBody(CategoryDto::class.java)).isEmpty()

        val slugCategories = getCategoriesBySlug("de").extractListFromBody(CategoryDto::class.java)

        // then
        assertThat(slugCategories).hasSize(1)
        assertThat(slugCategories.first().details.slug).isEqualTo("de")
    }

    @Test
    fun shouldGetTreeStructure() {
        // given
        val musicCategory =
                createCategory(topCategoryCreateRequest("music")).extractFromBody(CategoryDto::class.java)
        val moviesCategory =
                createCategory(topCategoryCreateRequest("movies")).extractFromBody(CategoryDto::class.java)
        val booksCategory =
                createCategory(topCategoryCreateRequest("books")).extractFromBody(CategoryDto::class.java)

        val rockCategory = createCategory(
                subcategoryCreateRequest(
                        "rock",
                        musicCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val popCategory = createCategory(
                subcategoryCreateRequest(
                        "pop",
                        musicCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val jazzCategory = createCategory(
                subcategoryCreateRequest(
                        "jazz",
                        musicCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        val horrorCategory = createCategory(
                subcategoryCreateRequest(
                        "horror",
                        moviesCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)
        val comedyCategory = createCategory(
                subcategoryCreateRequest(
                        "comedy",
                        moviesCategory.categoryId
                )
        ).extractFromBody(CategoryDto::class.java)

        // when
        val tree = getCategoriesTreeView().extractListFromBody(CategoryWithChildrenDto::class.java)

        // then
        assertThat(tree).hasSize(3)
        assertThat(tree[0].categoryId).isEqualTo(musicCategory.categoryId)
        assertThat(tree[0].childCategories[0].categoryId).isEqualTo(rockCategory.categoryId)
        assertThat(tree[0].childCategories[1].categoryId).isEqualTo(popCategory.categoryId)
        assertThat(tree[0].childCategories[2].categoryId).isEqualTo(jazzCategory.categoryId)
        assertThat(tree[1].categoryId).isEqualTo(moviesCategory.categoryId)
        assertThat(tree[1].childCategories[0].categoryId).isEqualTo(horrorCategory.categoryId)
        assertThat(tree[1].childCategories[1].categoryId).isEqualTo(comedyCategory.categoryId)
        assertThat(tree[2].categoryId).isEqualTo(booksCategory.categoryId)
    }

    private fun getCategoriesTreeView(): MockMvcResponse =
            RestAssuredMockMvc.given()
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .`when`().get(API_V1_CATEGORIES_TREE_VIEW)

    private fun getCategories(): MockMvcResponse =
            RestAssuredMockMvc.given()
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .`when`().get(API_V1_CATEGORIES)

    private fun getCategoriesBySlug(slug: String): MockMvcResponse =
            RestAssuredMockMvc.given()
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .queryParam("slug", slug)
                    .`when`().get(API_V1_CATEGORIES)

    private fun getCategory(categoryId: CategoryId): MockMvcResponse =
            RestAssuredMockMvc.given()
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .`when`().get(API_V1_CATEGORIES_ID, categoryId)

    private fun createCategory(requestDto: CreateCategoryRequestDto): MockMvcResponse =
            RestAssuredMockMvc.given().auth().with(adminHttpBasic)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .body(requestDto)
                    .`when`().post(API_V1_CATEGORIES)

    private fun updateDetails(
            categoryId: CategoryId,
            detailsDto: CategoryDetailsDto
    ): MockMvcResponse =
            RestAssuredMockMvc.given().auth().with(adminHttpBasic)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .body(detailsDto)
                    .`when`().put(API_V1_CATEGORIES_ID_DETAILS, categoryId)

    private fun updateLocation(
            categoryId: CategoryId,
            locationDto: CategoryLocationDto
    ): MockMvcResponse =
            RestAssuredMockMvc.given().auth().with(adminHttpBasic)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .body(locationDto)
                    .`when`().put(API_V1_CATEGORIES_ID_LOCATION, categoryId)

    private fun deleteCategory(
            categoryId: CategoryId,
            cascade: Boolean = false,
    ): MockMvcResponse =
            RestAssuredMockMvc.given().auth().with(adminHttpBasic)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .param("cascade", cascade)
                    .`when`().delete(API_V1_CATEGORIES_ID, categoryId)

    private fun topCategoryCreateRequest(slug: String) = CreateCategoryRequestDto(
            details = CategoryDetailsDto(
                    name = "example category $slug",
                    slug = slug,
                    description = "This is an example description for category $slug"
            ),
            parentCategoryId = null
    )

    private fun subcategoryCreateRequest(
            slug: String,
            parentCategoryId: CategoryId
    ) = CreateCategoryRequestDto(
            details = CategoryDetailsDto(
                    name = "example category $slug",
                    slug = slug,
                    description = "This is an example description for category $slug"
            ),
            parentCategoryId = parentCategoryId
    )
}