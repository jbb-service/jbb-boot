/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.health.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.swanframework.health.HealthStatus
import org.jbb.swanframework.health.web.HealthDto
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("disableAutoInstall")
internal class NotInstalledHealthResourceIT : AbstractBaseResourceIT() {
    @Test
    fun appShouldBeHealthy_evenBeforeInstallation() {
        // when
        val response = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`()[API_V1_HEALTH]

        // then
        response.then().status(HttpStatus.OK)

        val health = response.`as`(HealthDto::class.java)
        assertThat(health.status).isEqualTo(HealthStatus.HEALTHY)
    }

}