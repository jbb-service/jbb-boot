/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.lockout.web.settings.API_V1_LOCKOUT_SETTINGS
import org.jbb.boot.lockout.web.settings.LockoutSettingsDto
import org.jbb.boot.member.api.Member
import org.jbb.boot.password
import org.jbb.boot.signin.web.API_V1_SIGN_IN
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType

const val API_V1_MEMBER_ACTIVE_LOCK = "/api/v1/members/{memberId}/active-lock"

internal class MemberActiveLockResourceIT : AbstractBaseResourceIT() {

    @Test
    fun invalidAttemptsAboveLimit_shouldLockMember() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 3,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = true
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // when then
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)
    }

    @Test
    fun endpointShouldReturnLockNotFound_whenMemberIsNotLocked() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val lockResponse = given().auth().with(adminHttpBasic)
                .`when`().get(API_V1_MEMBER_ACTIVE_LOCK, testMember.memberId)

        // then
        assertErrorInfo(lockResponse, LockoutErrorInfo.ACTIVE_MEMBER_LOCK_NOT_FOUND)
    }

    @Test
    fun shouldReturnActiveLock_whenMemberIsLocked() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 1,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = true
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)

        // when
        val lockResponse = given().auth().with(adminHttpBasic)
                .`when`().get(API_V1_MEMBER_ACTIVE_LOCK, testMember.memberId)

        // then
        val memberLock = lockResponse.`as`(MemberLockDto::class.java)

        assertThat(memberLock.memberId).isEqualTo(testMember.memberId)
        assertThat(memberLock.active).isTrue
        assertThat(memberLock.createdAt).isNotNull
        assertThat(memberLock.expiresAt).isNotNull
    }

    @Test
    fun memberShouldBeAbleToSignIn_whenAdminRemovesLock() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 3,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = true
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // when then
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)

        // when
        val lockRemoveResponse = given().auth().with(adminHttpBasic)
                .`when`().delete(API_V1_MEMBER_ACTIVE_LOCK, testMember.memberId)
        assertThat(lockRemoveResponse.statusCode).isEqualTo(204)

        // then
        assertErrorInfo(
                given().auth().with(adminHttpBasic)
                        .`when`().get(API_V1_MEMBER_ACTIVE_LOCK, testMember.memberId),
                LockoutErrorInfo.ACTIVE_MEMBER_LOCK_NOT_FOUND
        )
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)

        // when then
        assertThat(performValidSignIn(testMember).statusCode()).isEqualTo(204)
    }


    @Test
    fun validSignInAttemptAfterLocking_shouldReturnLockError() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 2,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = true
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // when then
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)
        assertErrorInfo(performValidSignIn(testMember), CommonErrorInfo.MEMBER_HAS_BEEN_LOCKED)
    }

    @Test
    fun shouldResetInvalidAttempts_whenSuccessfulSignInPerformed() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 5,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = true
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // when then
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)

        assertThat(performValidSignIn(testMember).statusCode()).isEqualTo(204)

        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
    }

    @Test
    fun shouldIgnoreBadCredentials_whenLockingIsDisabled() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(
                        LockoutSettingsDto(
                                attemptsLimit = 4,
                                ignoreAttemptsFromMinutes = 30,
                                lockDurationMinutes = 15,
                                lockingEnabled = false
                        )
                )
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // when then
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertErrorInfo(performInvalidSignIn(testMember), CommonErrorInfo.BAD_CREDENTIALS)
        assertThat(performValidSignIn(testMember).statusCode()).isEqualTo(204)
    }

    private fun performInvalidSignIn(testMember: Member) =
            given().contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .`when`().post(String.format(API_V1_SIGN_IN, testMember.username, "badpass"))

    private fun performValidSignIn(testMember: Member) =
            given().contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .`when`()
                    .post(String.format(API_V1_SIGN_IN, testMember.username, testMember.password()))

}