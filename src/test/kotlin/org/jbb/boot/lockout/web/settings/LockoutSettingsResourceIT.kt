/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.lockout.web.settings

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.lockout.web.LockoutErrorInfo
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType

const val API_V1_LOCKOUT_SETTINGS = "/api/v1/lockout-settings"

internal class LockoutSettingsResourceIT : AbstractBaseResourceIT() {

    @Test
    fun adminShouldBeAbleToGetLockoutSettings() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_LOCKOUT_SETTINGS)

        // then
        val lockoutSettings = response.`as`(LockoutSettingsDto::class.java)

        assertThat(lockoutSettings.attemptsLimit).isGreaterThan(0)
        assertThat(lockoutSettings.ignoreAttemptsFromMinutes).isGreaterThan(0)
        assertThat(lockoutSettings.lockDurationMinutes).isGreaterThan(0)
        assertThat(lockoutSettings.lockingEnabled).isTrue
    }

    @Test
    fun normalUserShouldNotBeAbleToGetLockoutSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_LOCKOUT_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToGetLockoutSettings() {
        // when
        val response = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_LOCKOUT_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun adminShouldBeAbleToUpdateLockoutSettings() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(LockoutSettingsDto(3, 30, 15, true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // then
        val lockoutSettings = response.`as`(LockoutSettingsDto::class.java)
        assertThat(lockoutSettings.attemptsLimit).isEqualTo(3)
        assertThat(lockoutSettings.ignoreAttemptsFromMinutes).isEqualTo(30)
        assertThat(lockoutSettings.lockDurationMinutes).isEqualTo(15)
        assertThat(lockoutSettings.lockingEnabled).isTrue
    }

    @Test
    fun shouldFailed_whenZeroAttemptsLimit() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(LockoutSettingsDto(0, 30, 15, true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // then
        assertErrorInfo(
                response,
                LockoutErrorInfo.LOCKOUT_SETTINGS_UPDATE_FAILED,
                ErrorDetail("attemptsLimit", null, "must be greater than or equal to 1")
        )
    }

    @Test
    fun userShouldNotBeAbleToUpdateLockoutSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(LockoutSettingsDto(3, 30, 15, true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUpdateLockoutSettings() {
        // when
        val response = given()
                .body(LockoutSettingsDto(3, 30, 15, true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_LOCKOUT_SETTINGS)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}