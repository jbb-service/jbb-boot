/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.api.Member
import org.jbb.boot.password
import org.jbb.boot.password.web.PasswordErrorInfo
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_ME_EMAIL = "/api/v1/me/email"

internal class MeEmailResourceIT : AbstractBaseResourceIT() {

    private lateinit var testMember: Member

    @BeforeEach
    fun setUpMember() {
        testMember = testMemberHelper.createMember()
    }

    @Test
    fun memberShouldBeAbleToUpdateHisOwnEmail() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyEmailDto("pass", "new3@mail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        response.then().status(HttpStatus.NO_CONTENT)
    }


    @Test
    fun memberShouldBeAbleToUpdateHisOwnEmail_withTheSame() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyEmailDto("pass", testMember.email))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        response.then().status(HttpStatus.NO_CONTENT)
    }

    @Test
    fun shouldFailed_whenEmailIsNotValid() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyEmailDto(testMember.password(), "not-email"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        assertErrorInfo(
                response, MemberErrorInfo.EMAIL_UPDATE_FAILED,
                ErrorDetail("email", null, "must be a well-formed email address")
        )
    }

    @Test
    fun shouldFailed_whenEmailIsUsedByAnotherMember() {
        // given
        val anotherMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyEmailDto(testMember.password(), anotherMember.email))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        assertErrorInfo(
                response, MemberErrorInfo.EMAIL_UPDATE_FAILED,
                ErrorDetail("email", "email-already-used", "email is already used")
        )
    }

    @Test
    fun shouldFailed_whenCurrentPasswordIsInvalid() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyEmailDto("invalid_current_pass", "new4@mail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        assertErrorInfo(response, PasswordErrorInfo.INVALID_PASSWORD_FOR_CURRENT_MEMBER)
    }

    @Test
    fun shouldFailedWith401_whenNotSignInUserCalledEndpoint() {
        // when
        val response = given()
                .body(UpdateMyEmailDto("aaaa", "new5@mail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_EMAIL)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}