/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_EMAIL = "/api/v1/members/{memberId}/email"

internal class MemberEmailResourceIT : AbstractBaseResourceIT() {

    @Test
    fun adminShouldBeAbleToUpdateMemberEmail() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdateEmailDto("new1@gmail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, testMember.memberId)

        // then
        response.then().status(HttpStatus.OK)

        val member = response.`as`(MemberDto::class.java)
        assertThat(member.email).isEqualTo("new1@gmail.com")
    }

    @Test
    fun shouldReturnMemberNotFound_whenTryingToUpdatePasswordForNotExistingMember() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdateEmailDto("new1@gmail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, "not-existing-member-id")

        // then
        assertErrorInfo(response, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldFailed_whenEmailIsIncorrect() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdateEmailDto("not-email"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, testMember.memberId)

        // then
        assertErrorInfo(
                response, MemberErrorInfo.EMAIL_UPDATE_FAILED,
                ErrorDetail("email", null, "must be a well-formed email address")
        )
    }

    @Test
    fun shouldFailed_whenEmailIsUsedByAnotherMember() {
        // given
        val testMember = testMemberHelper.createMember()
        val anotherMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdateEmailDto(anotherMember.email))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, testMember.memberId)

        // then
        assertErrorInfo(
                response, MemberErrorInfo.EMAIL_UPDATE_FAILED,
                ErrorDetail("email", "email-already-used", "email is already used")
        )
    }

    @Test
    fun simpleUserShouldNotBeAbleToUseThatEndpointForChangingEmail() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateEmailDto("new2@gmail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUseThatEndpointForChangingEmail() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .body(UpdateEmailDto("new2@gmail.com"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_EMAIL, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}