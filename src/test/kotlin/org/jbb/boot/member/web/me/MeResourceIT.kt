/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web.me

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.api.Member
import org.jbb.boot.password.web.API_V1_PASSWORD
import org.jbb.boot.password.web.UpdatePasswordDto
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_ME = "/api/v1/me"

internal class MeResourceIT : AbstractBaseResourceIT() {

    @Test
    fun notLoggedUserShouldHaveNullCurrentMember() {
        // when
        val response = given().`when`()[API_V1_ME]

        // then
        response.then().status(HttpStatus.OK)
        val meData = response.`as`(MeDataDto::class.java)
        assertThat(meData.currentMember).isNull()
        assertThat(meData.sessionId).isNull()
    }

    @Test
    fun simpleUserShouldHaveAdministratorFlagSetToFalse() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_ME]

        // then
        response.then().status(HttpStatus.OK)

        val meData = response.`as`(MeDataDto::class.java)
        assertThat(meData.currentMember!!.administrator).isFalse
        assertThat(meData.sessionId).isNull()
    }

    @Test
    fun simpleUserShouldHavePasswordExpiredFlagSetToFalse() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_ME]

        // then
        response.then().status(HttpStatus.OK)

        val meData = response.`as`(MeDataDto::class.java)
        assertThat(meData.currentMember!!.hasPasswordExpired).isFalse
    }

    @Test
    fun simpleUserWithExpiredPassword_shouldHavePasswordExpiredFlagSetToTrue() {
        // given
        val testMember = testMemberHelper.createMember()
        expirePassword(testMember)

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .`when`()[API_V1_ME]

        // then
        response.then().status(HttpStatus.OK)

        val meData = response.`as`(MeDataDto::class.java)
        assertThat(meData.currentMember!!.hasPasswordExpired).isTrue
    }

    @Test
    fun adminShouldHaveAdministratorFlagSetToTrue() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .`when`()[API_V1_ME]

        // then
        response.then().status(HttpStatus.OK)

        val meData = response.`as`(MeDataDto::class.java)
        assertThat(meData.currentMember!!.administrator).isTrue
        assertThat(meData.sessionId).isNull()
    }

    private fun expirePassword(testMember: Member) {
        given().auth().with(adminHttpBasic)
                .body(UpdatePasswordDto(expired = true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)
    }

}