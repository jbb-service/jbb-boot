/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_MEMBERS = "/api/v1/members"
const val API_V1_MEMBERS_MEMBER_ID = "$API_V1_MEMBERS/{memberId}"

internal class MemberResourceIT : AbstractBaseResourceIT() {

    @Test
    fun shouldCreateGetAndDeleteMemberSuccessfully() {
        // when
        val response = given()
                .body(createMemberDto())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().post(API_V1_MEMBERS)

        // then
        response.then().status(HttpStatus.CREATED)
        val createdMember = response.`as`(MemberDto::class.java)
        assertThat(createdMember).isNotNull

        // when
        val memberId = createdMember.memberId
        val getResponse = given()
                .`when`()[API_V1_MEMBERS_MEMBER_ID, memberId]

        // then
        getResponse.then().status(HttpStatus.OK)
        val memberDto = getResponse.`as`(MemberDto::class.java)
        assertThat(memberDto.memberId).isEqualTo(memberId)

        // when
        val deleteResponse = given()
                .auth().with(adminHttpBasic)
                .`when`().delete(API_V1_MEMBERS_MEMBER_ID, memberId)
        deleteResponse.then().status(HttpStatus.NO_CONTENT)
        val getAgainResponse = given()
                .`when`()[API_V1_MEMBERS_MEMBER_ID, memberId]

        // then
        assertErrorInfo(getAgainResponse, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldCreationFail_whenUsernameIsBlank() {
        // given
        val createMemberDto = createMemberDto()
        val updatedMemberDto =
                createMemberDto.copy(username = "", email = "another-email@gmail3.com")

        // when
        val createResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(updatedMemberDto)
                .`when`().post(API_V1_MEMBERS)

        // then
        assertErrorInfo(createResponse, MemberErrorInfo.MEMBER_CREATION_FAILED,
                ErrorDetail("username", null, "length must be between 4 and 32"),
                ErrorDetail("username", null, "must not be blank"))
    }

    @Test
    fun shouldCreationFail_whenEmailIsNotValid() {
        // given
        val createMemberDto = createMemberDto()
        val updatedMemberDto = createMemberDto.copy(email = "invalid-email-jbb.com")

        // when
        val createResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(updatedMemberDto)
                .`when`().post(API_V1_MEMBERS)

        // then
        assertErrorInfo(createResponse, MemberErrorInfo.MEMBER_CREATION_FAILED,
                ErrorDetail("email", null, "must be a well-formed email address"))
    }

    @Test
    fun shouldCreationFail_whenUsernameIgnoreCaseIsAlreadyUsed() {
        // given
        val createMemberDto = createMemberDto()
        val updatedMemberDto = createMemberDto.copy(username = "test-user-3")
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(updatedMemberDto).`when`().post(API_V1_MEMBERS)

        // when
        val updatedMemberDto2 =
                createMemberDto.copy(username = "test-USER-3", email = "another-email-1@gmail.com")
        val createResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(updatedMemberDto2)
                .`when`().post(API_V1_MEMBERS)

        // then
        assertErrorInfo(
                createResponse, MemberErrorInfo.MEMBER_CREATION_FAILED,
                ErrorDetail("username", "username-already-used", "username is already used")
        )
    }

    @Test
    fun shouldCreationFail_whenEmailIgnoreCaseIsAlreadyUsed() {
        // given
        val createMemberDto =
                createMemberDto().copy(username = "test-user-5", email = "new-email-2020@gmail.com")
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(createMemberDto).`when`().post(API_V1_MEMBERS)

        // when
        val updatedMemberDto2 =
                createMemberDto.copy(username = "test-user-6", email = "new-eMail-2020@GMAIL.COM")
        val createResponse = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(updatedMemberDto2)
                .`when`().post(API_V1_MEMBERS)

        // then
        assertErrorInfo(
                createResponse, MemberErrorInfo.MEMBER_CREATION_FAILED,
                ErrorDetail("email", "email-already-used", "email is already used")
        )
    }

    @Test
    fun shouldReturnMemberNotFound_whenMemberDoesNotExist() {
        // when
        val getResponse = given()
                .`when`()[API_V1_MEMBERS_MEMBER_ID, "not-existing-member"]

        // then
        assertErrorInfo(getResponse, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldReturn4RecentMembers_whenLimitSetTo4() {
        // given
        for (i in 1..5) {
            given()
                    .body(createMemberDto(i))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .`when`().post(API_V1_MEMBERS)
        }
        // when
        val getManyResponse = given()
                .param("limit", 4)
                .`when`()[API_V1_MEMBERS]

        // then
        val page = getManyResponse.`as`(PagePublicMemberDto::class.java)
        assertThat(page.nextCursor).isNotEmpty
        assertThat(page.content).allMatch { it.username.startsWith("test-user-") }
    }

    @Test
    fun shouldFail_whenLimitSetToZero() {
        // when
        val getManyResponse = given()
                .param("limit", 0)
                .`when`()[API_V1_MEMBERS]

        // then
        assertErrorInfo(
                getManyResponse,
                CommonErrorInfo.VALIDATION_ERROR,
                ErrorDetail("limit", "findMembers.limit", "must be greater than or equal to 1")
        )
    }

    @Test
    fun shouldFail_whenLimitIsGreaterThan100() {
        // when
        val getManyResponse = given()
                .param("limit", 101)
                .`when`()[API_V1_MEMBERS]

        // then
        assertErrorInfo(
                getManyResponse,
                CommonErrorInfo.VALIDATION_ERROR,
                ErrorDetail("limit", "findMembers.limit", "must be less than or equal to 100")
        )
    }

    @Test
    fun shouldFail_whenTokenIsNotANumber() {
        // when
        val getManyResponse = given()
                .param("cursor", "xxx")
                .`when`()[API_V1_MEMBERS]

        // then
        assertErrorInfo(
                getManyResponse,
                CommonErrorInfo.VALIDATION_ERROR,
                ErrorDetail("cursor", "findMembers.cursor", "must match \"^[0-9]+\$\"")
        )
    }

    private fun createMemberDto(): CreateMemberDto {
        return CreateMemberDto("test-user", "test@jbb.com", "mypass")
    }

    private fun createMemberDto(number: Int): CreateMemberDto {
        return CreateMemberDto("test-user-$number", "test-$number@jbb.com", "mypass")
    }

}