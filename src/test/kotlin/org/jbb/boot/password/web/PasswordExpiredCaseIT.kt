/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.web.API_V1_MEMBERS
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

internal class PasswordExpiredCaseIT : AbstractBaseResourceIT() {

    @Test
    fun memberShouldGetMemberPasswordExpiredError_whenCallingBusinessEndpoint_whenHisPasswordIsExpired() {
        // given
        val testMember = testMemberHelper.createMember()

        given().auth().with(adminHttpBasic)
                .body(UpdatePasswordDto(expired = true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)
                .then().status(HttpStatus.NO_CONTENT)

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_MEMBERS)

        // then
        assertErrorInfo(response, CommonErrorInfo.MEMBER_PASSWORD_EXPIRED)
    }

}