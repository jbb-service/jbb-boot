/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.web.policy

import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.response.MockMvcResponse
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.api.Member
import org.jbb.boot.password
import org.jbb.boot.password.api.*
import org.jbb.boot.password.web.API_V1_ME_PASSWORD
import org.jbb.boot.password.web.PasswordErrorInfo
import org.jbb.boot.password.web.UpdateMyPasswordDto
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import java.util.*

const val API_V1_PASSWORD_POLICY = "/api/v1/password-policy"
const val API_V1_PASSWORD_BLACKLIST = "/api/v1/password-blacklist"


internal class PasswordPolicyResourceIT : AbstractBaseResourceIT() {

    @Autowired
    private lateinit var passwordPolicyFacade: PasswordPolicyFacade

    @Test
    fun everybodyShouldBeAbleToGetPasswordPolicy() {
        // when
        val response = RestAssuredMockMvc.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_PASSWORD_POLICY)

        // then
        val passwordPolicy = response.`as`(PasswordPolicyDto::class.java)

        assertThat(passwordPolicy).isNotNull
    }

    @Test
    fun simpleUserShouldNotBeAbleToUpdatePasswordPolicy() {
        // given
        val member = testMemberHelper.createMember()
        val passwordPolicyDto = currentPasswordPolicyDto()

        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(member.httpBasic())
                .body(passwordPolicyDto)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_POLICY)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUpdatePasswordPolicy() {
        // given
        val passwordPolicyDto = currentPasswordPolicyDto()

        // when
        val response = RestAssuredMockMvc.given()
                .body(passwordPolicyDto)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_POLICY)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun simpleUserShouldNotBeAbleToGetPasswordBlacklist() {
        // given
        val member = testMemberHelper.createMember()

        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(member.httpBasic())
                .body(PasswordBlacklistDto(listOf(), true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_BLACKLIST)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)

    }

    @Test
    fun anonymousShouldNotBeAbleToGetPasswordBlacklist() {
        // when
        val response = RestAssuredMockMvc.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(API_V1_PASSWORD_BLACKLIST)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun simpleUserShouldNotBeAbleToUpdatePasswordBlacklist() {
        // given
        val member = testMemberHelper.createMember()

        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(member.httpBasic())
                .body(PasswordBlacklistDto(listOf(), true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_BLACKLIST)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun anonymousShouldNotBeAbleToUpdatePasswordBlacklist() {
        // when
        val response = RestAssuredMockMvc.given()
                .body(PasswordBlacklistDto(listOf(), true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_BLACKLIST)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun passwordPolicyValidationFailed() {
        // when
        val response = RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(allowAllPolicyDto().copy(requiredCharacterTypes = 0))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_POLICY)

        // then
        assertErrorInfo(
                response,
                PasswordErrorInfo.PASSWORD_POLICY_UPDATE_FAILED,
                ErrorDetail(
                        "characterRules.requiredTypes",
                        null,
                        "must be greater than or equal to 1"
                )
        )
    }

    @Test
    fun passwordPolicyWithMinimumLength() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(length = PasswordLength(min = 6, max = null)))
        val response = testMember.setPassword("abcde")

        // then
        response.assertPasswordUpdateFailed(
                errorCode = "TooShort",
                message = "Password must be 6 or more characters in length"
        )
    }

    @Test
    fun passwordPolicyWithMaximumLength() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(length = PasswordLength(min = 4, max = 10)))
        val response = testMember.setPassword("abcdef78901")

        // then
        response.assertPasswordUpdateFailed(
                errorCode = "TooLong",
                message = "Password must be no more than 10 characters in length"
        )
    }

    @Test
    fun passwordPolicyWithTwoRequiredCharacterTypes() {
        // given
        val member = testMemberHelper.createMember()

        val errorCode = "InsufficientAmountOfCharacterTypes"
        val message = "Password requires 2 types of characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacterTypes = 2))

        // then
        member.setPassword("aaaaa").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("SAECD").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("12345").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("#&$*@").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("aaaaA").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithThreeRequiredCharacterTypes() {
        // given
        val member = testMemberHelper.createMember()

        val errorCode = "InsufficientAmountOfCharacterTypes"
        val message = "Password requires 3 types of characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacterTypes = 3))

        // then
        member.setPassword("7654456765").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("aaAA").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("1@@@@@@@3").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("aA1").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithFourRequiredCharacterTypes() {
        // given
        val member = testMemberHelper.createMember()

        val errorCode = "InsufficientAmountOfCharacterTypes"
        val message = "Password requires 4 types of characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacterTypes = 4))

        // then
        member.setPassword("aA1").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("1A#").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("@3a").assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("aA@1").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRequiredLowercaseCharacters() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password must contain 1 or more lowercase characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacters = RequiredCharacters(lowerCase = 1)))

        // then
        member.setPassword("A1^").assertPasswordUpdateFailed("InsufficientLowerCase", message)
        member.setPassword("A1^a").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRequiredUppercaseCharacters() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password must contain 2 or more uppercase characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacters = RequiredCharacters(upperCase = 2)))

        // then
        member.setPassword("aaaaaaaaa12345")
                .assertPasswordUpdateFailed("InsufficientUpperCase", message)
        member.setPassword("aaAaaaa1234")
                .assertPasswordUpdateFailed("InsufficientUpperCase", message)
        member.setPassword("aA1A").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRequiredDigitCharacters() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password must contain 4 or more digit characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacters = RequiredCharacters(digit = 4)))

        // then
        member.setPassword("aaaa12").assertPasswordUpdateFailed("InsufficientDigit", message)
        member.setPassword("a1b2c3d").assertPasswordUpdateFailed("InsufficientDigit", message)
        member.setPassword("a1b2c3d4").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRequiredSpecialCharacters() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password must contain 1 or more special characters"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(requiredCharacters = RequiredCharacters(special = 1)))

        // then
        member.setPassword("aA1").assertPasswordUpdateFailed("InsufficientSpecial", message)
        member.setPassword("a@1").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRejectingUsername() {
        // given
        val member = testMemberHelper.createMember()
        val anotherMember = testMemberHelper.createMember()
        val message = "Password must not contain a username"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(rejectUsername = true))

        // then
        member.setPassword(member.username).assertPasswordUpdateFailed("UsernameDetected", message)
        member.setPassword("${member.username}1")
                .assertPasswordUpdateFailed("UsernameDetected", message)
        member.setPassword(member.username.reversed()).assertPasswordUpdateSuccess()
        anotherMember.setPassword(member.username.uppercase(Locale.getDefault()))
                .assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRejectingLastPasswords() {
        // given
        val member = testMemberHelper.createMember()
        val errorCode = "LastPasswordDetected"
        val message = "Password matches one of 3 previous passwords"

        // when
        setPasswordPolicy(allowAllPolicyDto().copy(rejectLastPasswords = 3))

        // then
        member.setPassword("a").assertPasswordUpdateSuccess()
        member.setPassword("b", currentPassword = "a").assertPasswordUpdateSuccess()
        member.setPassword("c", currentPassword = "b").assertPasswordUpdateSuccess()
        member.setPassword("d", currentPassword = "c").assertPasswordUpdateSuccess()

        member.setPassword("c", currentPassword = "d")
                .assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("b", currentPassword = "d")
                .assertPasswordUpdateFailed(errorCode, message)
        member.setPassword("a", currentPassword = "d").assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRejectingBlacklistWithoutMatchingBackwards() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password contains word 'forbiddenPass' from black list"

        // when
        RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(PasswordBlacklistDto(listOf("forbiddenPass"), false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_BLACKLIST).then().status(HttpStatus.OK)


        setPasswordPolicy(allowAllPolicyDto().copy(useBlacklist = true))

        // then
        member.setPassword("forbiddenPass")
                .assertPasswordUpdateFailed("WordFromBlackListDetected", message)
        member.setPassword("forbiddenPass".reversed()).assertPasswordUpdateSuccess()
    }

    @Test
    fun passwordPolicyWithRejectingBlacklistWithMatchingBackwards() {
        // given
        val member = testMemberHelper.createMember()
        val message = "Password contains word 'forbiddenPass' from black list"
        val reversedMessage =
                "Password contains word 'forbiddenPass' from black list in reversed form"

        // when
        RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(PasswordBlacklistDto(listOf("forbiddenPass"), true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_BLACKLIST).then().status(HttpStatus.OK)


        setPasswordPolicy(allowAllPolicyDto().copy(useBlacklist = true))

        // then
        member.setPassword("forbiddenPass").assertPasswordUpdateFailed(
                "WordFromBlackListDetected",
                message
        )
        member.setPassword("forbiddenPass".reversed()).assertPasswordUpdateFailed(
                "ReversedWordFromBlackListDetected",
                reversedMessage
        )
    }

    @AfterEach
    internal fun setDefault() {
        passwordPolicyFacade.changePasswordPolicy(
                PasswordPolicy(
                        length = LengthRules(4, 128),
                        characterRules = CharacterRules(
                                requiredTypes = 1,
                                requiredCharacters = RequiredCharacterRules(
                                        lowerCase = 0,
                                        upperCase = 0,
                                        digit = 0,
                                        special = 0
                                )
                        ),
                        forbiddenRules = ForbiddenRules(
                                rejectUsername = false,
                                rejectLastPasswords = 0,
                                useBlacklist = false,
                                blacklistMatchBackwards = false
                        ),
                        expirationRules = ExpirationRules(days = null)
                )
        ).assertRight()
    }

    private fun setPasswordPolicy(passwordPolicyDto: PasswordPolicyDto) {
        val response = RestAssuredMockMvc.given()
                .auth().with(adminHttpBasic)
                .body(passwordPolicyDto)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_PASSWORD_POLICY)
        response.then().status(HttpStatus.OK)
    }

    private fun Member.setPassword(
            password: String,
            currentPassword: String? = null
    ): MockMvcResponse =
            RestAssuredMockMvc.given().auth().with(
                    httpBasic(this.username, currentPassword ?: this.password())
            )
                    .body(UpdateMyPasswordDto(currentPassword ?: this.password(), password))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .`when`().put(API_V1_ME_PASSWORD)

    private fun MockMvcResponse.assertPasswordUpdateFailed(errorCode: String, message: String) {
        assertErrorInfo(
                this, PasswordErrorInfo.PASSWORD_UPDATE_FAILED,
                ErrorDetail("newPassword", errorCode, message)
        )
    }

    private fun MockMvcResponse.assertPasswordUpdateSuccess() {
        this.then().status(HttpStatus.NO_CONTENT)
    }

    private fun allowAllPolicyDto() = PasswordPolicyDto(
            length = PasswordLength(min = 1, max = null),
            requiredCharacterTypes = 1,
            requiredCharacters = RequiredCharacters(
                    lowerCase = 0,
                    upperCase = 0,
                    digit = 0,
                    special = 0
            ),
            rejectUsername = false,
            rejectLastPasswords = 0,
            expireAfterDays = null,
            useBlacklist = false
    )

    private fun currentPasswordPolicyDto() = RestAssuredMockMvc.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .`when`().get(API_V1_PASSWORD_POLICY)
            .`as`(PasswordPolicyDto::class.java)

}