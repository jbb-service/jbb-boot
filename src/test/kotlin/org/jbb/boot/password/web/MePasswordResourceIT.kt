/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.api.Member
import org.jbb.boot.password
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_ME_PASSWORD = "/api/v1/me/password"

internal class MePasswordResourceIT : AbstractBaseResourceIT() {

    private lateinit var testMember: Member

    @BeforeEach
    fun setUpMember() {
        testMember = testMemberHelper.createMember()
    }

    @Test
    fun memberShouldBeAbleToUpdateHisOwnPassword() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyPasswordDto(testMember.password(), "newPass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_PASSWORD)

        // then
        response.then().status(HttpStatus.NO_CONTENT)
    }

    @Test
    fun shouldFailed_whenPasswordIsTooShort() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyPasswordDto(testMember.password(), "sho"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_PASSWORD)

        // then
        assertErrorInfo(
                response, PasswordErrorInfo.PASSWORD_UPDATE_FAILED,
                ErrorDetail(
                        "newPassword",
                        "TooShort",
                        "Password must be 4 or more characters in length"
                )
        )
    }

    @Test
    fun shouldFailed_whenCurrentPasswordIsInvalid() {
        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdateMyPasswordDto("invalid_current_pass", "newPass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_PASSWORD)

        // then
        assertErrorInfo(response, PasswordErrorInfo.INVALID_PASSWORD_FOR_CURRENT_MEMBER)
    }

    @Test
    fun shouldFailedWith401_whenNotSignInUserCalledEndpoint() {
        // when
        val response = given()
                .body(UpdateMyPasswordDto("aaaa", "bbbb"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_ME_PASSWORD)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}