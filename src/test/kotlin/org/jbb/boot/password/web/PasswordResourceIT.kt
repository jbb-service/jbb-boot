/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.boot.member.web.MemberErrorInfo
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_PASSWORD = "/api/v1/members/{memberId}/password"

internal class PasswordResourceIT : AbstractBaseResourceIT() {

    @Test
    fun adminShouldBeAbleToUpdateMemberPassword() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePasswordDto("newPass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)

        // then
        response.then().status(HttpStatus.NO_CONTENT)
    }

    @Test
    fun adminShouldBeAbleToExpireMemberPassword() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePasswordDto(expired = true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)

        // then
        response.then().status(HttpStatus.NO_CONTENT)
    }

    @Test
    fun shouldReturnMemberNotFound_whenTryingToUpdatePasswordForNotExistingMember() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePasswordDto("newPass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, "not-existing-member-id")

        // then
        assertErrorInfo(response, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldFailed_whenPasswordIsTooShort() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePasswordDto("sho"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)

        // then
        assertErrorInfo(
                response, PasswordErrorInfo.PASSWORD_UPDATE_FAILED,
                ErrorDetail(
                        "newPassword",
                        "TooShort",
                        "Password must be 4 or more characters in length"
                )
        )
    }

    @Test
    fun simpleUserShouldNotBeAbleToUseThatEndpointForChangingPassword() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdatePasswordDto("newPass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun simpleUserShouldNotBeAbleToUseThatEndpointForUndoExpiringPassword() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(UpdatePasswordDto(expired = false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().patch(API_V1_PASSWORD, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

}