/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import com.nhaarman.mockitokotlin2.any
import org.jbb.boot.privilege.api.AdministratorPrivilegeAddedEvent
import org.jbb.boot.privilege.api.AdministratorPrivilegeRemovedEvent
import org.jbb.boot.privilege.api.Privilege
import org.jbb.swanframework.eventbus.DomainEventBus
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class PrivilegeEventEmitterTest {
    @InjectMocks
    private lateinit var privilegeEventEmitter: PrivilegeEventEmitter

    @Mock
    private lateinit var domainEventBusMock: DomainEventBus

    @Test
    fun shouldSendAdministratorPrivilegeAddedEvent_whenSendAboutAddition_withAdminPrivilege() {
        // when
        privilegeEventEmitter.sendAboutAddition(administratorPrivilege())

        // then
        Mockito.verify(domainEventBusMock).post(any<AdministratorPrivilegeAddedEvent>())
    }

    @Test
    fun shouldSendAdministratorPrivilegeRemovedEvent_whenSendAboutRemoved_withAdminPrivilege() {
        // when
        privilegeEventEmitter.sendAboutRemoval(administratorPrivilege())

        // then
        Mockito.verify(domainEventBusMock).post(any<AdministratorPrivilegeRemovedEvent>())
    }

    private fun administratorPrivilege() = PrivilegeEntity(
            memberId = "any",
            privilege = Privilege.ADMINISTRATOR
    )
}

