/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.member.web.MemberErrorInfo
import org.jbb.boot.privilege.api.Privilege
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType

const val API_V1_MEMBER_PRIVILEGES = "/api/v1/members/{memberId}/privileges"
const val API_V1_MEMBER_PRIVILEGES_ADMINISTRATOR = "$API_V1_MEMBER_PRIVILEGES/administrator"

internal class PrivilegeResourceIT : AbstractBaseResourceIT() {

    @Test
    fun shouldAddAndRemoveAdministratorPrivilegeSuccessfully() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        var getResponse = given().`when`()[API_V1_MEMBER_PRIVILEGES, testMember.memberId]
        var responseBody = getResponse.`as`(PrivilegesDto::class.java)

        // then
        assertThat(responseBody.privileges).isEmpty()

        // when
        var grantedResponse = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePrivilegeDto(true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_PRIVILEGES_ADMINISTRATOR, testMember.memberId)
        var grantedResponseBody = grantedResponse.`as`(PrivilegesDto::class.java)

        // then
        assertThat(grantedResponseBody.privileges).containsExactly(Privilege.ADMINISTRATOR)

        // when
        getResponse = given().`when`()[API_V1_MEMBER_PRIVILEGES, testMember.memberId]
        responseBody = getResponse.`as`(PrivilegesDto::class.java)

        // then
        assertThat(responseBody.privileges).containsExactly(Privilege.ADMINISTRATOR)

        // when
        grantedResponse = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePrivilegeDto(false))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_PRIVILEGES_ADMINISTRATOR, testMember.memberId)
        grantedResponseBody = grantedResponse.`as`(PrivilegesDto::class.java)

        // then
        assertThat(grantedResponseBody.privileges).doesNotContain(Privilege.ADMINISTRATOR)

        // when
        getResponse = given().`when`()[API_V1_MEMBER_PRIVILEGES, testMember.memberId]
        responseBody = getResponse.`as`(PrivilegesDto::class.java)

        // then
        assertThat(responseBody.privileges).isEmpty()
    }

    @Test
    fun shouldReturnMemberNotFound_whenGettingPrivilegeForNotExistingMember() {
        // when
        val response = given()
                .`when`()[API_V1_MEMBER_PRIVILEGES, "not-existing-member"]

        // then
        assertErrorInfo(response, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

    @Test
    fun shouldReturnMemberNotFound_whenUpdatingPrivilegeForNotExistingMember() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(UpdatePrivilegeDto(true))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_MEMBER_PRIVILEGES_ADMINISTRATOR, "not-existing-member")

        // then
        assertErrorInfo(response, MemberErrorInfo.MEMBER_NOT_FOUND)
    }

}