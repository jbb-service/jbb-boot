/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.etag

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

internal class ETagIT : BaseSwanIT() {

    @Autowired
    private lateinit var getResource: GetResource

    @Test
    fun getResponsesShouldHaveETag() {
        // given
        getResource.value = 0

        // when
        val response = given().`when`().get("api/etag-test")

        // then
        response.then().status(HttpStatus.OK)
        assertThat(response.`as`(Foo::class.java).value).isEqualTo(0)

        val etag = response.header("ETag")

        // when
        val secondResponse = given().header("If-None-Match", etag)
                .`when`().get("api/etag-test")

        // then
        secondResponse.then().status(HttpStatus.NOT_MODIFIED)
        assertThat(secondResponse.header("ETag")).isEqualTo(etag)

        // when
        getResource.value = 1
        val thirdResponse = given().header("If-None-Match", etag)
                .`when`().get("api/etag-test")

        // then
        assertThat(thirdResponse.`as`(Foo::class.java).value).isEqualTo(1)

        val newETag = thirdResponse.header("ETag")
        assertThat(newETag).isNotEqualTo(etag)
    }
}