/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.openapi

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

const val API_V3_SWAGGER_DOCS = "/api/v3/swagger-docs"

internal class SwaggerControllerIT : BaseSwanIT() {
    @Test
    fun shouldRedirectToSwaggerUI_whenRootPageOpened() {
        // when
        val response = given().`when`()["/"]

        // then
        response.then().expect(MockMvcResultMatchers.redirectedUrl("/swagger-ui.html"))
    }

    @Test
    fun shouldReturnOpenApiFile_whenSwaggerDocsEndpointCalled() {
        // when
        val response = given().`when`()[API_V3_SWAGGER_DOCS]

        // then
        response.then().status(HttpStatus.OK)
        assertThat(response.body).isNotNull
    }
}