/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.startup

import com.fasterxml.jackson.databind.ObjectMapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import org.apache.catalina.connector.Request
import org.apache.catalina.connector.Response
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verifyNoInteractions
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import javax.servlet.ServletOutputStream

@ExtendWith(MockitoExtension::class)
internal class ProgressValveTest {

    @Mock
    private lateinit var objectMapper: ObjectMapper

    @InjectMocks
    private lateinit var progressValve: ProgressValve

    @Test
    fun shouldSaveErrorCodeUsingObjectMapper_whenCallToApiUrl() {
        // given
        val request = mock(Request::class.java)
        val response = mock(Response::class.java)

        given(request.requestURI).willReturn("/api/test")
        given(response.outputStream).willReturn(mock(ServletOutputStream::class.java))

        // when
        progressValve.invoke(request, response)

        // then
        verify(objectMapper).writeValueAsBytes(any())
        verify(response).status = eq(503)
    }

    @Test
    fun shouldReturnLoadingResource_whenCallNotToApiUrl() {
        // given
        val request = mock(Request::class.java)
        val response = mock(Response::class.java)

        given(request.requestURI).willReturn("/not-api-call")
        given(response.outputStream).willReturn(mock(ServletOutputStream::class.java))

        // when
        progressValve.invoke(request, response)

        // then
        verifyNoInteractions(objectMapper)
        verify(response).status = eq(503)
    }
}