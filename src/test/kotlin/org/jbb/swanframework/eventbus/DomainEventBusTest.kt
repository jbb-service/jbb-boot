/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import com.nhaarman.mockitokotlin2.any
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.eventbus.EventHandler.*
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import org.springframework.context.annotation.Import

@SpringBootTest
@Import(TestEventsConfiguration::class)
internal class DomainEventBusTest {
    @Autowired
    private lateinit var domainEventBus: DomainEventBus

    @Autowired
    private lateinit var eventHandler: EventHandler

    @SpyBean
    private lateinit var eventExceptionHandlerSpy: EventExceptionHandler

    @Test
    fun shouldSubscriberGetAnEvent() {
        // when
        domainEventBus.post(ExampleEvent(234))

        // then
        assertThat(eventHandler.events).hasSize(1)
        val event = eventHandler.events[0]
        assertThat(event).isInstanceOf(ExampleEvent::class.java)
        assertThat((event as ExampleEvent).value).isEqualTo(234)
    }

    @Test
    fun postingNotDomainEvents_shouldNotBePossible() {
        assertThrows(IllegalArgumentException::class.java) { domainEventBus.post(Any()) }
    }

    @Test
    fun inCaseOfFailureDuringProcessing_exceptionHandlerHaveToBeCalled() {
        // when
        domainEventBus.post(FailureEvent())

        // then
        verify(eventExceptionHandlerSpy).handleException(any<MyCustomException>(), any())
    }

}