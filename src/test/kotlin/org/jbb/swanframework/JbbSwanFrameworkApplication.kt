/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService

@SpringBootApplication
class JbbSwanFrameworkApplication {

    @Bean
    fun testAppDirectory(): TestAppDirectory = TestAppDirectory()

    @Bean
    fun userDetailsService(): UserDetailsService {
        return UserDetailsService { User("test", "test", emptyList()) }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(JbbSwanFrameworkApplication::class.java, *args)
        }
    }
}