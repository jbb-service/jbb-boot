/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.dryrun

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus

@Import(TestDryRunConfiguration::class)
internal class DryRunIT : BaseSwanIT() {

    @Test
    fun shouldPerformOperation_whenDryRunParamMissingForDryRunnableMethod() {
        // when
        val response = given().post("/api/operation")

        // then
        val responseBody = response.`as`(TestDryRunResource.Status::class.java)
        assertThat(responseBody).isEqualTo(TestDryRunResource.Status(true))
    }

    @Test
    fun shouldPerformOperation_whenDryRunParamIsFalseForDryRunnableMethod() {
        // when
        val response = given().queryParam("dryRun", false)
                .post("/api/operation")

        // then
        val responseBody = response.`as`(TestDryRunResource.Status::class.java)
        assertThat(responseBody).isEqualTo(TestDryRunResource.Status(true))
    }

    @Test
    fun shouldNotPerformOperation_whenDryRunParamIsTrueForDryRunnableMethod() {
        // when
        val response = given().queryParam("dryRun", true)
                .post("/api/operation")

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.NO_CONTENT.value())
        assertThat(response.header("dry-run-success")).isEqualTo("true")
        assertThat(response.body().asString()).isEmpty()
    }

    @Test
    fun shouldPerformOperation_whenDryRunParamHasDummyValueForDryRunnableMethod() {
        // when
        val response = given().queryParam("dryRun", "omc")
                .post("/api/operation")

        // then
        val responseBody = response.`as`(TestDryRunResource.Status::class.java)
        assertThat(responseBody).isEqualTo(TestDryRunResource.Status(true))
    }

}