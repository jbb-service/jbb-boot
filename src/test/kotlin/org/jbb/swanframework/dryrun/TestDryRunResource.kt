/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.dryrun

import io.swagger.v3.oas.annotations.Hidden
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Hidden
@RestController
@RequestMapping(value = ["/api/operation"], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class TestDryRunResource(private val dryRunContext: DryRunContext) {

    @DryRunnable
    @PostMapping
    fun dryRunnable(): Status {
        dryRunContext.completeIfDryRun()
        return Status(true)
    }

    data class Status(val status: Boolean)

}