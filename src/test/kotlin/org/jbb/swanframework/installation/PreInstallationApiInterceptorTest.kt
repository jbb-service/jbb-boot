/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.installation

import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.web.utils.PathResolver
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.mock
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.web.method.HandlerMethod
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ExtendWith(MockitoExtension::class)
internal class PreInstallationApiInterceptorTest {
    @Mock
    private lateinit var installationCheckProperties: InstallationCheckProperties

    @Mock
    private lateinit var installationVerifier: InstallationVerifier

    @Mock
    private lateinit var pathResolver: PathResolver


    private val request = Mockito.mock(HttpServletRequest::class.java)
    private val response = Mockito.mock(HttpServletResponse::class.java)

    @InjectMocks
    private lateinit var preInstallationApiInterceptor: PreInstallationApiInterceptor

    @Test
    fun shouldPass_whenIsItInstalled() {
        // given
        given(installationVerifier.isInstalled()).willReturn(true)

        // when
        val result = preInstallationApiInterceptor.preHandle(request, response, Any())

        // then
        assertThat(result).isTrue
    }

    @Test
    fun shouldThrowNotInstalled_whenPathFromIncludeNotExcluded() {
        // given
        given(installationVerifier.isInstalled()).willReturn(false)
        given(pathResolver.getRequestPathWithinApplication()).willReturn("/api/members")
        given(installationCheckProperties.include).willReturn("/api/**")
        given(installationCheckProperties.exclude).willReturn(listOf("/api/v1/health"))

        // when then
        Assertions.assertThrows(NotInstalled::class.java) {
            preInstallationApiInterceptor.preHandle(request, response, Any())
        }
    }

    @Test
    fun shouldPass_whenPathNotInInclude() {
        // given
        given(installationVerifier.isInstalled()).willReturn(false)
        given(pathResolver.getRequestPathWithinApplication()).willReturn("/index.html")
        given(installationCheckProperties.include).willReturn("/api/**")
        given(installationCheckProperties.exclude).willReturn(listOf("/api/v1/health"))

        // when
        val result = preInstallationApiInterceptor.preHandle(request, response, Any())

        // then
        assertThat(result).isTrue
    }

    @Test
    fun shouldPass_whenPathIsInIncludeAndExcludeList() {
        // given
        given(installationVerifier.isInstalled()).willReturn(false)
        given(pathResolver.getRequestPathWithinApplication()).willReturn("/api/v1/health")
        given(installationCheckProperties.include).willReturn("/api/**")
        given(installationCheckProperties.exclude).willReturn(listOf("/api/v1/health"))

        // when
        val result = preInstallationApiInterceptor.preHandle(request, response, Any())

        // then
        assertThat(result).isTrue
    }

    @Test
    fun shouldPass_whenPathIsInIncludeButMethodIsAnnotatedWithAvailableBeforeInstallation() {
        // given
        given(installationVerifier.isInstalled()).willReturn(false)
        given(pathResolver.getRequestPathWithinApplication()).willReturn("/api/v1/install")
        given(installationCheckProperties.include).willReturn("/api/**")
        given(installationCheckProperties.exclude).willReturn(listOf("/api/v1/health"))

        val handler = mock(HandlerMethod::class.java)
        given(handler.hasMethodAnnotation(AvailableBeforeInstallation::class.java)).willReturn(true)

        // when
        val result = preInstallationApiInterceptor.preHandle(request, response, handler)

        // then
        assertThat(result).isTrue
    }
}