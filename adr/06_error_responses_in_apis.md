# Error responses in APIs

## Status

Accepted

## Context

Application should return standardised format of the response in case of any client or server error.

## Decision

1. Client and server errors raised by the app have a response body compatible
   with [RFC 7807](https://tools.ietf.org/html/rfc7807). It means that every request which results
   in HTTP 400-599 error should return in response these fields:
    * `type` - string with the url to the proper Error code endpoint
      e.g. `/api/v1/error-codes/IEER-001` (see point 4.)
    * `title` - a short, human-readable summary of the problem. It will be i18nized in a future
    * `status` - HTTP status of response e.g. 400, 404, 500 etc.
    * `instance` - string in format `urn:uuid:TRACE_ID` where `TRACE_ID` is a unique server-assigned
      identifier of the request
1. Based on [section 3.2](https://tools.ietf.org/html/rfc7807#section-3.2) app should introduce
   fixed set of Extension Members:
    * `errorCode` (required) - an obligatory short code in format `[A-Z]{4}-[0-9]{3}` where the
      first part specifies a domain (e.g. `IEER` - internal errors, `GNRL` - general errors, `MMBR` - member-related errors etc.) and second part is just a counter (consecutively `001`, `002`, `003` etc.)
    * `timestamp` (required) - a moment of experiencing error on a server in ISO 8601 format
    * `details` (might be empty, not null) - a list of Error Detail objects specified in point 3.
1. Error Detail object has a given structure:
    * `property` - a json path related to the problematic field in payload, or it is just a name of
      path/query parameter (might be absent when detail is related to whole request in general)
    * `violationCode` - a code for a given violation in PascalCase (e.g. `Length`, `NotBlank`, `Pattern`, `LastPasswordDetected` etc.)
    * `message` - a short, human-readable summary of the detail. It will be i18nized in a future (required)
    * `context` - an optional map object which may help in automatic resolving an error by the
      machine without requirement of parsing `message`. Context object may have different structure
      depending on error code and/or given violation
1. Application should implement `GET /api/v1/error-codes/{errorCode}` available for everybody which
   will return all details about particular error code

### Examples

#### Internal server error:

```json
{
  "type": "/api/v1/error-codes/IERR-001",
  "errorCode": "IERR-001",
  "title": "Internal error",
  "status": 500,
  "instance": "urn:uuid:7a493e06-b3c9-47e4-85e8-7cc0d732ffd2",
  "timestamp": "2021-05-08T13:13:16.608444300Z",
  "details": []
}
```

#### Client not found error:

```json
{
  "type": "/api/v1/error-codes/MMBR-100",
  "errorCode": "MMBR-100",
  "title": "Member not found",
  "status": 404,
  "instance": "urn:uuid:72011a4-b09f-4822-a160-7b298e48e5d4",
  "timestamp": "2021-05-04T21:37:11.809111300Z",
  "details": []
}
```

#### Client validation error:

```json
{
  "type": "/api/v1/error-codes/MMBR-101",
  "errorCode": "MMBR-101",
  "title": "Member creation failed",
  "status": 400,
  "instance": "urn:uuid:b962990a-e01e-486f-8d2a-676b7061c5d3",
  "timestamp": "2021-05-04T21:37:11.809111300Z",
  "details": [
     {
        "property": "email",
        "violationCode": "NotBlank",
        "message": "Must not be blank"
     },
     {
        "property": "username",
        "violationCode": "Length",
        "message": "Length must be between 4 and 32",
        "context": {
           "min": 4,
           "max": 32
        }
     },
     {
        "property": "password",
        "violationCode": "InsufficientDigit",
        "message": "Password must contain 1 or more digit characters",
        "context": {
           "required": 1
        }
     },
     {
        "property": "password",
        "violationCode": "WordFromBlackListDetected",
        "message": "Password contains word 'qwerty' from black list",
        "context": {
           "word": "qwerty"
        }
     }
  ]
}
```

## Consequences

* Error responses can be easily processed by external libraries
  like [Zalando Problem](https://github.com/zalando/problem)
* It might be challenging to manage all error codes and violation codes across multiple application
  domains
