# Application directory

## Status

Accepted

## Context

Every instance of the application needs some space for storing, for example:
* configuration related to the database access
* configuration which can't be stored in DB like logging config, cache config etc.
* log files
* some temporary and cache files

Application requires some system-agnostic place for storing that kind of data in local storage.

## Decision

* On a first run application will create a directory with name `.jbb-boot` under user home location (`System.getProperty("user.home")`) on a local machine
* Application MUST have read and write access for `.jbb-boot` folder. Otherwise, it won't start.

## Consequences

* Every user of the system will be able to run own instance of the application
* We won't give possibility to override path to application directory (for example, through environment variables) now. It means that running multiple instances of the app by one user may cause some damage - all instances will use the same location for storage
* Having multi-instance environment will need some extra checks that all instances are working with the same DB credentials, logging levels etc.