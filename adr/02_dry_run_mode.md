# Dry run mode

## Status

Accepted

## Context

The API client would like to validate the request that changes the system before performing the change.

## Decision

Each `POST` / `PUT` / `PATCH` / `DELETE` business endpoint should accept optional `dryRun` query parameter of type `boolean` in request. 

* When it's `true` then system must perform necessary validations only. If request is valid then response:
    * must have 204 status
    * must have empty body
    * must have header `dry-run-success` set to `true`
* When it's `false` or absent then system must perform validations and change the state as usual when request is valid.

## Consequences

Domain services implementation will be more complex. Perhaps services will have to be differently organized to perform any validation operations first. 
Not always it will be easy to implement and this can obscure the readability of the solution.