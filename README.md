# jBB Boot

### Build instructions
Run command:
```
./gradlew build
```
it will:
* Run all unit & integration tests
* Run architecture tests
* Build a fat jar with the app

### Run instructions

Run command:

```
./gradlew bootRun
```

You can start app without gradle. After successful build, run:

```
java -jar build/libs/jbb-boot-app-${project.version}.jar
```

#### 🐳 Running with Docker

Application has been dockerized. More details how to run it using Docker can be
found [here](docker/README.md).

### Useful commands

* `./gradlew build -x test` - fast build without any tests
* `./gradlew architectureTest` - running architecture tests only
* `./gradlew detekt` - running static code analysis. Report can be found
  under `/build/reports/detekt/detekt.html`
* `./gradlew generateOpenApiDocs` - generate OpenAPI YAML under `/build/docs/openapi.yaml`
* `./gradlew generateReDoc` - generate ReDoc documentation under `/build/redoc`
* `./gradlew bootBuildImage` - build `jbb/boot:latest` docker image with the application and put it
  into local docker repository
* `./gradlew checkFixMe` - fail if it finds any `FIXME` phrase (case insensitive) in source code
  comments

### End to end tests

E2E tests have been written
in [Serenity BDD framework](https://serenity-bdd.github.io/theserenitybook/latest/index.html). They
treat a system like a [black box](https://www.guru99.com/black-box-testing.html), so they should not
based on internal structure of the application.

You can run them with command:

```
./gradlew endToEndTest -PbaseUrl=... -PadminUsername=... -PadminPassword=...
```

#### Notes

- Application must be started before running E2E tests under `baseUrl` location
- E2E tests assume that the application is already installed
- Some E2E tests need an administrator's credentials, taken from `adminUsername`, `adminPassword`
  parameters
  - Admin member must exist
  - Admin member must have administrative privileges
  - Admin member can't be locked or have an expired password
- Parameter `baseUrl` is not required. Its default value is `http://localhost:8080`
- Parameter `adminUsername` is not required. Its default value is `administrator`
- Parameter `adminPassword` is not required. Its default value is `administrator`
- Report with results can be found under `/target/site/serenity/index.html`

### Architecture decision records

* 01 - [Pagination in APIs](adr/01_pagination_in_apis.md)
* 02 - [Dry run mode](adr/02_dry_run_mode.md)
* 03 - [Category model](adr/03_category_model.md)
* 04 - [Application directory](adr/04_application_directory.md)
* 05 - [Installation state and bootstrapping](adr/05_installation_and_bootstrapping.md)
* 06 - [Error responses in APIs](adr/06_error_responses_in_apis.md)